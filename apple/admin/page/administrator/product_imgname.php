<style>
#input_hover { border:2px solid #CBFA6C; transition:all 0.9s ease-out; background:white; margin:2px; text-align:left; width:100%; padding:15px 10px;  }
#input_hover:hover { border:2px solid #B3049C; transition:all 0.9s ease-out; background:white; margin:2px;  text-align:left; width:100%; padding:15px 10px; -webkit-transition:0.2s; transition:0.3s; }
#input_hover:active { border:2px solid #69085C; transition:all 0.9s ease-out; background:white; margin:2px;  text-align:left; width:100%; padding:15px 10px; -webkit-transition:0.2s; transition:0.3s; }

.error { padding:15px 10px 15px 10px; color:red;  border-radius:5px; overflow:hidden; font-size:12px; letter-spacing:0px; background:url(img/error_back.png);}
.error p { text-align:justify; color:red;  font-weight:700; }
.error b { text-align:justify; color:red; font-size:13px; padding:10px 0px; }


</style>
<div id="page-wrapper">

         <div class="container-fluid">
			
			
			
			
			
			<div id="page-wrapper">
           <aside class="right-side">                
            <section class="content-header">
                    <h1><img src="img/head_panel.jpg" align="left"><small> Edit product image</small></h1>
                    <ol class="breadcrumb">
                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=manage_page'>"; ?>Home</a></li>
                        <li><strong><?php echo $_REQUEST["brandname"]; ?></strong></li>
						<li class="active" style="float:right;">
						<!------------------------right-------------menu--------------->
						
						
						
							<div class="pull-right">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">
                                        Actions
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage trichology product&file_name=Trichology">Trichology</a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage dermatology product&file_name=Dermatology">Dermatology  </a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage gynecology product&file_name=Gynecology">Gynecology</a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage pediatrics product&file_name=Paediatrics">Pediatrics  </a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage cosmetology product&file_name=Cosmetology">Cosmetology  </a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage nutrition product&file_name=Nutrition">Nutrition  </a></li>
                                    
									</ul>
                                </div>
                            </div>
							<div class="pull-right" style="padding:0px 10px;">
									<div class="btn-group">
										<?php echo "<a href='website.php?PHPISID=$remoteaddress&home=manage_page'>"; ?><img src="img/left.jpg" width="20px"></a>
									</div>
							</div>
							<?php
							echo 
							"
							<div class='pull-right'>
									<div class='btn-group'>
										<button type='button' class='btn btn-default btn-xs dropdown-toggle' data-toggle='dropdown'>
											Actions
											<span class='caret'></span>
										</button>
										<ul class='dropdown-menu pull-right' role='menu'>
											<li><a href='website.php?PHPISID=$remoteaddress&home=product_spec&about_mass=Home&brandid=$image_name&brandname=$brandname'>New specification</a></li>
											<li><a href='website.php?PHPISID=$remoteaddress&home=product_imgname&about_mass=Home&brandid=$image_name&brandname=$brandname'>Edit Product image</a></li>
											<li><a href='website.php?PHPISID=$remoteaddress&home=product_barcode&about_mass=Home&brandid=$image_name&brandname=$brandname'>Product Barcode</a></li>
											<li><a href='website.php?PHPISID=$remoteaddress&home=product_dimsen&about_mass=Home&brandid=$image_name&brandname=$brandname'>Product dimension  </a></li>
											<li><a href='website.php?PHPISID=$remoteaddress&home=product_caution&about_mass=Home&brandid=$image_name&brandname=$brandname'>Caution  </a></li>
											<li><a href='website.php?PHPISID=$remoteaddress&home=product_other&about_mass=Home&brandid=$image_name&brandname=$brandname'>Other points</a></li>
										
										</ul>
									</div>
								</div>
							";	
							?>
							
						
						
						
						<!--------------------right close------------------------>
						</li>
                    </ol>
            </section>

                                
            </aside>
            
			<form method="post" enctype="multipart/form-data">
			<div class="row">
			<?php
							
							
							function error()
							{
									echo "<div class=error><p>There was a problem with your request</p> Error image .<br> Please correct and try again.</div><br>";
									
							}
							function error_image()
							{
									echo "<div class=error><p>There was a problem with your request</p> Error image .<br> Please correct and try again.</div><br>";
									
							}
							function error_image_size()
							{
									echo "<div class=error><p>There was a problem with your request</p> Maximum image size 1 MB .<br> Please correct and try again.</div><br>";
									
							}
							function error_image_support()
							{
									echo "<div class=error><p>There was a problem with your request</p> Plz. upload jpg image  .<br> Please correct and try again.</div><br>";
									
							}
							?>
                <?php 
				$ravi = new ravikumar03391_indexpages(); $ravi->method('edit_productimage'); $ravi->method('edit_product_function_save'); $ravi->method('edit_product_function');  
				function product_desc1($brandname,$productname,$head_name,$pkg,$nick_name,$image_name,$subcat,$ingredient,$direction,$rate,$expdate)
				{
				
					echo '<div class="col-lg-6">';
						echo '<b><br>&nbsp;&nbsp; Your Old Product Image:</b>';
						echo "<img src=../../images/products/$image_name.jpg id='input_hover'>";
						
					echo '</div>';
					echo '<div class="col-lg-6">';
						echo '<p class="clear"></p>';
						
						echo "<input type='hidden' name='brandid' value='".$image_name."'>";
						echo '<b><br>&nbsp;&nbsp; Product image specification:</b>';
						echo '<br>&nbsp;&nbsp; 1. Image dimension : 600 X 600 px:';
						echo '<br>&nbsp;&nbsp; 2. Image Weight : Max 50kb';
						echo '<br>&nbsp;&nbsp; 3. Do not use image border';
						echo '<br>&nbsp;&nbsp; 4. We are supported only jpg images. Dot not upload any files<br><br>';
						
						echo '<b><br>&nbsp;&nbsp; New Product Image:</b>';
						
						echo "<input type='file' class=input name='img' id='input_hover'>";
						
						
						
					echo '<input type="submit" value="Save data" name="edit_productimage" class="w3-col m6 submit_button radius_h5"><input type="reset" value="Reset / Back" name="" class="w3-col m6 submit_button radius_h5">';						echo '</div>';
					
				
				}
									 
				?>
				
                 
                
            </div>
							
			</form>
            <!-- /.row -->
        </div>
			
			
			
			
			</div>
            
        </div>
		