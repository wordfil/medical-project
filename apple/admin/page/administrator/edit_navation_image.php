<style>
#input_hover { border:2px solid #CBFA6C; transition:all 0.9s ease-out; background:white; margin:2px; text-align:left; width:100%; padding:15px 10px;  }
#input_hover:hover { border:2px solid #B3049C; transition:all 0.9s ease-out; background:white; margin:2px;  text-align:left; width:100%; padding:15px 10px; -webkit-transition:0.2s; transition:0.3s; }
#input_hover:active { border:2px solid #69085C; transition:all 0.9s ease-out; background:white; margin:2px;  text-align:left; width:100%; padding:15px 10px; -webkit-transition:0.2s; transition:0.3s; }

.error { padding:15px 10px 15px 10px; color:red;  border-radius:5px; overflow:hidden; font-size:12px; letter-spacing:0px; background:url(img/error_back.png);}
.error p { text-align:justify; color:red;  font-weight:700; }
.error b { text-align:justify; color:red; font-size:13px; padding:10px 0px; }


</style>


<div id="page-wrapper">

         <div class="container-fluid">
			
			
			
			
			
			<div id="page-wrapper">
           <aside class="right-side">                
            <section class="content-header">
                    <h1><img src="img/head_panel.jpg" align="left"><small> Edit navigation slider</small></h1>
                    <ol class="breadcrumb">
                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=manage_page'>"; ?>Home</a></li>
                        <li><strong><?php $file_name=$_REQUEST["file_name"]; echo $file_name; ?></strong></li>
						
                    </ol>
            </section>

                                
            </aside>
            
			<form method="post" enctype="multipart/form-data">
			<div class="row">
			<?php
							
							
							function error()
							{
									echo "<div class=error><p>There was a problem with your request</p> Error image .<br> Please correct and try again.</div><br>";
									
							}
							function error_image()
							{
									echo "<div class=error><p>There was a problem with your request</p> Error image .<br> Please correct and try again.</div><br>";
									
							}
							function error_image_size()
							{
									echo "<div class=error><p>There was a problem with your request</p> Maximum image size 1 MB .<br> Please correct and try again.</div><br>";
									
							}
							function error_image_support()
							{
									echo "<div class=error><p>There was a problem with your request</p> Plz. upload jpg image  .<br> Please correct and try again.</div><br>";
									
							}
							?>
                <?php 
				  
				echo '<div class="col-lg-6">';
						echo '<b><br>&nbsp;&nbsp; Your Old navigation slider Image:</b>';
						echo "<img src='../../images/product_image/$file_name' id='input_hover'>";
						
					echo '</div>';
					echo '<div class="col-lg-6">';
						echo '<p class="clear"></p>';
						
						echo "<input type='hidden' name='brandid' value='".$file_name."'>";
						echo '<b><br>&nbsp;&nbsp; Image specification:</b>';
						echo '<br>&nbsp;&nbsp; 1. Image dimension : 800 X 597 px:';
						echo '<br>&nbsp;&nbsp; 2. Image Weight : Max 250kb';
						echo '<br>&nbsp;&nbsp; 3. Do not use image border';
						echo '<br>&nbsp;&nbsp; 4. We are supported only jpg images. Dot not upload any files<br><br>';
						
						echo '<b><br>&nbsp;&nbsp; New Product Image:</b>';
						
						echo "<input type='file' class=input name='upload_file' id='input_hover'>";
						
						
						
					echo '<input type="submit" value="Save image" name="navigation_slider_edit_image" class="w3-col m6 submit_button radius_h5"><input type="reset" value="Reset / Back" name="" class="w3-col m6 submit_button radius_h5">';						echo '</div>';
					
									 
				?>
				
                 
                
            </div>
							
			</form>
            <!-- /.row -->
        </div>
			
			
			
			
			</div>
            
        </div>
		