<style>
.ok { padding:15px 10px 15px 10px; color:white;  border-radius:5px; overflow:hidden; font-size:12px; letter-spacing:0px; background:url(img/ok.png);}
.ok p { text-align:justify; color:green;  font-weight:700; }
.ok b { text-align:justify; color:green; font-size:13px; padding:10px 0px; }

</style>
<?php 
		$session=$_SESSION["username"];
		$md=md5($_SERVER['REMOTE_ADDR']);
		$time=time("h-m-s");
		$sh=sha1($_SERVER['REMOTE_ADDR']);
		$remoteaddress=$md.$time.$sh;
		
	?>
<div id="page-wrapper">

         <div class="container-fluid">
			
			<div id="page-wrapper">
				<aside class="right-side">                
					<section class="content-header">
						<h1><img src="img/head_panel.jpg" align="left"><small>Apple therapeutics</small></h1>
						<ol class="breadcrumb">
							<?php echo "<a href='website.php?PHPISID=$remoteaddress&home=manage_page'>"; ?>Home / </a>
							<li>Control panel</li>
											
						</ol>
					</section>

													
				</aside>
			
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-fw fa-desktop"></i>&nbsp; Control panel for text
								
							</div>
							<div class="panel-body">
								<!------------body------->
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                        Manage your page
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Home&page_folder=Control Panel&file_name=home'>"; ?>Home</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=About us&page_folder=Control Panel&file_name=about'>"; ?>About</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Company History&page_folder=Control Panel&file_name=history'>"; ?>History</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Our Mission&page_folder=Control Panel&file_name=mission'>"; ?>Mission</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Business Opportunities&page_folder=Control Panel&file_name=business'>"; ?>Business</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Doctor info&page_folder=Control Panel&file_name=doctor'>"; ?>Doctor info</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Trading&page_folder=Control Panel&file_name=treading'>"; ?>Trading</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Get updates&page_folder=Control Panel&file_name=update'>"; ?>Get updates</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Useful link&page_folder=Control Panel&file_name=useful'>"; ?>Useful link</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Your Feedback&page_folder=Control Panel&file_name=feedback'>"; ?>Feedback</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Contact us&page_folder=Control Panel&file_name=contact'>"; ?>Contact us</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Career&page_folder=Control Panel&file_name=career'>"; ?>Career</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Why Apple us&page_folder=Control Panel&file_name=whyapple'>"; ?>Why Apple</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Training&page_folder=Control Panel&file_name=training'>"; ?>Training</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Career&page_folder=Control Panel&file_name=career'>"; ?>Current Opening</a></li>
										
										
                                    </ul>
                                </div>
								
								
														
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                        Manage your banner
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=About Banner&page_folder=Control Panel&file_name=about'>"; ?>About Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Mission Banner&page_folder=Control Panel&file_name=mission'>"; ?>Mission Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=History Banner&page_folder=Control Panel&file_name=history'>"; ?>History Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Business Banner&page_folder=Control Panel&file_name=business'>"; ?>Business Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Career Banner&page_folder=Control Panel&file_name=career'>"; ?>Career Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Contact us Banner&page_folder=Control Panel&file_name=contact'>"; ?>Contact Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Doctor Info Banner&page_folder=Control Panel&file_name=doctor'>"; ?>Doctor Info Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Treading Banner&page_folder=Control Panel&file_name=trading'>"; ?>Trading Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Get Update Banner&page_folder=Control Panel&file_name=update'>"; ?>Get Update Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Usefull Links Banner&page_folder=Control Panel&file_name=useful'>"; ?>Usefull Links Banner</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Feedback Banner&page_folder=Control Panel&file_name=feedback'>"; ?>Feedback Banner</a></li>
										
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                        Manage your events
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                         <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=news'>"; ?>News & events</a></li>
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                        Manage your sliders
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=navi&about_mass=Home&page_folder=Naigation sliders&file_name=navigation'>"; ?>Navigation slider</a></li>
										
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Manage your menu
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Home&page_folder=Manage Banner&file_name=about'>"; ?>Head</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Home&page_folder=Manage Banner&file_name=mission'>"; ?>Footer</a></li>
										
                                    </ul>
                                </div>
								<div class="btn-group">
                                    <br>
									
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Manage your product page
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage trichology product&file_name=Trichology">Trichology</a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage dermatology product&file_name=Dermatology">Dermatology  </a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage gynecology product&file_name=Gynecology">Gynecology</a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage pediatrics product&file_name=Paediatrics">Pediatrics  </a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage cosmetology product&file_name=Cosmetology">Cosmetology  </a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage nutrition product&file_name=Nutrition">Nutrition  </a></li>
										<li><a href="website.php?PHPISID=$remoteaddress&home=product&about_mass=Home&page_folder=Manage Lemollis product&file_name=Lemollis">L'emollis range  </a></li>
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Add new sub-category
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=subcat'>"; ?>Add Subcatagory</a></li>
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Our Greetings
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=greadings'>"; ?>My Greetings</a></li>
                                    </ul>
                                </div>
								
								<!-----------end body------>
							</div>
						</div>
					</div>
              
				</div>
				
				
				
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-fw fa-desktop"></i>&nbsp; Control panel for images
								
							</div>
							<div class="panel-body">
								<!------------body------->
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Manage your images
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Home&page_folder=Manage Banner&file_name=internal_img'>"; ?>Internal</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Home&page_folder=Manage Banner&file_name=external_img'>"; ?>External</a></li>
										
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Products all images
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=product_allimages&about_mass=Home&page_folder=Manage Images'>"; ?>All images</a></li>
										
                                    </ul>
                                </div>
								<!-----------end body------>
							</div>
							
							
						</div>
					</div>
              
				</div>
				
				
				
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div class="panel-heading">
								<i class="fa fa-fw fa-desktop"></i>&nbsp; Developers
								
							</div>
							<div class="panel-body">
								<!------------body------->
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Contact
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=develop&about_mass=Developers Contact&page_folder=Control Panel&file_name=contact_mail'>"; ?>Contact</a></li>
										
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Career
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=develop&about_mass=Developers Career&page_folder=Control Panel&file_name=career_mail'>"; ?>Career</a></li>
										
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Online Requriment
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Home&page_folder=Manage Banner&file_name=internal_img'>"; ?>Internal</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Home&page_folder=Manage Banner&file_name=external_img'>"; ?>External</a></li>
										
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Login
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Home&page_folder=Manage Banner&file_name=internal_img'>"; ?>login</a></li>
										 <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=develop&about_mass=Developers Reset password&page_folder=Control Panel&file_name=mail_password'>"; ?>Reset password</a></li>
										
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       My search engine
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Home&page_folder=Manage Banner&file_name=internal_img'>"; ?>Internal</a></li>
										<li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=banner&about_mass=Home&page_folder=Manage Banner&file_name=external_img'>"; ?>External</a></li>
										
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       My Feedback
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=develop&about_mass=Developers Feedback&page_folder=Control Panel&file_name=feedback_mail'>"; ?>Feedback</a></li>
										
                                    </ul>
                                </div>
								
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Disclaimer
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=page_about&about_mass=Disclaimer&page_folder=Control Panel&file_name=Disclaimer'>"; ?>Disclaimer</a></li>
                                    </ul>
                                </div>
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Manage database
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=manage_database&about_mass=manage_database&page_folder=Control Panel (Manage database)&file_name=manage_database'>"; ?>Mannage</a></li>
                                    </ul>
                                </div>
								<div class="btn-group">
                                    <br>
									<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown" style="padding:20px 50px;">
                                       Send mail
                                        <span class="caret"></span>
                                    </button>
                                    <ul class="dropdown-menu pull-right" role="menu">
                                        <li><?php echo "<a href='website.php?PHPISID=$remoteaddress&home=mailss&about_mass=manage_database&page_folder=Control Panel (Manage database)&file_name=manage_database'>"; ?>Send mail</a></li>
                                    </ul>
                                </div>
								<!-----------end body------>
							</div>
						</div>
					</div>
              
				</div>
				
				
				
            </div>
		

			
			
			
			
		</div>
            
        </div>
		