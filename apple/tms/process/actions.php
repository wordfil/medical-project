<?php
ob_start();
session_start();
include_once('../includes/connection.php');
if($_SESSION['FORMPOS']=='MRFORM')
	{
		$mfid=$_GET['mfid'];			
		$rst = mysql_query("DELETE FROM mrform WHERE id=$mfid",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../mr-reporting-url.php?info=M R Reporting form url successfully deleted.");
		exit();
	}
	
	
if($_SESSION['FORMPOS']=='UPLOADMGR')
	{	
	if($_SESSION['ISADMIN']<>'TRUE'){
		header("Location: ../index.php?err=You are not authorised to access this page.");	
		}
	
	$sysfilename=$_GET['sysfile'];
	$fileid=$_GET['id'];
	
		$Path = '../empfiles/'.$sysfilename;
	if (file_exists($Path)){
		if (unlink($Path)) {   
			echo "success";
			$rst = mysql_query("UPDATE uploaddetails SET active='N' WHERE id=$fileid",$con);
			mysql_free_result($rst);
			$_SESSION['FORMPOS']='';	
			header("Location: ../upload-manager.php?info=File successfully deleted.");
			exit();
		
		} else {
			header("Location: ../upload-manager.php?err=Some error occurred.");
			exit();   
		}   
	} else {
		header("Location: ../upload-manager.php?err=File does not exist.");
		exit();   
	}
}


if($_SESSION['FORMPOS']=='ROP')
	{
		$pid=$_GET['pid'];			
		$rst = mysql_query("DELETE FROM rfpolicies WHERE id=$pid",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../reinforcement-of-policies.php?info=Policy successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='STA')
	{
		$pid=$_GET['pid'];			
		$rst = mysql_query("DELETE FROM salestips WHERE id=$pid",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../sales-tips.php?info=Sale tips/advice successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='CT')
	{
		$pid=$_GET['pid'];			
		$rst = mysql_query("DELETE FROM customertestimonials WHERE id=$pid",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../customer-testimonials.php?info=Testimonial successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='ATP')
	{
		$id=$_GET['id'];			
		$rst = mysql_query("DELETE FROM training WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../available-training-programs.php?info=Training details successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='ASSYS')
	{
		$id=$_GET['id'];			
		$rst = mysql_query("DELETE FROM supportsystem WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../available-support-systems.php?info=Details successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='FAQS')
	{
		$id=$_GET['id'];			
		$rst = mysql_query("DELETE FROM faqs WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../faqs.php?info=FAQs details successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='INCPROG')
	{
		$id=$_GET['id'];			
		$rst = mysql_query("DELETE FROM incentive WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../incentive-prog.php?info=Incentive program details successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='PRODUCTUP')
	{
		$id=$_GET['id'];			
		$rst = mysql_query("DELETE FROM productinfo WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../product-updates.php?info=Product successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='DBSTATE')
	{
		$id=$_GET['id'];
		
		$rst = mysql_query("SELECT count(1) as cnt FROM employeedata WHERE stateid=$id",$con);
			$show = mysql_fetch_assoc($rst);
			$cnt=$show['cnt'];
			mysql_free_result($rst);
		
		if($cnt>0){			
		$_SESSION['FORMPOS']='';
		header("Location: ../state-master.php?err=Can not delete this state as it is already used in employee registration.");
		exit();
			}
		
		
		mysql_free_result($rst);
		$rst = mysql_query("DELETE FROM dbstate WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../state-master.php?info=State successfully deleted.");
		exit();
	}
	
if($_SESSION['FORMPOS']=='DBCITY')
	{
		$id=$_GET['id'];
		
		$rst = mysql_query("SELECT count(1) as cnt FROM employeedata WHERE cityid=$id",$con);
			$show = mysql_fetch_assoc($rst);
			$cnt=$show['cnt'];
			mysql_free_result($rst);
		
		if($cnt>0){			
		$_SESSION['FORMPOS']='';
		header("Location: ../city-master.php?err=Can not delete this city as it is already used in employee registration.");
		exit();
			}
		
		
		mysql_free_result($rst);
		$rst = mysql_query("DELETE FROM dbcity WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../city-master.php?info=City successfully deleted.");
		exit();
	}

if($_SESSION['FORMPOS']=='DBROLE')
	{
		$id=$_GET['id'];
		
		$rst = mysql_query("SELECT count(1) as cnt FROM employeedata WHERE designationid=$id OR immidiateid=$id",$con);
			$show = mysql_fetch_assoc($rst);
			$cnt=$show['cnt'];
			mysql_free_result($rst);
		
		if($cnt>0){			
		$_SESSION['FORMPOS']='';
		header("Location: ../role-master.php?err=Can not delete this role/designation as it is already used in employee registration.");
		exit();
			}
		
		
		mysql_free_result($rst);
		$rst = mysql_query("DELETE FROM designation WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../role-master.php?info=Role/designation successfully deleted.");
		exit();
	}
	
if($_SESSION['FORMPOS']=='VIEWEMP')
	{
		$id=$_GET['id'];		
		$active=$_GET['act'];
		if($active=='Y'){
			$active='N';
			}else{
				$active='Y';
				}
		
		mysql_free_result($rst);
		$rst = mysql_query("UPDATE employeedata SET active='$active' WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../view-employee.php?info=Status successfully updated.");
		exit();
	}
	
if($_SESSION['FORMPOS']=='VDBTOPA')
	{
		$topid=$_GET['topid'];			
		$rst = mysql_query("DELETE FROM toprank WHERE id=$topid",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../view-recognition-high-achievers.php?info=Data successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='DBPERMISSION'){
		$id=$_GET['id'];
		$uid=$_GET['uid'];
		$rst = mysql_query("DELETE FROM permission_manager WHERE id=$id",$con);
		mysql_free_result($rst);
		
		$rst = mysql_query("UPDATE employeedata SET extpermissions='U' WHERE id=$uid",$con);
		mysql_free_result($rst);
		
		$_SESSION['FORMPOS']='';
		header("Location: ../permission-master.php?info=Data successfully deleted.");
		exit();	
	}
if($_SESSION['FORMPOS']=='AOD')
	{
		$id=$_GET['id'];			
		$rst = mysql_query("DELETE FROM alertofdeadlines WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../alert-of-deadlines.php?info=Alert details successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='INBOX' ||$_SESSION['FORMPOS']=='DASHHOME')
	{
		$id=$_GET['mid'];			
		$rst = mysql_query("DELETE FROM admin_inbox WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../inbox.php?info=Message successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='HISTORY')
	{
		$id=$_GET['mid'];
		if($_GET['action']=='all'){
			$rst = mysql_query("DELETE FROM logindata",$con);
		}else{
			$rst = mysql_query("DELETE FROM logindata WHERE id=$id",$con);
			}
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../history.php?info=Message successfully deleted.");
		exit();
	}
if($_SESSION['FORMPOS']=='MRAREA')
	{
		$id=$_GET['id'];
		$rst = mysql_query("DELETE FROM mrarea WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../headquarter-master.php?info=Data successfully deleted.");
		exit();
	}
?>