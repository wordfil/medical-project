<?php
ob_start();
session_start();
include_once('../includes/connection.php');
include_once('includes/adminarea.php');
	
if($_SESSION['FORMPOS']=='UPLOADMGR')
{
	$filename=$_FILES["fileToUpload"]["name"];
	
	if(strlen($filename)>0)
	{
		$target_dir = "../empfiles/";
		$getfiletype=substr($filename, strpos($filename, ".") + 1);
		
		$rst = mysql_query("SELECT MAX(id) AS maxid FROM uploaddetails",$con);
			$show = mysql_fetch_assoc($rst);
			$maxid=$show['maxid']+1;
		mysql_free_result($rst);
		
		//$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
		$target_file = $target_dir . basename($maxid.'.'. $getfiletype);
		
		$uploadOk = 1;
		$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
		// Check if image file is a actual image or fake image
		if(isset($_POST["submit"])) {
			$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
			if($check !== false) {
				echo "File is an image - " . $check["mime"] . ".";
				$uploadOk = 1;
			} else {
				echo "File is not an image or file type not supported";
				$uploadOk = 0;
			}
		}
		// Check if file already exists
		if (file_exists($target_file)) {
			$err=1;
			$errmsg=  "Sorry, file with same name already exists.";
			$uploadOk = 0;
		}
		// Check file size
		if ($_FILES["fileToUpload"]["size"] > 10000000) {
			$err=1;
			$errmsg=  "Sorry, your file is too large.";
			$uploadOk = 0;
		}
		// Allow certain file formats
		if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "doc" && $imageFileType != "ppt" && $imageFileType != "pdf" && $imageFileType != "docx" && $imageFileType != "pptx" && $imageFileType != "xls" && $imageFileType != "xlsx"
		&& $imageFileType != "gif" ) {
			$err=1;
			$errmsg=  "Sorry, only DOC, DOCX, PPT, PPTX, PDF, XLS, XLSX, JPG, JPEG, PNG & GIF files are allowed.";
			$uploadOk = 0;
		}
		// Check if $uploadOk is set to 0 by an error
				if ($uploadOk == 0) {
					//$err=1;
					//$errmsg= "Sorry, your file was not uploaded.";
					header("Location: ../upload-manager.php?err=".$errmsg);
					exit();
				// if everything is ok, try to upload file
				} else {
					if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
						//echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
						//$picname=$_FILES["fileToUpload"]["name"];
					} else {
						$err=1;
						$errmsg= "Sorry, there was an error uploading your file.";
					}
				}
		
				if($err==1){
					header("Location: ../upload-manager.php?err=".$errmsg);
					exit();
									
				// Image upload component
				}
				
//$getfiletype=substr($filename, -3);
$getfiletype=substr($filename, strpos($filename, ".") + 1);
$userid=$_SESSION['userid'];
$newfilename=$maxid.'.'. $getfiletype;
$rst = mysql_query("INSERT into uploaddetails(filename,sysfiletype,sysdate,sysip,userid)values('$newfilename','$getfiletype','$sysdate','$sysip',$userid)",$con);
mysql_free_result($rst);

$_SESSION['FORMPOS']='';	
header("Location: ../upload-manager.php?info=File has been uploaded successfully.");				
	}else{
		$_SESSION['FORMPOS']='';	
		header("Location: ../upload-manager.php?err=File name not found.");
		}
}
?>