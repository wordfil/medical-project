<?php
ob_start();
session_start();

include_once('../includes/connection.php');

if($_SESSION['FORMPOS']=='ROP')
{
		$pid=$_GET['pid'];
		$editor=$_POST['editor'];
		$policytitle=$_POST['policytitle'];

		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}
	
		$attachment=$_POST['addattachment'];
		if($attachment=='on'){
			$attachment='Y';
			}else{
			$attachment='N';	
				}

		$forceopen=$_POST['forceopen'];
		if($forceopen=='on'){
			$forceopen='Y';
			}else{
			$forceopen='N';	
				}
		
		$fileattached=$_POST['attachedfiles'];
		$fileids='';
		foreach ($_POST['attachedfiles'] as $names)
		{
        $fileids=$fileids.$names.',';
		}
		$fileids=rtrim($fileids, ",");
		
		//echo('Title- '.$policytitle.'<Br> Attachment- '.$attachment.'<Br> Show on dashboard- '.$showondashboard.'<Br> Forceopen- '.$forceopen.'<Br> Attached files- '.$fileids);
		//exit();
			
		$rst = mysql_query("UPDATE rfpolicies SET policies='$editor',attachmentids='$fileids',sysdate='$sysdate',sysip='$sysip',policytitle='$policytitle',isanyattachment='$attachment',forceopen='$forceopen',active='$isactive' WHERE id=$pid",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../edit-reinforcement-of-policies.php?id=".$pid."&info=Policy has been updated successfully.");
}

if($_SESSION['FORMPOS']=='STA')
{
		$pid=$_GET['pid'];
		$editor=$_POST['editor'];
		$policytitle=$_POST['policytitle'];

		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}
	
		$attachment=$_POST['addattachment'];
		if($attachment=='on'){
			$attachment='Y';
			}else{
			$attachment='N';	
				}

		$forceopen=$_POST['forceopen'];
		if($forceopen=='on'){
			$forceopen='Y';
			}else{
			$forceopen='N';	
				}
		
		$fileattached=$_POST['attachedfiles'];
		$fileids='';
		foreach ($_POST['attachedfiles'] as $names)
		{
        $fileids=$fileids.$names.',';
		}
		$fileids=rtrim($fileids, ",");
		
		//echo('Title- '.$policytitle.'<Br> Attachment- '.$attachment.'<Br> Show on dashboard- '.$showondashboard.'<Br> Forceopen- '.$forceopen.'<Br> Attached files- '.$fileids);
		//exit();
			
		$rst = mysql_query("UPDATE salestips SET policies='$editor',attachmentids='$fileids',sysdate='$sysdate',sysip='$sysip',policytitle='$policytitle',isanyattachment='$attachment',forceopen='$forceopen',active='$isactive' WHERE id=$pid",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../edit-sales-tips.php?id=".$pid."&info=Sale tips/advice has been updated successfully.");
}

if($_SESSION['FORMPOS']=='CT')
{
		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}	
		$title=$_POST['title'];
		$testimonial=$_POST['testimonial'];
		$ctid=$_GET['ctid'];
		
		$rst = mysql_query("UPDATE customertestimonials SET testimonial='$testimonial',sysdate='$sysdate',sysip='$sysip',title='$title',active='$isactive' WHERE id=$ctid",$con);
		mysql_free_result($rst);
		
		$_SESSION['FORMPOS']='';
		header("Location: ../edit-customer-testimonials.php?ctid=".$ctid."&info=Testimonial has been updated successfully.");
		
}
if($_SESSION['FORMPOS']=='DBSTATE')
{
		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}	
		$state=$_POST['state'];
		$id=$_GET['id'];
		
		$rst = mysql_query("SELECT count(1) as cnt FROM dbstate WHERE statename='$state' AND id<>$id",$con);
			$show = mysql_fetch_assoc($rst);
			$cnt=$show['cnt'];
		mysql_free_result($rst);
		
		if($cnt==1){
			$_SESSION['FORMPOS']='';
			header("Location: ../edit-state-master.php?id=".$id."&err=State name already found, please choose different name.");
			exit();
			}
		
		$rst = mysql_query("UPDATE dbstate SET statename='$state',active='$isactive' WHERE id=$id",$con);
		mysql_free_result($rst);
		
		$_SESSION['FORMPOS']='';
		header("Location: ../state-master.php?info=State has been updated successfully.");
		
}
if($_SESSION['FORMPOS']=='DBCITY')
{
		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}	
		$cityname=$_POST['cityname'];
		$id=$_GET['id'];
		$stateid=$_POST['state'];
		//echo($stateid);
		//exit();
		
		
		$rst = mysql_query("SELECT count(1) as cnt FROM dbcity WHERE city='$cityname' AND stateid=$stateid AND id<>$id",$con);
			$show = mysql_fetch_assoc($rst);
			$cnt=$show['cnt'];
		mysql_free_result($rst);
		
		if($cnt==1){
			$_SESSION['FORMPOS']='';
			header("Location: ../edit-city-master.php?id=".$id."&err=Duplicate city name already found, please choose different name.");
			exit();
			}
		
		
		$rst = mysql_query("UPDATE dbcity SET stateid=$stateid, city='$cityname',active='$isactive' WHERE id=$id",$con);
		mysql_free_result($rst);
		
		$rst = mysql_query("UPDATE employeedata SET stateid=$stateid WHERE cityid=$id",$con);
		mysql_free_result($rst);
		
		$_SESSION['FORMPOS']='';
		header("Location: ../city-master.php?info=City name has been updated successfully.");
		
}

if($_SESSION['FORMPOS']=='DBROLE')
{
		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}	
		$designation=$_POST['designation'];
		$id=$_GET['id'];

		//echo($designation);
		//exit();		
		
		$rst = mysql_query("SELECT count(1) as cnt FROM designation WHERE designation='$designation' AND id<>$id",$con);
			$show = mysql_fetch_assoc($rst);
			$cnt=$show['cnt'];
		mysql_free_result($rst);
		
		if($cnt==1){
			$_SESSION['FORMPOS']='';
			header("Location: ../edit-role-master.php?id=".$id."&err=Duplicate designation/role already found, please choose different name.");
			exit();
			}
		
		
		$rst = mysql_query("UPDATE designation SET designation='$designation',active='$isactive' WHERE id=$id",$con);
		mysql_free_result($rst);
		
		$_SESSION['FORMPOS']='';
		header("Location: ../role-master.php?info=Designation has been updated successfully.");
		
}

if($_SESSION['FORMPOS']=='ATP')
{
		$id=$_GET['id'];
		$editor=$_POST['editor'];
		$policytitle=$_POST['policytitle'];

		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}
	
		$attachment=$_POST['addattachment'];
		if($attachment=='on'){
			$attachment='Y';
			}else{
			$attachment='N';	
				}

		$forceopen=$_POST['forceopen'];
		if($forceopen=='on'){
			$forceopen='Y';
			}else{
			$forceopen='N';	
				}
		
		$fileattached=$_POST['attachedfiles'];
		$fileids='';
		foreach ($_POST['attachedfiles'] as $names)
		{
        $fileids=$fileids.$names.',';
		}
		$fileids=rtrim($fileids, ",");
		
		//echo('Title- '.$policytitle.'<Br> Attachment- '.$attachment.'<Br> Show on dashboard- '.$showondashboard.'<Br> Forceopen- '.$forceopen.'<Br> Attached files- '.$fileids);
		//exit();
			
		$rst = mysql_query("UPDATE training SET policies='$editor',attachmentids='$fileids',sysdate='$sysdate',sysip='$sysip',policytitle='$policytitle',isanyattachment='$attachment',forceopen='$forceopen',active='$isactive' WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../edit-available-training-programs.php?id=".$id."&info=Training information has been updated successfully.");
}
if($_SESSION['FORMPOS']=='ASSYS')
{
		$id=$_GET['id'];
		$editor=$_POST['editor'];
		$policytitle=$_POST['policytitle'];

		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}
	
		$attachment=$_POST['addattachment'];
		if($attachment=='on'){
			$attachment='Y';
			}else{
			$attachment='N';	
				}

		$forceopen=$_POST['forceopen'];
		if($forceopen=='on'){
			$forceopen='Y';
			}else{
			$forceopen='N';	
				}
		
		$fileattached=$_POST['attachedfiles'];
		$fileids='';
		foreach ($_POST['attachedfiles'] as $names)
		{
        $fileids=$fileids.$names.',';
		}
		$fileids=rtrim($fileids, ",");
		
		//echo('Title- '.$policytitle.'<Br> Attachment- '.$attachment.'<Br> Show on dashboard- '.$showondashboard.'<Br> Forceopen- '.$forceopen.'<Br> Attached files- '.$fileids);
		//exit();
			
		$rst = mysql_query("UPDATE supportsystem SET policies='$editor',attachmentids='$fileids',sysdate='$sysdate',sysip='$sysip',policytitle='$policytitle',isanyattachment='$attachment',forceopen='$forceopen',active='$isactive' WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../edit-available-support-systems.php?id=".$id."&info=Details has been updated successfully.");
}

if($_SESSION['FORMPOS']=='FAQS')
{
		$id=$_GET['id'];
		$editor=$_POST['editor'];
		$policytitle=$_POST['policytitle'];

		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}
	
		$attachment=$_POST['addattachment'];
		if($attachment=='on'){
			$attachment='Y';
			}else{
			$attachment='N';	
				}

		$forceopen=$_POST['forceopen'];
		if($forceopen=='on'){
			$forceopen='Y';
			}else{
			$forceopen='N';	
				}
		
		$fileattached=$_POST['attachedfiles'];
		$fileids='';
		foreach ($_POST['attachedfiles'] as $names)
		{
        $fileids=$fileids.$names.',';
		}
		$fileids=rtrim($fileids, ",");
		
		//echo('Title- '.$policytitle.'<Br> Attachment- '.$attachment.'<Br> Show on dashboard- '.$showondashboard.'<Br> Forceopen- '.$forceopen.'<Br> Attached files- '.$fileids);
		//exit();
			
		$rst = mysql_query("UPDATE faqs SET policies='$editor',attachmentids='$fileids',sysdate='$sysdate',sysip='$sysip',policytitle='$policytitle',isanyattachment='$attachment',forceopen='$forceopen',active='$isactive' WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../edit-faqs.php?id=".$id."&info=FAQs has been updated successfully.");
}

if($_SESSION['FORMPOS']=='INCPROG')
{
		$id=$_GET['id'];
		$editor=$_POST['editor'];
		$policytitle=$_POST['policytitle'];

		$isactive=$_POST['isactive'];
		if($isactive=='on'){
			$isactive='Y';
			}else{
			$isactive='N';	
				}
	
		$attachment=$_POST['addattachment'];
		if($attachment=='on'){
			$attachment='Y';
			}else{
			$attachment='N';	
				}

		$forceopen=$_POST['forceopen'];
		if($forceopen=='on'){
			$forceopen='Y';
			}else{
			$forceopen='N';	
				}
		
		$fileattached=$_POST['attachedfiles'];
		if(isset($_POST['attachedfiles'])) {	
			$fileids='';
			foreach ($_POST['attachedfiles'] as $names)
			{
				$fileids=$fileids.$names.',';
			}
				$fileids=rtrim($fileids, ",");
		}
		
		

		$empids=$_POST['emps'];
		if (isset($_POST['emps'])) {

                $ids='';
				foreach ($_POST['emps'] as $empnames)
				{
					$ids=$ids.$empnames.',';
				}
					$ids=rtrim($ids, ",");
            }
		
		//echo('Title- '.$policytitle.'<Br> Attachment- '.$attachment.'<Br> Show on dashboard- '.$showondashboard.'<Br> Forceopen- '.$forceopen.'<Br> Attached files- '.$fileids);
		//exit();
			
		$rst = mysql_query("UPDATE incentive SET policies='$editor',attachmentids='$fileids',sysdate='$sysdate',sysip='$sysip',policytitle='$policytitle',isanyattachment='$attachment',forceopen='$forceopen',active='$isactive',empids='$ids' WHERE id=$id",$con);
		mysql_free_result($rst);
		$_SESSION['FORMPOS']='';
		header("Location: ../edit-incentive-prog.php?id=".$id."&info=Incentive program details has been updated successfully.");
}

if($_SESSION['FORMPOS']=='EDITPASSWORD'){
		
		$passwordnew=$_POST['password'];
		$userid=$_SESSION['userid'];
		
		$rst = mysql_query("UPDATE employeedata SET epassword='$passwordnew',sysdatetime='$sysdate' WHERE id=$userid",$con);
		mysql_free_result($rst);
		
		$_SESSION['FORMPOS']='';
		header("Location: ../".$rootdir."edit-password.php?info=Password has been updated successfully.");
		
}

if($_SESSION['FORMPOS']=='VDBTOPA')
{

		$rank=$_POST['rank'];
		$description=$_POST['description'];
		$widetype=$_POST['widetype'];
		$id=$_POST['dbid'];		
		
		$rst = mysql_query("UPDATE toprank SET rank=$rank, description='$description', widetype=$widetype WHERE id=$id",$con);
		mysql_free_result($rst);
		
		$_SESSION['FORMPOS']='';
		header("Location: ../view-recognition-high-achievers.php?info=Data has been updated successfully.");
		
}


?>


