<?php
ob_start();
session_start();
include_once('includes/connection.php');
$_SESSION['FORMPOS']='PMASTER';
include_once('includes/admin-permissions.php');
$getdate=$_SESSION['getdate'];
	if($_SESSION['$getempid']==''){
		$getempid='-1';
	}else{
		$getempid=$_SESSION['$getempid'];
	}
$ismonth=$_SESSION['$ismonth'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
	<script language="javascript">
        function addtargetrows(param){
            window.location.href = "target-master.php?mgrid="+param;
        }
    </script>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h3>Performance Statistics</h3>
<?php include('includes/admin-alerts.php');?>     

 <div class="form-group">
 <form action="process/get-performance-sessions.php" form name="datesubmit" id="datesubmit" method="POST">
 <label class="control-label">Select employee name</label>  
<div class="row">
<div class="col-sm-3">
 <Select name="empname" id="empname" class="form-control">
          <Option value="0">All</Option>
        	<?php 
				$rst = mysql_query("SELECT *,E.empid as empid FROM emptarget AS E INNER JOIN employeedata as EMP ON E.empid=EMP.id INNER JOIN designation as D ON E.designationid=D.id GROUP BY E.empid",$con);
				
				while($show = mysql_fetch_object($rst))
				{?>
            	<Option value="<?php echo($show->empid);?>" <?php if(($show->empid)==$getempid){echo('selected');}?>><?php echo($show->firstname.' '.$show->lastname.' {'.$show->designation.'}');?></Option>
            <?php } mysql_free_result($rst);?>
        </Select>
</div>
</div> 
    <label class="control-label">Show targets set for *</label>  
        <div class="row">
        <div class="col-sm-3">
        
            <div class="input-append date" id="datepicker" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy" data-date-minviewmode="months" data-date-viewmode="months">
            <span class="add-on">
            <input name="datenew" type="text" class="span2 form-control" id="datenew" size="10" readonly="" data-bind="value: Customer.DateOfBirth" value="<?php echo($getdate);?>"/>
            </span>
            </div>
             <div class="radio">
 			<label>
            <input type="radio" id="monthpat" name="monthpat" value="Y" <?php if($ismonth=='Y'){echo('checked');}?><?php if($ismonth==''){echo('checked');}?>>             
            Include month in search
 			</label>

 			<label>
            <input type="radio" id="monthpat2" name="monthpat" value="N" <?php if($ismonth=='N'){echo('checked');}?>>
            Exclude month in search
 			</label>
            </div>        
          </div>
          <input type="submit" value="Get Targets" class="btn-primary btn btn-warning">      
        </div>
        
</form>
</div> 

   <div class="table-responsive">
   
    <div class="input-group"> <span class="input-group-addon">Filter</span>
    <input id="filter" type="text" class="form-control col-sm-3" placeholder="Type here...">
   
</div>

<div id="divTableDataHolder">
  <table class="table table-hover table-striped searchable">
      <tr class="success">
        <td><strong>Sno</strong></td>
        <td><strong>Emp Code</strong></td>
        <td><strong>Name</strong></td>
        <td><strong>Month</strong></td>
        <td><strong>Division</strong></td>        
        <td><strong>Description</strong></td>
        <td><strong>Target</strong></td>
        <td><strong>Target Achieved</strong></td>
        <td><strong>Overall</strong></td>
        <td></td>
      </tr>
      
      <?php
	  
	  if($getempid==0){
		  if($ismonth=='Y'){
			 $sqlquery="SELECT *, DC.id as cityid, ET.id as dbid, ET.active as etactive FROM emptarget AS ET INNER JOIN employeedata AS E ON E.id=ET.empid INNER JOIN designation AS D ON D.id=ET.designationid INNER JOIN dbcity AS DC ON ET.cityid=DC.id WHERE ET.targetdate='$getdate' ORDER BY ET.empid, ET.cityid DESC";	
			  }else{
			$sqlquery="SELECT *, DC.id as cityid, ET.id as dbid, ET.active as etactive FROM emptarget AS ET INNER JOIN employeedata AS E ON E.id=ET.empid INNER JOIN designation AS D ON D.id=ET.designationid INNER JOIN dbcity AS DC ON ET.cityid=DC.id ORDER BY ET.empid, ET.cityid DESC";	
				  }

	  }else{
		  if($ismonth=='Y'){
		 	$sqlquery="SELECT *, DC.id as cityid, ET.id as dbid, ET.active as etactive FROM emptarget AS ET INNER JOIN employeedata AS E ON E.id=ET.empid INNER JOIN designation AS D ON D.id=ET.designationid INNER JOIN dbcity AS DC ON ET.cityid=DC.id WHERE ET.targetdate='$getdate' AND ET.empid=$getempid ORDER BY ET.empid, ET.cityid DESC";
		  }else{
			$sqlquery="SELECT *, DC.id as cityid, ET.id as dbid, ET.active as etactive FROM emptarget AS ET INNER JOIN employeedata AS E ON E.id=ET.empid INNER JOIN designation AS D ON D.id=ET.designationid INNER JOIN dbcity AS DC ON ET.cityid=DC.id WHERE ET.empid=$getempid ORDER BY ET.empid, ET.cityid DESC";  
			  }
	  }
		$rst = mysql_query($sqlquery,$con);
		$loopi=1;
		$targettotal=0;
		$targettotaldone=0;
				while($show = mysql_fetch_object($rst))
				
					{
						?>
      <tr class="small">
        <td><?php echo($loopi);?>
          <input name="empid<?php echo($loopi);?>" type="hidden" id="empid<?php echo($loopi);?>" value="<?php echo($show->empid);?>" class="form-control">
          <input name="dbid<?php echo($loopi);?>" type="hidden" id="dbid<?php echo($loopi);?>" value="<?php echo($show->dbid);?>" class="form-control">
          
          </td>

          <td><?php echo($show->empcode);?>
          </td>
          
        <td>
		<?php echo($show->firstname .' '.$show->lastname);?><br>
        <?php echo($show->designation);?></td>
        <td>
		<?php 
		$mydate =$show->targetdate;
		$month = date("M",strtotime($mydate));
		$year = date("Y",strtotime($mydate));
		?>
        <?php echo($month.' '.$year);?>
          </td>
                         
        <td>
        <?php echo($show->city);?>
        </td>
        
         <td style="font-size:10px">
         	<?php echo(nl2br($show->description));?>
         </td>
         
        <td>
        <?php echo($show->target);?>
        </td>
        
        <td><?php echo($show->targetdone);?></td>
        <td>
        <?php 
		$ta=$show->target;
		$td=$show->targetdone;
		if($ta>0){
			$targettotal=$targettotal+$ta;
			$targettotaldone=$targettotaldone+$td;
			
			$ts=$ta-$td;
			$tspercent=($td/$ta)*100;
			if($tspercent<40){
			echo '<font color=red>'.number_format((float)$tspercent, 2, '.', '').'%<br>'.$ts.'</font>';
			}else{
				echo '<font color=green>'.number_format((float)$tspercent, 2, '.', '').'%<br>'.$ts.'</font>';
				}
		}
		?>
        </td>
        <td>
        <a href="alert-of-deadlines.php?optext=<?php echo('{'.$show->designation.'} '.$show->firstname.' '.$show->lastname);?>&opval=<?php echo $show->empid;?>" type="button" class="btn btn-default btn-sm" <?php if($ta>0){ echo('');}else{echo('disabled');}?>>
          <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Send alert
        </a>        
        
        </td>
      </tr>
      
              <?php 
			  $loopi=$loopi+1;
			  $getdate= $show->targetdate;
			  }
			  $_SESSION['loopi']='';
			  $_SESSION['loopi']=$loopi-1;
			  ?>
              

       <tr>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td><strong><?php echo $targettotal;?></strong></td>
       <td><strong><?php echo $targettotaldone;?></strong></td>
       <td>
       <strong>
       <?php 
	   if($targettotal>0){
		   $tspercent=($targettotaldone/$targettotal)*100;
		   if($tspercent<60){
			echo '<font color=red>'.number_format((float)$tspercent, 2, '.', '').'%</font>';
			}else{
				echo '<font color=green>'.number_format((float)$tspercent, 2, '.', '').'%</font>';
				}
	   }
	   ?>
       </strong>
       </td>
<td></td>
       </tr>       
    </table>
</div>
 <button id="myButtonControlID" class="btn btn-success pull-right"><i class="fa fa-2x fa-file-excel-o"></i> Export to Excel</button>

</div>
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
var d = new Date();
//$('#datenew').val('01'+'-'+(d.getMonth()+1+'-'+d.getFullYear()));
$('#datepicker').datepicker('hide');
});//]]> 

function setdescription(param){
	//alert(param);
	$('#description'+param).val($('#description'+param).val()+$('#product'+param).find('option:selected').text() + "- \n");
	}
	
$('#gettar').click(function() {
    $("#datesubmit").submit();
});

(function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));

function isNumberKey(evt)
      {
		var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode != 47 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;
 
          return true;
}

$("[id$=myButtonControlID]").click(function(e) {
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=divTableDataHolder]').html()));
    e.preventDefault();
});

$('#datesubmit').submit(function() {
	
	$errmsg='';	
	
	if($('#datenew').val().length<=0){
		$('#datenew').focus();
		$errmsg='Please select month';
		}
		
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{
	
	var selObj = document.getElementById('attachedfiles');
	  for (var i=0; i<selObj.options.length; i++) {
		selObj.options[i].selected = true;
	  }
		return true;	
		}
		
});

</script>

</body>
</html>