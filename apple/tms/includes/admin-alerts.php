 <?php if($_GET['err']!=''){?>
    <div class="alert alert-danger" role="alert">
     <a href="#" class="close" data-dismiss="alert">&times;</a>
      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
      <?php echo $_GET['err'];?>
    </div>
<?php }?> 
 
<?php if($_GET['info']!=''){?>
    <div class="alert alert-success" role="alert">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
      <span class="glyphicon glyphicon-saved" aria-hidden="true"></span>
      <?php echo $_GET['info'];?>
    </div>
<?php }?> 