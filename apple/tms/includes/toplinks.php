<?php
if($_SESSION['ISADMIN']=='TRUE'){//echo $userid;?>
<Div class="toplinkbar clearall" align="center">

<?php if(($_SESSION['DBROLE']==1) || ($_SESSION['UPLOADMGR']==1) ||($_SESSION['DBSTATE']==1) ||($_SESSION['DBCITY']==1) ||($_SESSION['DBEMP']==1)||($_SESSION['MRAREA']==1)||($_SESSION['VIEWEMP']==1)||($_SESSION['DBTARGET']==1) || ($_SESSION['DBVTARGET']==1) ||($_SESSION['PMASTER']==1)||($_SESSION['MRFORM']==1) || ($_SESSION['FOOTERLINKS']==1)|| ($_SESSION['DBPERMISSION']==1) || ($_SESSION['INBOX']==1)){?>
	<div class="btn-group" role="group">
           <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Master Data <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">

                    <?php if(($_SESSION['UPLOADMGR'])==1){?><li><a href="upload-manager.php">Upload files</a></li><?php }?>
                    
                    <?php if(($_SESSION['UPLOADMGR'])==1){?><li class="divider"></li><?php }?>
                    
                        <?php if(($_SESSION['DBSTATE'])==1){?><li><a href="state-master.php">Add State</a></li><?php }?>
                        <?php if(($_SESSION['DBCITY'])==1){?><li><a href="city-master.php">Add City</a></li><?php }?>
                        <?php if(($_SESSION['DBROLE'])==1){?><li><a href="role-master.php">Add Roles/Designations</a></li><?php }?>
                        
                         <?php if(($_SESSION['DBROLE']==1) || ($_SESSION['UPLOADMGR']==1) ||($_SESSION['DBSTATE']==1) ||($_SESSION['DBCITY']==1)){?><li class="divider"></li><?php }?>                       

                        <?php if(($_SESSION['DBEMP']==1)||($_SESSION['MRAREA']==1)||($_SESSION['VIEWEMP']==1)){?>
                        <li>
                        	<a href="#">Employee Management</a>
                            <ul>
                            	 <?php if(($_SESSION['DBEMP'])==1){?><li><a href="emp-register.php">Add Empolyee data</a></li><?php }?>
                                 <?php if(($_SESSION['MRAREA'])==1){?><li><a href="headquarter-master.php">Assign Headquarters</a></li><?php }?>
                                 <?php if(($_SESSION['VIEWEMP'])==1){?><li><a href="view-employee.php">View Empolyee data</a></li><?php }?>
                            </ul>
                        </li>
                        <?php }?>
                        
                        
                         <?php if(($_SESSION['DBTARGET']==1) || ($_SESSION['DBVTARGET']==1) ||($_SESSION['PMASTER']==1)){?><li class="divider"></li><?php }?>
                         
                         <?php if(($_SESSION['DBTARGET'])==1){?><li><a href="target-master.php">Add Targets</a></li><?php }?>
                          <?php if(($_SESSION['DBVTARGET'])==1){?><li><a href="view-target-master.php">View Targets</a></li><?php }?>
                          <?php if(($_SESSION['PMASTER'])==1){?><li><a href="performance-target-master.php">Performance Stat</a></li><?php }?>
                         
                         <?php if(($_SESSION['MRFORM']==1) || ($_SESSION['FOOTERLINKS']==1)){?><li class="divider"></li><?php }?>
                         
                        <?php if(($_SESSION['MRFORM'])==1){?> <li><a href="mr-reporting-url.php">Edit M R Reporting URL</a></li><?php }?>
                         <?php if(($_SESSION['FOOTERLINKS'])==1){?><li><a href="footerlink.php">Edit footer links</a></li><?php }?>
                         
                        <?php if(($_SESSION['DBPERMISSION']==1) || ($_SESSION['INBOX']==1)){?><li class="divider"></li><?php }?>
                        
                         <?php if(($_SESSION['DBPERMISSION'])==1){?><li><a href="permission-master.php">Permission Manager</a></li><?php }?>
                         <?php if(($_SESSION['INBOX'])==1){?><li><a href="inbox.php">Inbox</a></li><?php }?>
                      </ul>

          </div>
<?php }?>

          
<?php if(($_SESSION['DBTOPA']==1) || ($_SESSION['VDBTOPA']==1)||($_SESSION['SUCCESSQUOTE']==1) || ($_SESSION['MOTIVATIONAL']==1)|| ($_SESSION['CT']==1)){?>          
          <div class="btn-group" role="group">
           <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Montly updates <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                    	<?php if(($_SESSION['DBTOPA'])==1){?><li><a href="recognition-high-achievers.php">Recognition of high achievers</a></li><?php }?>
                        <?php if(($_SESSION['VDBTOPA'])==1){?><li><a href="view-recognition-high-achievers.php">View highest achievers data</a></li><?php }?>
                        
						<?php if(($_SESSION['SUCCESSQUOTE']==1) || ($show->MOTIVATIONAL==1)|| ($show->CT==1)){?><li class="divider"></li><?php }?>
                        
                       <?php if(($_SESSION['SUCCESSQUOTE'])==1){?> <li><a href="success-quotes.php">Success quotes</a></li><?php }?>
                        <?php if(($_SESSION['MOTIVATIONAL'])==1){?><li><a href="motivational-thoughts.php">Motivational thoughts</a></li>   <?php }?>                     
                        <?php if(($_SESSION['CT'])==1){?><li><a href="customer-testimonials.php">Customer testimonials</a></li><?php }?>
                      </ul>

          </div>
<?php }?>
 
<?php if(($_SESSION['ATP']==1) || ($_SESSION['ASSYS']==1)||($_SESSION['INCPROG']==1)){?>             
          <div class="btn-group" role="group">
           <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Support Tools <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <?php if(($_SESSION['ATP'])==1){?><li><a href="available-training-programs.php">Available training programs</a></li><?php }?>
                        <?php if(($_SESSION['ASSYS'])==1){?><li><a href="available-support-systems.php">Available support systems</a></li><?php }?>
                        <?php if(($_SESSION['INCPROG'])==1){?><li><a href="incentive-prog.php">Incentive programs</a></li><?php }?>
                      </ul>

          </div>
<?php }?>


          
          <?php if(($_SESSION['PRODUCTUP'])==1){?>
          <div class="btn-group" role="group">
            <a href="product-updates.php" role="button" class="btn btn-info btn-large">Products Updates</a>            
          </div>
          <?php }?>
          
          <?php if(($_SESSION['STA'])==1){?>
            <div class="btn-group" role="group">
            <a href="sales-tips.php" role="button" class="btn btn-info btn-large">Sales tips and advice</a>
          </div>
          <?php }?>
          
          <?php if(($_SESSION['ROP'])==1){?>
            <div class="btn-group" role="group">
            <a href="reinforcement-of-policies.php" role="button" class="btn btn-info btn-large">Reinforcement of policies</a>
          </div>
          <?php }?>
          
          <?php if(($_SESSION['AOD'])==1){?>
            <div class="btn-group" role="group">            
            <a href="alert-of-deadlines.php" role="button" class="btn btn-info btn-large">Alert of deadlines</a>
          </div>
          <?php }?>
          
          <?php if(($_SESSION['FAQS'])==1){?>
            <div class="btn-group" role="group">
            <a href="faqs.php" role="button" class="btn btn-info btn-large">FAQs</a>
          </div>
          <?php }?>

</Div>
<?php }?>



<?php //Hyperlinks assigned for user below ?>

<?php if($_SESSION['ISADMIN']=='FALSE'){?>
<Div class="toplinkbar clearall" align="center">

		<div class="btn-group" role="group">
        <?php if($_SESSION['istarget']=='Y'){?>
            <a href="my-stats.php" role="button" class="btn btn-success btn-large">My performance</a>   
          <?php }else{?>
          <a href="my-stats.php" role="button" class="btn btn-success btn-large">Team performance</a>   
          <?php }?>             
          </div>

          <div class="btn-group" role="group">
           <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Montly updates <?php if($trankdate>0){?> <span class="badge alert-danger">*</span><?php }?> <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="achievers-data.php">Recognition of high achievers<?php if($trankdate>0){?> <span class="badge alert-danger">New</span><?php }?></a></li>
                        <li><a href="customer-testimonials.php">Customer testimonials</a></li>
                      </ul>
           </div>
          
          <div class="btn-group" role="group">
           <button type="button" class="btn btn-info dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Support Tools <span class="caret"></span></button>
                    <ul class="dropdown-menu" role="menu">
                        <li><a href="available-training-programs.php">Available training programs</a></li>
                        <li><a href="available-support-systems.php">Available support systems</a></li>
                        <li><a href="incentive-prog.php">Incentive programs</a></li>
                    </ul>
                    
          </div>          
          
          <div class="btn-group" role="group">
            <a href="products-all.php" role="button" class="btn btn-info btn-large">Products Updates</a>            
          </div>
            <div class="btn-group" role="group">
            <a href="sales-tips.php" role="button" class="btn btn-info btn-large">Sales tips and advice</a>
          </div>
            <div class="btn-group" role="group">
            <a href="reinforcement-of-policies.php" role="button" class="btn btn-info btn-large">Reinforcement of policies</a>
          </div>

            <div class="btn-group" role="group">
            <a href="faqs.php" role="button" class="btn btn-info btn-large">FAQs</a>
          </div>
          
          <?php if($_SESSION['adminpowers']=='Y'){?>
          <div class="btn-group" role="group">
            <a href="extuser.php" role="button" class="btn btn-danger btn-large">Login as Admin</a>
          </div>
          <?php }?>

</Div>

<?php }?>