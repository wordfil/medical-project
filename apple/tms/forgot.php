<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='FORGOTPASS';
include_once('includes/connection.php');
if($_SESSION['ISLOGIN']=='TRUE'){
	header("Location: dashboard.php?info=".$_SESSION['username']." you are already logged in. Please logout before new signin");
	}
	
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$randstring=generateRandomString(8);
$_SESSION['captcha']=$randstring;
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<style>
.noselect {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>

<Div id="midsection" class="clearall">

<form id="captchaform" method="post" action="process/checklogin.php">
<Div id="loginform1" class="col-sm-4 col-md-offset-4">
    <div class="panel panel-success">
    <div class="panel-heading"><h3>Forgot your password?</h3></div>       
	<div class="panel-body">
      <?php include('includes/admin-alerts.php');?>
           
      <div class="form-group">   
        <label for="email">My email ID is:</label>
        <input type="email" class="form-control" id="email" placeholder="Enter email" name="email">
      </div>
      
      <div class="form-group">
        <label for="captcha">Type below written characters in the textbox (case insensitive) :</label>
            <input type="text" maxlength="8" name="captcha" id="captcha" class="form-control"> 
    </div>
    
    <div class="form-group">
     <label class="text-success thumbnail h4 noselect alert-info" ><?php echo $randstring;?></label>
      <button type="submit" name="submit" id="submit" class="btn btn-primary pull-right" value="Check">Submit</button>  
    </div>  
      
      
     <p>If you can&#39;t decipher the text, then reload the page to dynamically generate a new one.</p>
     </div>
     </div>
    </div>
    </form> 
</Div>
<?php include_once("includes/footer.php");?>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script type='text/javascript'>//<![CDATA[ 
$('#captchaform').submit(function() {
	
	$errmsg='';					 

	if($('#email').val().length<=0){
		$('#email').focus();
		$errmsg='Password can not be blank\n';
		}
	
	if($('#captcha').val().length<=0){
		$('#captcha').focus();
		$errmsg=$errmsg+'Please enter text string shown in green color \n';
		} 
 
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});
</script>
</body>
</html>