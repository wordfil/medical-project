<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='DBPERMISSION';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
<Div id="loginform2">
      <h2>Grant Permissions</h2>
      
<?php include('includes/admin-alerts.php');?>
     
<form id="form1" name="form1" method="post" action="process/permission-insert.php">
  <div class="form-group col-sm-4">
    <label class="control-label">Select employee *</label>
    <select name="empname" id="empname" class="form-control">
      <option value=0>Select employee</option>
        <?php $rst2 = mysql_query("SELECT *,emp.id as empid FROM employeedata as emp INNER JOIN designation ON emp.designationid=designation.id WHERE emp.active='Y' AND isadmin='N' order by emp.firstname",$con);
                  while($show2= mysql_fetch_object($rst2))
                      {
              ?>
        <option value="<?php echo($show2->empid); ?>"><?php echo($show2->firstname); ?> <?php echo($show2->lastname); ?> [<?php echo($show2->designation); ?>]</option>
        <?php }
        mysql_free_result($rst2);?>
   	</select>
  </div>
   
  <Div class="row col-sm-12">  
  <div class="form-group col-sm-3">
  <label class="control-label">Upload Manager</label>
  <Select size="1" name="uploadmanager" id="uploadmanager" class="form-control">
	<Option value="1" class="bg-success">Grant</Option>
    <Option value="0" class="bg-warning" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">State Master</label>
  <Select size="1" name="statemaster" id="statemaster" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">City Master</label>
  <Select size="1" name="citymaster" id="citymaster" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Add Designations/Roles</label>
  <Select size="1" name="rolemaster" id="rolemaster" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">Employee Registration</label>
  <Select size="1" name="empregister" id="empregister" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Assign Headquarters</label>
  <Select size="1" name="headquarter-master" id="headquarter-master" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">View Employee Data</label>
  <Select size="1" name="view-employee" id="view-employee" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Target Master</label>
  <Select size="1" name="target-master" id="target-master" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div>
  
    <div class="form-group col-sm-3">
  <label class="control-label">View Target Master</label>
  <Select size="1" name="view-target-master" id="view-target-master" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Performance Statistics</label>
  <Select size="1" name="performance-target-master" id="performance-target-master" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">M R Reporting Form</label>
  <Select size="1" name="mr-reporting-url" id="mr-reporting-url" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Recognition Achievers</label>
  <Select size="1" name="recognition-high-achievers" id="recognition-high-achievers" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">View Achievers Data</label>
  <Select size="1" name="view-recognition-high-achievers" id="view-recognition-high-achievers" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Success Quotes</label>
  <Select size="1" name="success-quotes" id="success-quotes" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">Motivational thoughts</label>
  <Select size="1" name="motivational-thoughts" id="motivational-thoughts" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Customer Testimonials</label>
  <Select size="1" name="customer-testimonials" id="customer-testimonials" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">Training Programs</label>
  <Select size="1" name="available-training-programs" id="available-training-programs" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Available support systems</label>
  <Select size="1" name="available-support-systems" id="available-support-systems" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div>  
  
    <div class="form-group col-sm-3">
  <label class="control-label">Incentive programs</label>
  <Select size="1" name="incentive-prog" id="incentive-prog" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Product Updates</label>
  <Select size="1" name="product-updates" id="product-updates" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
      <div class="form-group col-sm-3">
  <label class="control-label">Sales tips and advice</label>
  <Select size="1" name="sales-tips" id="sales-tips" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Reinforcement of policies</label>
  <Select size="1" name="reinforcement-of-policies" id="reinforcement-of-policies" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div>  
  
    <div class="form-group col-sm-3">
  <label class="control-label">Alert of Deadlines</label>
  <Select size="1" name="alert-of-deadlines" id="alert-of-deadlines" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Faqs</label>
  <Select size="1" name="faqs" id="faqs" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">Footer</label>
  <Select size="1" name="footerlink" id="footerlink" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">Inbox</label>
  <Select size="1" name="inbox" id="inbox" class="form-control">
	<Option value="1">Grant</Option>
    <Option value="0" selected>Revoke</Option>
  </Select>
  </div> 
  
  </Div> 		
            
  <div class="form-group col-sm-4">
  <label class="control-label"></label>
  <button type="submit" class="form-control btn-primary">Grant Permissions</button>              
  </div>            


 </form>

<table class="table table-striped">
<tr class="info">
  <td ><strong>Employee Name</strong></td>
  <td ><strong>Date/Time</strong></td>
  <td ><strong>Edit</strong></td>
  <td><strong>Delete</strong></td>
</tr>
 <?php
$rst = mysql_query("SELECT *, P.id AS pid FROM permission_manager AS P INNER JOIN employeedata AS E ON P.empid=E.id WHERE P.empid<>1",$con);
$num_rows = mysql_num_rows($rst);

	if($num_rows>0){
		while($show = mysql_fetch_object($rst))
			{
			?>
            <tr>
              <td><?php echo($show->firstname.' '.$show->lastname);?></td>
              <td><?php echo($show->sysdate.'/'.$show->systime);?></td>
              <td><a href="edit-permission-master.php?pid=<?php echo($show->pid);?>"><span class="glyphicon glyphicon-list-alt"> Details</span></a></td>
              <td><a href="process/actions.php?id=<?php echo($show->pid);?>&uid=<?php echo($show->empid);?>"><span class="glyphicon glyphicon-floppy-remove"> Remove</span></a></td>
            </tr>
			<?php } } ?>

 </table>   
</Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#empname').val()<=0){
		$('#empname').focus();
		$errmsg='Please select employee name';
		}
	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
}); 
</script>

</body>
</html>