<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='FOOTERLINKS';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
      <h2>Edit Footer Bar</h2>
 <?php
	 	$rst = mysql_query("Select * from footerpanel",$con);
	 	$show = mysql_fetch_object($rst);
?>
<?php include('includes/admin-alerts.php');?>

<form id="form" name="form1" method="post" action="process/insertdb.php">

        <div class="col-sm-12">
          <label class="control-label">Footer Title</label>
          <input name="footertitle" type="text" class="form-control" id="footertitle" value="<?php echo($show->footertitle);?>" />
      </div>

    <div class="form-group col-sm-12">
    <label class="control-label">Footer Body</label>
    <textarea name="footerbody" class="form-control" id="footerbody" maxlength="299"><?php echo($show->footerbody);?></textarea>
    </div>
    
    <div class="form-group col-sm-12">
    <label class="control-label">Footer</label>
    <textarea name="footer" class="form-control" id="footer" maxlength="299"><?php echo($show->footer);?></textarea>
    </div>

 
    <div class="col-sm-3">
    <label class="control-label"></label>
    <button type="submit" class="form-control btn-primary">Submit</button>              
    </div>            

 </form>  


</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#quote').val().length<=0){
		$('#quote').focus();
		$errmsg='Quote can not be blank';
		}
	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});
</script>

</body>
</html>