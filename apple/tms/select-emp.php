<?php
ob_start();
session_start();
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">

<body>


    <div class="form-group">
       
       <div class="row">
        		<div class="col-sm-12">
            <label class="control-label">State</label>
            <select name="state" id="state" class="form-control">
            <option value="0">select here</option>
 <?php
    $rst = mysql_query("SELECT * from dbstate WHERE active='Y'",$con);
    $num_rows = mysql_num_rows($rst);
    
        if($num_rows>0){
            while($show = mysql_fetch_object($rst))
                {
?>
             <option value="<?php echo($show->id); ?>"><?php echo($show->statename); ?></option>
<?php } } ?>
            </select>


            <label class="control-label">Division</label>
           <select name="city" id="city" class="form-control">
           <option value="0">select here</option>
           <?php
    $rst = mysql_query("SELECT * from dbcity WHERE active='Y'",$con);
    $num_rows = mysql_num_rows($rst);
    
        if($num_rows>0){
            while($show = mysql_fetch_object($rst))
                {
?>
            <option value="<?php echo($show->id); ?>"><?php echo($show->city); ?></option>
<?php } } ?>
            </select>

            <label class="control-label">Post/designation</label>
            <select class="form-control"  name="designation" id="designation">
            <option value="0">select here</option>
    <?php
    $rst = mysql_query("SELECT * from designation WHERE active='Y'",$con);
    $num_rows = mysql_num_rows($rst);
    
        if($num_rows>0){
            while($show = mysql_fetch_object($rst))
                {

                ?>
               
            <option value="<?php echo($show->id);?>"><?php echo($show->designation);?></option>
                <?php } } ?>
                </select>          
               </div> 
                            
        </div>
    </div>
            
            
            
<div class="form-group">
        <div class="row">
          <div class="col-sm-6">
            <label class="control-label">Search result</label>
             <p><small class="fa fa-keyboard-o"> Use control key to multi-select</small></p>
              <select name="result" id="result" size="20" multiple class="form-control">
			  
			  <?php
    $rst = mysql_query("SELECT *, employeedata.id as eid FROM employeedata INNER JOIN designation as des ON des.id=employeedata.designationid WHERE employeedata.active='Y'",$con);
    $num_rows = mysql_num_rows($rst);
    
        if($num_rows>0){
            while($show = mysql_fetch_object($rst))
                {
				echo('<option value='.$show->eid.'>'.'{'.$show->designation.'} '.$show->firstname. ' '.$show->lastname.'</option>');
					 } } ?>
            </select>
            <button type="button" class="form-control btn-primary" id="btnRight" ><span class="glyphicon glyphicon-circle-arrow-right"> Move >></span></button> 
            </div>                      
            
            <div class="col-sm-6">
            <label class="control-label">Selected employees</label>
            <p><small class="fa fa-keyboard-o"> Use control key to multi-select</small></p>
            <select name="emps[]" multiple="multiple" id="emps" size="20" class="form-control">
            <?php
            if($_SESSION['FORMPOS']=='INCPROG')
			{
				$array =explode(',',$mysqlempids);
				
				$rst = mysql_query("SELECT * FROM employeedata WHERE id IN('".implode("','",$array)."')",$con);
				$num_rows = mysql_num_rows($rst);
    
				if($num_rows>0){
					while($show = mysql_fetch_object($rst))
						{
						$mysqlempid=$show->designationid;	
						$rst2 = mysql_query("SELECT * FROM designation WHERE id=$mysqlempid",$con);
						$show2 = mysql_fetch_object($rst2);	
							echo('<option value='.$show->id.'>'.'{'.$show2->designation.'} '.$show->firstname.' '.$show->lastname.'</option>');
						mysql_free_result($rst2);
						}
						mysql_free_result($rst);
					}	
			}?>
            
            </select>

             <button type="button" class="form-control btn-primary" id="btnLeft"><span class="glyphicon glyphicon-circle-arrow-left"> Move <<</span></button>   
            </div>
            

        </div>
    </div>
		
        <div class="form-group">
        <div class="row">
             <div class="col-sm-12">
             <button type="button" class="form-control btn-success" id="btnSave"><span class="glyphicon glyphicon-save"> Save &amp; Minimize</span></button>   
            </div>
        </div>
        </div>




<script type='text/javascript'>//<![CDATA[ 
$(document).ready(function() {						  
					
        $('#state').change(function(){
            $.ajax({
                type: "GET",
                url: "process/getemployee.php",
                data: 'id=' + $('#state').val()+'&uid=1',
                success: function(msg){
                    $('#city').html(msg);
                }

            }); // Ajax Call
			
			$.ajax({
                type: "GET",
                url: "process/getemployee-callback.php",
                data: 'id=' + $('#state').val()+'&uid=1',
                success: function(msg){
                    $('#result').html(msg);
                }

            }); // Ajax Call
		
        }); //event handler
		
		$('#city').change(function(){
            $.ajax({
                type: "GET",
                url: "process/getemployee.php",
                data: 'id=' + $('#city').val()+'&uid=2&stateid=' + $('#state').val(),
                success: function(msg){
                    $('#designation').html(msg);
                }

            }); // Ajax Call
			$.ajax({
                type: "GET",
                url: "process/getemployee-callback.php",
                data: 'id=' + $('#city').val()+'&uid=2&stateid=' + $('#state').val(),
                success: function(msg){
                    $('#result').html(msg);
                }

            }); // Ajax Call
			
        }); //event handler
		
		$('#designation').change(function(){
			$.ajax({
                type: "GET",
                url: "process/getemployee-callback.php",
                data: 'id=' + $('#designation').val()+'&uid=3&cityid=' + $('#city').val(),
                success: function(msg){
                    $('#result').html(msg);
                }

            }); // Ajax Call
			
        }); //event handler



$('#result').dblclick(function(e) {$('#btnRight').click()});
<?php 
$optext=$_GET['optext'];
$opval=$_GET['opval'];

if($opval<>''){?>
$("#emps").prepend("<option value='<?php echo $opval;?>'><?php echo $optext;?></option>");
<?php }?>
						
$('#btnRight').click(function(e) {
        var selectedOpts = $('#result option:selected');
        if (selectedOpts.length == 0) {
            alert("Nothing to move.");
            e.preventDefault();
        }

        $('#emps').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });

$('#emps').dblclick(function(e) {$('#btnLeft').click()});
    $('#btnLeft').click(function(e) {
        var selectedOpts = $('#emps option:selected');
        if (selectedOpts.length == 0) {
            alert("Nothing to move.");
            e.preventDefault();
        }

        $('#result').append($(selectedOpts).clone());
        $(selectedOpts).remove();
        e.preventDefault();
    });

$('#empdiv').hide();

$('#showemps').click(function(e) {
$('#empdiv').show(500);
$('#showemps').hide();
});

$('#btnSave').click(function(e) {
$('#empdiv').hide(500);
$('#showemps').show();
});

		
    }); //document.ready


</script>

</body>
</html>