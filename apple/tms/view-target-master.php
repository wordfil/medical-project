<?php
ob_start();
session_start();
include_once('includes/connection.php');
$_SESSION['FORMPOS']='DBVTARGET';
include_once('includes/admin-permissions.php');
$getdate=$_GET['datenew'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
	<script language="javascript">
        function addtargetrows(param){
            window.location.href = "target-master.php?mgrid="+param;
        }
    </script>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h3>View Target Master - Edit</h3>
<?php include('includes/admin-alerts.php');?>     

 <div class="form-group">
 <form action="view-target-master.php" form name="datesubmit" id="datesubmit" method="GET">
    <label class="control-label">Show targets set for *</label>  
        <div class="row">
        <div class="col-sm-3">
        
            <div class="input-append date" id="datepicker" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy" data-date-minviewmode="months" data-date-viewmode="months">
            <span class="add-on">
            <input name="datenew" type="text" class="span2 form-control" id="datenew" size="10" readonly="" data-bind="value: Customer.DateOfBirth" />
            </span>  
            </div>        
          </div>
          <input type="submit" value="Get Targets" class="btn-primary btn btn-warning">  
        
        </div>        
</form>
</div>  

<form id="form1" name="form1" method="post" action="process/update-target.php">   
   <div class="table-responsive">
  <table class="table table-hover table-striped">
      <tr class="success">
        <td><strong>Sno</strong></td>
        <td><strong>Emp Code</strong></td>
        <td><strong>Name</strong></td>
        <td><strong>Designation</strong></td>
        <td><strong>Productname</strong></td> 
        <td><strong>Division</strong></td>        
        <td><strong>Description</strong></td>
        <td><strong>Target</strong></td>
        <td><strong>Target Achievement</strong></td>
      </tr>
      
      <?php 
	  			$rst = mysql_query("SELECT *, DC.id as cityid, ET.id as dbid, ET.active as etactive FROM emptarget AS ET INNER JOIN employeedata AS E ON E.id=ET.empid INNER JOIN designation AS D ON D.id=ET.designationid INNER JOIN dbcity AS DC ON ET.cityid=DC.id WHERE ET.targetdate='$getdate' ORDER BY ET.empid, ET.cityid DESC",$con);
				$loopi=1;
				while($show = mysql_fetch_object($rst))
				
					{
						?>
      <tr>
        <td><?php echo($loopi);?>
          <input name="empid<?php echo($loopi);?>" type="hidden" id="empid<?php echo($loopi);?>" value="<?php echo($show->empid);?>" class="form-control">
          <input name="dbid<?php echo($loopi);?>" type="hidden" id="dbid<?php echo($loopi);?>" value="<?php echo($show->dbid);?>" class="form-control">
          
          </td>

          <td><?php echo($show->empcode);?>
          </td>
          
        <td><?php echo($show->firstname);?>
          <input name="desigid<?php echo($loopi);?>" type="hidden" id="desigid<?php echo($loopi);?>" value="<?php echo($show->designationid);?>" class="form-control"></td>
        <td><?php echo($show->designation);?>
          </td>
          
          <td>
          <Select name="product<?php echo($loopi);?>" id="product<?php echo($loopi);?>" class="form-control" onChange="setdescription(<?php echo($loopi);?>);">
          <Option value="0">All</Option>
        	<?php 
				$rst2 = mysql_query("SELECT * FROM productinfo WHERE active='Y'",$con);
				
				while($show2 = mysql_fetch_object($rst2))
				{?>
            	<Option value="<?php echo($show2->id);?>"><?php echo($show2->mediname);?></Option>
            <?php } mysql_free_result($rst2);?>
        </Select>
          </td>
        
        <td>
        <?php echo($show->city);?>
        <Select size="1" name="division<?php echo($loopi);?>" id="division<?php echo($loopi);?>" class="form-control hidden">
			<Option value="<?php echo($show->cityid);?>"><?php echo($show->city);?></Option>
        </Select>
        </td>
        
         <td>
         	<textarea style="font-size:9px; resize:none; width:206px;" name="description<?php echo($loopi);?>" class="form-control" id="description<?php echo($loopi);?>" placeholder="You can add product qty here for record"><?php echo($show->description);?></textarea>
         </td>
         
        <td>
        <input  onkeypress="return isNumberKey(event)" name="target<?php echo($loopi);?>" type="text" id="target<?php echo($loopi);?>" placeholder="only numbers" class="form-control" value="<?php echo($show->target);?>">
        <input <?php if(($show->etactive)=='Y'){?>checked<?php }?> type="checkbox" id="active<?php echo($loopi);?>" name="active<?php echo($loopi);?>" value="Y" aria-label=""> is active
        </td>
        
        <td><input onKeyPress="return isNumberKey(event)" name="targetdone<?php echo($loopi);?>" type="text" id="targetdone<?php echo($loopi);?>" placeholder="only numbers" class="form-control" value="<?php echo($show->targetdone);?>"></td>
        
      </tr>
      
              <?php 
			  $loopi=$loopi+1;
			  $getdate= $show->targetdate;
			  }
			  $_SESSION['loopi']='';
			  $_SESSION['loopi']=$loopi-1;
			  ?>
              

       <tr>
       <td colspan="9">
       <div class="col-sm-12">
       <input name="datenew2" type="hidden" class="span2 form-control" id="datenew2" size="10" readonly="" value="<?php echo($getdate);?>" />
         <input type="submit" name="button" id="button" value="Submit Achievement" class="btn-primary btn pull-right">
         </div>
		</td>
       </tr>       
    </table>
</div>

</form>
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
var d = new Date();
var currentMonth=('0'+(d.getMonth()+1)).slice(-2);
$('#datenew').val('01'+'-'+ currentMonth +'-'+d.getFullYear());
$('#datepicker').datepicker('show');
});//]]> 

function setdescription(param){
	//alert(param);
	$('#description'+param).val($('#description'+param).val()+$('#product'+param).find('option:selected').text() + "- \n");
	}
	
$('#gettar').click(function() {
    $("#datesubmit").submit();
});

function isNumberKey(evt)
      {
		var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode != 47 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;
 
          return true;
}
</script>

</body>
</html>