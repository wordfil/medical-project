<?php
ob_start();
session_start();
include_once('includes/connection.php');
$_SESSION['FORMPOS']='DBTARGET';
include_once('includes/admin-permissions.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
	<script language="javascript">
        function addtargetrows(param){
            window.location.href = "target-master.php?mgrid="+param;
        }
    </script>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h3>Target Master for MR</h3>
<?php include('includes/admin-alerts.php');?> 


<?php
	$tdate=date('01-m-Y');
	$rst = mysql_query("Select Count(1) as cnt from emptarget WHERE targetdate='$tdate' ORDER BY ID DESC",$con);
	$show = mysql_fetch_object($rst);
	$getres=$show->cnt;
?>
 <?php if($getres>0){?>
    <div class="alert alert-danger" role="alert">
     <a href="#" class="close" data-dismiss="alert">&times;</a>
      <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
      Admin, please note that targets already found in the database, you can edit by <a href="view-target-master.php?datenew=<?php echo $tdate;?>">clicking here</a> to avoid duplicate entries.
    </div>
<?php }?> 



    
<form id="form1" name="form1" method="post" action="process/inserttarget.php">   
 <div class="form-group">
        <div class="row">         
        <div class="col-sm-6">
   <label class="control-label">Target set for *</label>  
    <div class="input-append date" id="datepicker" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy" data-date-minviewmode="months" data-date-viewmode="months"><span class="add-on">
    <input name="datenew" type="text" class="span2 form-control" id="datenew" size="10" readonly="" data-bind="value: Customer.DateOfBirth" /></span>
      </div>
  </div>

  </div>
</div>
    

    
 
   <div class="table-responsive">
  <table class="table table-hover table-striped">
      <tr class="warning">
        <td><strong>Sno</strong></td>
        <td><strong>Emp Code</strong></td>
        <td><strong>Name</strong></td>
        <td><strong>Designation</strong></td>
        <td><strong>Productname</strong></td> 
        <td><strong>Division</strong></td>        
        <td><strong>Description</strong></td>
        <td><strong>Target</strong></td>
      </tr>
      
      <?php 
	  			$rst = mysql_query("SELECT *, E.id as emid, DC.stateid AS estateid, DC.id as cityid FROM mrarea AS M INNER JOIN employeedata AS E ON M.empid=E.id INNER JOIN designation AS D ON D.id=E.designationid INNER JOIN dbcity AS DC ON M.cityid=DC.id WHERE E.active='Y' AND E.istarget='Y' AND M.active='Y' ORDER BY M.empid, M.cityid DESC",$con);
				$loopi=1;
				while($show = mysql_fetch_object($rst))
				
					{
						?>
      <tr>
        <td><?php echo($loopi);?>
          <input name="empid<?php echo($loopi);?>" type="hidden" id="empid<?php echo($loopi);?>" value="<?php echo($show->emid);?>" class="form-control"></td>

          <td><?php echo($show->empcode);?>
          </td>
          
        <td><?php echo($show->firstname);?>
          <input name="desigid<?php echo($loopi);?>" type="hidden" id="desigid<?php echo($loopi);?>" value="<?php echo($show->designationid);?>" class="form-control"></td>
        <td><?php echo($show->designation);?><br/>
        <input name="stateid<?php echo($loopi);?>" type="hidden" id="stateid<?php echo($loopi);?>" value="<?php echo($show->estateid);?>" class="form-control">        
          </td>
          
          <td>
          <Select name="product<?php echo($loopi);?>" id="product<?php echo($loopi);?>" class="form-control" onChange="setdescription(<?php echo($loopi);?>);">
          <Option value="0">All</Option>
        	<?php 
				$rst2 = mysql_query("SELECT * FROM productinfo WHERE active='Y'",$con);
				
				while($show2 = mysql_fetch_object($rst2))
				{?>
            	<Option value="<?php echo($show2->id);?>"><?php echo($show2->mediname);?></Option>
            <?php }?>
        </Select>
          </td>
        
        <td>
        <?php echo($show->city);?>
        <Select size="1" name="division<?php echo($loopi);?>" id="division<?php echo($loopi);?>" class="form-control hidden">
			<Option value="<?php echo($show->cityid);?>"><?php echo($show->city);?></Option>
        </Select>
        </td>
        
         <td>
         
         	<textarea style="font-size:9px; resize:none; width:206px;" name="description<?php echo($loopi);?>" class="form-control" id="description<?php echo($loopi);?>" placeholder="You can add product qty here for record"></textarea>
         </td>
         
        <td>
        <input onKeyPress="return isNumberKey(event)" name="target<?php echo($loopi);?>" type="text" id="target<?php echo($loopi);?>" placeholder="only numbers" class="form-control">
        <input checked type="checkbox" id="active<?php echo($loopi);?>" name="active<?php echo($loopi);?>" value="Y" aria-label=""> is active
        </td>
        
        
      </tr>
      
              <?php 
			  $loopi=$loopi+1;
			  }
			  $_SESSION['loopi']='';
			  $_SESSION['loopi']=$loopi-1;
			  ?>
              

       <tr>
       <td colspan="8">
       <div class="col-sm-12">
         <input type="submit" name="button" id="button" value="Submit Targets" class="btn-primary btn pull-right">
         </div>
		</td>
       </tr>       
    </table>
</div>

</form>
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
var d = new Date();
var currentMonth=('0'+(d.getMonth()+1)).slice(-2);
$('#datenew').val('01'+'-'+ currentMonth +'-'+d.getFullYear());
$('#datepicker').datepicker();
});//]]> 

function setdescription(param){
	//alert(param);
	$('#description'+param).val($('#description'+param).val()+$('#product'+param).find('option:selected').text() + "- \n");
	}
function isNumberKey(evt)
      {
		var charCode = (evt.which) ? evt.which : event.keyCode
          if (charCode != 47 && charCode > 31 
            && (charCode < 48 || charCode > 57))
             return false;
 
          return true;
}
</script>

</body>
</html>