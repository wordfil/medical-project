<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='MOTIVATIONAL';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>Motivational thoughts</h2>
 <?php
	 	$rst = mysql_query("Select * from motivationalthoughts",$con);
	 	$show = mysql_fetch_object($rst);
		$quote = $show->quote;
?>
<?php include('includes/admin-alerts.php');?>
   
 <form id="form1" name="form1" method="post" action="process/insertdb.php">
 <div class="form-group">
        <div class="row">
              <div class="col-sm-12">
                <label class="control-label">Motivational thoughts *</label>
                <textarea name="quote" class="form-control" id="quote"><?php echo($quote);?></textarea>
            </div>
            
        </div>
    </div>
    
 	
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="reset" class="form-control btn-info">Clear</button>              
        	</div>
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary">Submit</button>              
        	</div>            
	  </div>     
   </div>
 </form>  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#quote').val().length<=0){
		$('#quote').focus();
		$errmsg='Motivational thought can not be blank';
		}
	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});
</script>

</body>
</html>