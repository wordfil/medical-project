<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='EDITDBPERMISSION';
include_once('includes/adminarea.php');
include_once('includes/connection.php');
$getid=$_GET['pid'];
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
<Div id="loginform2">
      <h2>Edit Permissions</h2>
      
<?php include('includes/admin-alerts.php');?>
<?php $rst = mysql_query("SELECT * FROM permission_manager WHERE id=$getid",$con);
$show= mysql_fetch_object($rst)
?>  
  
<form id="form1" name="form1" method="post" action="process/permission-updated.php">

  <div class="form-group col-sm-4">
    <label class="control-label">Select employee *</label>
    <select name="empname" id="empname" class="form-control">
      <option value=0>Select employee</option>
        <?php $rst2 = mysql_query("SELECT *,emp.id as empid FROM employeedata as emp INNER JOIN designation ON emp.designationid=designation.id WHERE emp.active='Y' AND isadmin='N' order by emp.firstname",$con);
                  while($show2 = mysql_fetch_object($rst2))
                      {
              ?>
        <option <?php if(($show2->empid)==($show->empid)){ echo 'selected';}?> value="<?php echo($show2->empid); ?>"><?php echo($show2->firstname); ?> <?php echo($show2->lastname); ?> [<?php echo($show2->designation); ?>]</option>
        <?php }
        mysql_free_result($rst);?>
   	</select>
  </div>
   
  <Div class="row col-sm-12">  
  <div class="form-group col-sm-3">
  <label class="control-label">Upload Manager</label>
  <Select size="1" name="uploadmanager" id="uploadmanager" class="form-control">
	<Option <?php if(($show->UPLOADMGR)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->UPLOADMGR)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">State Master</label>
  <Select size="1" name="statemaster" id="statemaster" class="form-control">
	<Option <?php if(($show->DBSTATE)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->DBSTATE)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">City Master</label>
  <Select size="1" name="citymaster" id="citymaster" class="form-control">
	<Option <?php if(($show->DBCITY)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->DBCITY)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Add Designations/Roles</label>
  <Select size="1" name="rolemaster" id="rolemaster" class="form-control">
	<Option <?php if(($show->DBROLE)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->DBROLE)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">Employee Registration</label>
  <Select size="1" name="empregister" id="empregister" class="form-control">
	<Option <?php if(($show->DBEMP)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->DBEMP)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Assign Headquarters</label>
  <Select size="1" name="headquarter-master" id="headquarter-master" class="form-control">
	<Option <?php if(($show->MRAREA)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->MRAREA)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">View Employee Data</label>
  <Select size="1" name="view-employee" id="view-employee" class="form-control">
	<Option <?php if(($show->VIEWEMP)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->VIEWEMP)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Target Master</label>
  <Select size="1" name="target-master" id="target-master" class="form-control">
	<Option <?php if(($show->DBTARGET)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->DBTARGET)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div>
  
    <div class="form-group col-sm-3">
  <label class="control-label">View Target Master</label>
  <Select size="1" name="view-target-master" id="view-target-master" class="form-control">
	<Option <?php if(($show->DBVTARGET)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->DBVTARGET)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Performance Statistics</label>
  <Select size="1" name="performance-target-master" id="performance-target-master" class="form-control">
	<Option <?php if(($show->PMASTER)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->PMASTER)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">M R Reporting Form</label>
  <Select size="1" name="mr-reporting-url" id="mr-reporting-url" class="form-control">
	<Option <?php if(($show->MRFORM)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->MRFORM)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Recognition Achievers</label>
  <Select size="1" name="recognition-high-achievers" id="recognition-high-achievers" class="form-control">
	<Option <?php if(($show->DBTOPA)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->DBTOPA)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">View Achievers Data</label>
  <Select size="1" name="view-recognition-high-achievers" id="view-recognition-high-achievers" class="form-control">
	<Option <?php if(($show->VDBTOPA)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->VDBTOPA)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Success Quotes</label>
  <Select size="1" name="success-quotes" id="success-quotes" class="form-control">
	<Option <?php if(($show->SUCCESSQUOTE)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->SUCCESSQUOTE)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">Motivational thoughts</label>
  <Select size="1" name="motivational-thoughts" id="motivational-thoughts" class="form-control">
	<Option <?php if(($show->MOTIVATIONAL)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->MOTIVATIONAL)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Customer Testimonials</label>
  <Select size="1" name="customer-testimonials" id="customer-testimonials" class="form-control">
	<Option <?php if(($show->CT)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->CT)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
    <div class="form-group col-sm-3">
  <label class="control-label">Training Programs</label>
  <Select size="1" name="available-training-programs" id="available-training-programs" class="form-control">
	<Option <?php if(($show->ATP)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->ATP)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Available support systems</label>
  <Select size="1" name="available-support-systems" id="available-support-systems" class="form-control">
	<Option <?php if(($show->ASSYS)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->ASSYS)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div>  
  
    <div class="form-group col-sm-3">
  <label class="control-label">Incentive programs</label>
  <Select size="1" name="incentive-prog" id="incentive-prog" class="form-control">
	<Option <?php if(($show->INCPROG)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->INCPROG)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Product Updates</label>
  <Select size="1" name="product-updates" id="product-updates" class="form-control">
	<Option <?php if(($show->PRODUCTUP)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->PRODUCTUP)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
      <div class="form-group col-sm-3">
  <label class="control-label">Sales tips and advice</label>
  <Select size="1" name="sales-tips" id="sales-tips" class="form-control">
	<Option <?php if(($show->STA)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->STA)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Reinforcement of policies</label>
  <Select size="1" name="reinforcement-of-policies" id="reinforcement-of-policies" class="form-control">
	<Option <?php if(($show->ROP)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->ROP)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div>  
  
    <div class="form-group col-sm-3">
  <label class="control-label">Alert of Deadlines</label>
  <Select size="1" name="alertdead" id="alertdead" class="form-control">
	<Option <?php if(($show->AOD)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->AOD)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Faqs</label>
  <Select size="1" name="faqs" id="faqs" class="form-control">
	<Option <?php if(($show->FAQS)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->FAQS)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
  <div class="form-group col-sm-3">
  <label class="control-label">Footer</label>
  <Select size="1" name="footerlink" id="footerlink" class="form-control">
	<Option <?php if(($show->FOOTERLINKS)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->FOOTERLINKS)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div> 
  
 <div class="form-group col-sm-3">
  <label class="control-label">Inbox</label>
  <Select size="1" name="inbox" id="inbox" class="form-control">
	<Option <?php if(($show->INBOX)==1){ echo 'selected';}?> value="1" class="bg-success">Grant</Option>
    <Option <?php if(($show->INBOX)==0){ echo 'selected';}?> value="0" class="bg-warning">Revoke</Option>
  </Select>
  </div>
  
  
  </Div> 		
            
  <div class="form-group col-sm-4">
  <label class="control-label"></label>
  <button type="submit" class="form-control btn-success">Edit Permissions</button>              
  </div>            


 </form>
 
</Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#empname').val()<=0){
		$('#empname').focus();
		$errmsg='Please select employee name';
		}
	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
}); 
</script>

</body>
</html>