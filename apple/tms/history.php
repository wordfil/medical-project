<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='HISTORY';
if($_SESSION['userid']<>1){
	header("Location:index.php?err=You are not authorised to access this page.");	
	}
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>
<body>
<?php include_once("includes/header.php");?>
  <?php include_once("includes/toplinks.php");?>
  <Div id="midsection" class="clearall">
    <h2>Login History</h2>
    <?php
$rst = mysql_query("SELECT *, L.sysdate as datel, L.sysip AS ipl, L.id AS mid, L.systime AS timel FROM logindata AS L INNER JOIN employeedata AS E ON L.userid = E.id order by L.id DESC",$con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){?>
    <h3 class="btn btn-danger">All messages</h3>
    <div class="input-group"> <span class="input-group-addon">Filter</span>
      <input id="filter" type="text" class="form-control" placeholder="Type here...">
    </div>
    <div id="divTableDataHolder">
      <table class="table table-striped table-condensed searchable">
        <tr class="danger">
          <td><strong>User</strong></td>
          <td><strong>System Date</strong></td>
          <td><strong>System Time</strong></td>
          <td><strong>System IP</strong></td>
          <td><strong>Action</strong></td>
        </tr>
        <?php
    while($show = mysql_fetch_object($rst))
        {
        ?>
        <tr class="small">
          <td><?php echo($show->firstname.' '.$show->lastname);?></td>
          <td><?php echo($show->datel);?></td>
          <td><?php echo($show->timel);?></td>
          <td><?php echo($show->ipl);?></td>
          <td><a href="process/actions.php?mid=<?php echo($show->mid);?>">Delete</a></td>
        </tr>
        <?php } ?>
      </table>
    </div>
    <a href="process/actions.php?mid=<?php echo($show->mid);?>&action=all" class="btn btn-danger"><i class="fa fa-2x fa-trash"></i> Clear all history</a>
    <button id="myButtonControlID" class="btn btn-success pull-right"><i class="fa fa-2x fa-file-excel-o"></i> Export to Excel</button>
    <?php } ?>
  </Div>
  <?php include_once("includes/footerlinks.php");?>
  <?php include_once("includes/footer.php");?>
<script type='text/javascript'>//<![CDATA[   

(function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
 
 $("[id$=myButtonControlID]").click(function(e) {
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=divTableDataHolder]').html()));
    e.preventDefault();
});
</script>
</body>
</html>