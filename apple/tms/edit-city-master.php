<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='DBCITY';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>City Master - Edit</h2>
   <?php
	 	$id=$_GET['id'];
		$rst = mysql_query("Select * from dbcity WHERE id=$id",$con);
	 	$show = mysql_fetch_object($rst);
		$city=$show->city;
		$stateid=$show->stateid;
		$active=$show->active;
?>     
 <?php include('includes/admin-alerts.php');?>
     
 <form id="form1" name="form1" method="post"action="process/updatedb.php?id=<?php echo $id;?>">
 
   <div class="form-group">
        <div class="row">
            <div class="col-sm-12">
                <label class="control-label">State Group</label>
                <select class="form-control" name="state" id="state">
                  <?php $rst = mysql_query("SELECT * FROM dbstate order by statename",$con);
                            while($show = mysql_fetch_object($rst))
                                {
                        ?>
                  <option value="<?php echo($show->id); ?>" <?php if($stateid==($show->id)){ echo('selected'); } ?>><?php echo($show->statename); ?></option>
                  <?php }
				  mysql_free_result($rst);?>
                </select>
            </div>
        </div>
    </div>
    
 <div class="form-group">
        <div class="row">            
              <div class="col-sm-12">
                <label class="control-label">City Name</label>
                <input name="cityname" type="text" class="form-control" id="cityname" value="<?php echo($city); ?>" />
                <label class="btn btn-primary warning"><input name="isactive" type="checkbox" id="isactive" <?php if($active=='Y'){echo('checked');}?>  autocomplete="off">&nbsp; Active &nbsp;</label>
            </div>
            
        </div>
    </div>
    
 	
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-info">Clear</button>              
        	</div>
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary">Submit</button>              
        	</div>            
	  </div>
      
   </div>
 </form>
 
 
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 
		
	if($('#cityname').val().length<=0){
		$('#cityname').focus();
		$errmsg='City can not be blank';
		}
	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});
</script>


</body>
</html>