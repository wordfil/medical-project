<?php
ob_start();
session_start();
include_once('includes/connection.php');
$_SESSION['FORMPOS']='DBTOPA';
include_once('includes/admin-permissions.php');

$getdate=$_SESSION['getdate'];

$getcityid=$_SESSION['cityid'];
$getcity=$_SESSION['city'];

?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
	<script language="javascript">
        function addtargetrows(param){
            window.location.href = "target-master.php?mgrid="+param;
        }
    </script>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">

<Div id="loginform">
      <h3>Recognition of High Achievers</h3>
<?php include('includes/admin-alerts.php');?>
     
<form action="process/get-achievers-sessions.php" form name="datesubmit" id="datesubmit" method="POST">
<div class="form-group">
 
  <div class="row">
    <div class="col-sm-3">
        <label class="control-label">State *</label>
          <select name="state" id="state" class="form-control">
          <option value=0>Select state</option>
            <?php $rst = mysql_query("SELECT * FROM dbstate WHERE active='Y' order by statename",$con);
                      while($show = mysql_fetch_object($rst))
                          {
                  ?>
            <option value="<?php echo($show->id);?>" <?php if(($show->id)==$_SESSION['stateid']){echo('selected');}?>><?php echo($show->statename); ?></option>
            <?php }
            mysql_free_result($rst);?>
          </select>
  			
      </div>
            
      <div class="col-sm-3">
        <label class="control-label">City *</label>
        <select name="city" id="city" class="form-control">
          <option value="0|City not entered by admin">none</option>
          
          <option selected value="<?php echo($getcityid);?>|<?php echo($getcity);?>"><?php echo($getcity);?></option>
        </select>
      </div>          
  </div>

  <label class="control-label">Show high achievers for *</label>  
  <div class="row">
  <div class="col-sm-3">
          
    <div class="input-append date" id="datepicker" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy" data-date-minviewmode="months" data-date-viewmode="months">
      <span class="add-on">
      <input name="datenew" type="text" class="span2 form-control" id="datenew" size="10" readonly="" data-bind="value: Customer.DateOfBirth" value="<?php echo($getdate);?>"/>
      </span>      
    </div>
      
  </div>            
  </div>

  <label class="control-label"></label>         
  <div class="row">
  <div class="col-sm-3">        
      <input type="submit" value="Show Targets" class="btn-primary btn btn-success"> 	
  </div>            
  </div>
  
         
</div> 
</form>
   


<form action="process/update-achievers.php" form name="hadata" id="hadata" method="POST">
<table class="table table-hover table-striped">
      <tr class="success">
        <td><strong>Sno</strong></td>
        <td><strong>Emp Code</strong></td>
        <td><strong>Name/Designation</strong></td>
        <td><strong>Month / Year</strong></td>
        <td><strong>Zone</strong></td>
        <td><strong>Division</strong></td>        
        <td><strong>Target Description</strong></td>
        <td><strong>Target/Target Achieved</strong></td>
        <td><strong>Overall/ In nos</strong></td>
        <td><i class="fa fa-2x fa-trophy"></i><strong>Rank/Selection</strong></td>
        <td><strong>Note</strong></td>
      </tr>
      
<?php
if($_SESSION['stateid']==0){
	$sqlquery="SELECT *, DC.id as cityid, ET.id as dbid, ET.active as etactive, ET.empid AS empid, ET.stateid AS etstateid, DS.statename AS sname FROM emptarget AS ET INNER JOIN employeedata AS E ON E.id=ET.empid INNER JOIN designation AS D ON D.id=ET.designationid INNER JOIN dbcity AS DC ON ET.cityid=DC.id INNER JOIN dbstate AS DS ON ET.stateid=DS.id WHERE ET.targetdate='$getdate' AND ET.targetdone>0 ORDER BY ET.empid, ET.cityid DESC";		
}else{
	$stateid=$_SESSION['stateid'];
	
	if($_SESSION['stateid']<>0 && $getcityid==0){	
	$sqlquery="SELECT *, DC.id as cityid, ET.id as dbid, ET.active as etactive, ET.empid AS empid, ET.stateid AS etstateid, DS.statename AS sname FROM emptarget AS ET INNER JOIN employeedata AS E ON E.id=ET.empid INNER JOIN designation AS D ON D.id=ET.designationid INNER JOIN dbcity AS DC ON ET.cityid=DC.id INNER JOIN dbstate AS DS ON ET.stateid=DS.id WHERE ET.targetdate='$getdate' AND ET.stateid=$stateid AND ET.targetdone>0 ORDER BY ET.empid, ET.cityid DESC";
	}
	
	if($_SESSION['stateid']<>0 && $getcityid<>0){
	$sqlquery="SELECT *, DC.id as cityid, ET.id as dbid, ET.active as etactive, ET.empid AS empid, ET.stateid AS etstateid, DS.statename AS sname FROM emptarget AS ET INNER JOIN employeedata AS E ON E.id=ET.empid INNER JOIN designation AS D ON D.id=ET.designationid INNER JOIN dbcity AS DC ON ET.cityid=DC.id INNER JOIN dbstate AS DS ON ET.stateid=DS.id WHERE ET.targetdate='$getdate' AND ET.stateid=$stateid AND ET.cityid=$getcityid AND ET.targetdone>0 ORDER BY ET.empid, ET.cityid DESC";
		}
}

		$rst = mysql_query($sqlquery,$con);
		$loopi=1;
		$targettotal=0;
		$targettotaldone=0;
				while($show = mysql_fetch_object($rst))
				
					{
						?>
      <tr class="small">
        <td><?php echo($loopi);?>
          <input name="empid<?php echo($loopi);?>" type="hidden" id="empid<?php echo($loopi);?>" value="<?php echo($show->empid);?>" class="form-control">
          <input name="dbid<?php echo($loopi);?>" type="hidden" id="dbid<?php echo($loopi);?>" value="<?php echo($show->dbid);?>" class="form-control">
          
          </td>

          <td><?php echo($show->empcode);?>
          </td>
          
        <td>
		<?php echo($show->firstname .' '.$show->lastname);?><br>
        <?php echo($show->designation);?></td>
        <td>
		<?php 
		$mydate =$show->targetdate;
		$month = date("M",strtotime($mydate));
		$year = date("Y",strtotime($mydate));
		?>
        <?php echo($month.' '.$year);?>
          </td>
          
        <td>
        <?php echo($show->sname);?>
        </td>
                         
        <td>
        <?php echo($show->city);?>
        </td>
        
         <td style="font-size:10px">
         	<?php echo(nl2br($show->description));?>
         </td>
         
        <td>
        <?php echo($show->target);?>/<?php echo($show->targetdone);?></td>
        <td>
        <?php 
		$ta=$show->target;
		$td=$show->targetdone;
		if($ta>0){
			$targettotal=$targettotal+$ta;
			$targettotaldone=$targettotaldone+$td;
			
			$ts=$td-$ta;
			$tspercent=($td/$ta)*100;
			if($tspercent<40){
			echo '<font color=red>'.number_format((float)$tspercent, 2, '.', '').'%<br>'.$ts.'</font>';
			}else{
				echo '<font color=green>'.number_format((float)$tspercent, 2, '.', '').'%<br>'.$ts.'</font>';
				}
		}
		?>
        </td>
        <td>
<div class="form-group">
         <label for="rank<?php echo($loopi);?>" class="col-sm-1" ></label>
         
         <div class="col-sm-9">
       	<Select size="1" name="rank<?php echo($loopi);?>" id="rank<?php echo($loopi);?>" class="form-control" style="width:60px;">
        <Option value="0|0|<?php echo($getdate);?>|0|0" selected>-</Option>
        <?php for($oploop=1; $oploop<=5; $oploop++){?>
			<Option value="<?php echo($oploop);?>|<?php echo($show->empid);?>|<?php echo($getdate);?>|<?php echo($show->dbid);?>|<?php echo($show->cityid);?>|<?php echo($show->etstateid);?>"><?php echo($oploop);?></Option>
            <?php }?>
            </Select>
            
            <input type="checkbox" id="active<?php echo($loopi);?>" name="active<?php echo($loopi);?>" value="Y" aria-label="" onClick="showcombo(<?php echo($loopi);?>);"> Select           
         </div>
</div>
        </td>
        
        <td>
           <Select size="1" name="widetype<?php echo($loopi);?>" id="widetype<?php echo($loopi);?>" class="form-control" style="display:none">
            <option value="2">National</option>
            <option value="3" selected>Zonal</option>
            <option value="1">Both</option>
            </Select>
        <textarea style="font-size:9px; resize:none; width:180px;" name="description<?php echo($loopi);?>" class="form-control" id="description<?php echo($loopi);?>" placeholder="You can add text/appreciation"></textarea>

        </td>
        
      </tr>
      
              <?php 
			  $loopi=$loopi+1;
			  $getdate= $show->targetdate;
			  }
			  $_SESSION['loopi']='';
			  $_SESSION['loopi']=$loopi-1;
			  ?>
              

       <tr>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td>
       <td></td> 
       <td><strong><?php echo $targettotal;?></strong></td>
       <td><strong><?php echo $targettotaldone;?></strong></td>
       
       <td>
       <strong>
       <?php 
	   if($targettotal>0){
		   $tspercent=($targettotaldone/$targettotal)*100;
		   if($tspercent<60){
			echo '<font color=red>'.number_format((float)$tspercent, 2, '.', '').'%</font>';
			}else{
				echo '<font color=green>'.number_format((float)$tspercent, 2, '.', '').'%</font>';
				}
	   }
	   ?>
       </strong>
       </td>
       <td></td>
<td>
 <input type="submit" value="Set Top Achiever" class="btn-primary btn btn-primary"> 	
</td>
       </tr>       
    </table>
</form>


 <button id="myButtonControlID" class="btn btn-success pull-right"><i class="fa fa-2x fa-file-excel-o"></i> Export to Excel </button>

</div>
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
var d = new Date();
//$('#datenew').val('01'+'-'+(d.getMonth()+1+'-'+d.getFullYear()));
$('#datepicker').datepicker('hide');
});//]]> 

$(document).ready(function() {
        $('#state').change(function(){
            $.ajax({
                type: "GET",
                url: "process/getcityadvance.php",
                data: 'stateid=' + $('#state').val(),
                success: function(msg){
                    $('#city').html(msg);
                }
            }); // Ajax Call
        }); //event handler
    }); //document.ready


$("[id$=myButtonControlID]").click(function(e) {
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=divTableDataHolder]').html()));
    e.preventDefault();
});

function showcombo(e){
	if(document.getElementById('widetype'+e).style.display=='none')
	{
		document.getElementById('widetype'+e).style.display='inline';
	}else{
		document.getElementById('widetype'+e).style.display='none';
		}
	}
</script>

</body>
</html>