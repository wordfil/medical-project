<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='CPIC';
include_once('../includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("../includes/meta.php");?>
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link href="../css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../css/bootstrap.css">
<link rel="stylesheet" href="../css/datepicker.css">
<?php include_once("../includes/title.php");?>
</head>

<body>
<?php include_once("../includes/header.php");?>
<?php include_once("../includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>Change Employee photo - Edit</h2>
  <?php
	 	$id=$_GET['id'];
		$rst = mysql_query("Select * from employeedata WHERE id=$id",$con);
	 	$show = mysql_fetch_object($rst);
		$empphoto=$show->empphoto;
?>
<?php include('../includes/admin-alerts.php');?>
   
  <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
          <label class="control-label">Your Current photo</label>
            <img src=<?php if(($show->empphoto)!='NA'){?>"../emppics/<?php echo($show->empphoto); ?>"<?php }else{?>"../emppics/nopic_2014.png"<?php }?> alt="Your profile picture" name="blahold" width="100" height="100" class="img-thumbnail" id="blahold" onload="if (this.width > 100) this.width = 150;">                           
            </div>    
			</div> 
    </div>
    <hr>

<form name="empform" method="post" action="../process/update-pic.php?id=<?php echo $id;?>" enctype="multipart/form-data" id="empform">
  <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <img src="emppics/nopic_2014.png" alt="Your profile picture" name="blah" width="100" height="100" class="img-thumbnail" id="blah" onload="if (this.width > 100) this.width = 150;">               
            </div>
        </div>
    </div>
    
    
    <div class="form-group">
        <div class="row">
            <div class="col-sm-12">
            <label class="control-label">Upload new picture</label>
                  <div style="position:relative;">
		<a class='btn btn-primary' href='javascript:;'>
			Browse picture...
			<input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="fileToUpload" id="fileToUpload" size="40"  onchange="readURL(this);">
		</a>
        <br/>
		<small>Picture must be in jpg or jpeg format and less than 1MB</small>
		<span class='label label-info' id="upload-file-info"></span>
	</div>
      </div>              
	  </div>
      </div>   
 	
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-3">
            <label class="control-label"></label>
           <button type="reset" class="form-control btn-info">Clear</button>              
        	</div>
            <div class="col-sm-3">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary">Submit</button>              
        	</div>            
	  </div>     
   </div>
 </form>

    </Div>
</Div>
 
<?php include_once("../includes/footerlinks.php");?>
<?php include_once("../includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
										  
function readURL(input) {
			
			$("#upload-file-info").html($(input).val());
			
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }										  
										  
$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#state').val().length<=0){
		$('#state').focus();
		$errmsg='State can not be blank';
		}
		
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});
</script>

</body>
</html>