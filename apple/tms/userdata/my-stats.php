<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='MYSTAT';
include_once('../includes/connection.php');
if($_SESSION['ISLOGIN']<>'TRUE'){
	header("Location: ../index.php?info=Please login first");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("../includes/meta.php");?>
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link href="../css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../css/bootstrap.css">
<link rel="stylesheet" href="../css/datepicker.css">
<?php include_once("../includes/title.php");?>

</head>

<body>
<?php include_once("../includes/header.php");?>
<?php include_once("../includes/toplinks.php");?> 
<?php include('../includes/admin-alerts.php');?>

<Div id="midsection" class="clearall">
<?php if($_SESSION['istarget']=='Y'){?>
<h2><span class="label label-primary">My Performance Statistics</span></h2>   
<?php
$uid=$_SESSION['userid'];
$rst = mysql_query("SELECT * FROM emptarget AS E INNER JOIN dbcity as DC ON E.cityid=DC.id INNER JOIN designation AS D ON E.designationid=D.ID WHERE E.empid=$uid AND E.active='Y' order by E.id DESC",$con);
$num_rows = mysql_num_rows($rst);
	$modelid=1;
	if($num_rows>0){
		
		$totaltarget=0;
		$totaltargetdone=0;
		$totaltargetinpercent=0;
	
		?>
<div class="table-responsive">   
    <div class="input-group"> <span class="input-group-addon">Filter</span>
    <input id="filter" type="text" class="form-control col-sm-3" placeholder="Type here...">   
	</div>
<table class="table table-striped searchable">
<thead class="alert-info">
    <th>Month</th>
    <th>Work as</th>
    <th>Description</th>
    <th>Target Assign</th>
    <th>Target Achieved</th>
    <th>Division</th>
    <th>Performance</th>
</thead>
    <?php while($show = mysql_fetch_object($rst))
        {
			$targetmonth=$show->target;
			$targetdone=$show->targetdone;
			
			$totaltarget=$totaltarget+$targetmonth;
			$totaltargetdone=$totaltargetdone+$targetdone;
			
			$dbdate=$show->targetdate;				
			$sysmonth= substr($dbdate, 3, 2);	
			$sysmonth= date('F', mktime(0, 0, 0, $sysmonth, 10));
			$sysyear= date('Y',strtotime($dbdate));

        ?>
<tr>
    <td scope="row"><span  data-toggle="tooltip" data-placement="top" title="<?php echo($dbdate);?>"><?php echo($sysmonth.', '.$sysyear);?></span></td>
    <td scope="row"><small><?php echo($show->designation);?></small></td>
    <td scope="row"><small><?php echo(nl2br($show->description));?></small></td>
     <td scope="row"><?php echo($targetmonth);?></td>
    <td scope="row"><?php echo($targetdone);?></td>
     <td scope="row"><?php echo($show->city);?></td>
     
    <td scope="row">
    <?php
	if($targetmonth>0){
		$tarinpercent=($targetdone/$targetmonth)*100;
		
		if($targetmonth==$targetdone){	
			echo '<font color=blue> +'.number_format((float)$tarinpercent, 2, '.', '').'%'.$ts.' <span class="glyphicon glyphicon-star"></span></font>';
			}
		if($targetmonth>$targetdone){
			echo '<font color=red>- '.number_format((float)$tarinpercent, 2, '.', '').'%'.$ts.' <span class="glyphicon glyphicon-hand-down"></span></font>';
			}
		if($targetmonth<$targetdone){	
			echo '<font color=green> +'.number_format((float)$tarinpercent, 2, '.', '').'%'.$ts.' <span class="glyphicon glyphicon-hand-up"></span> <span class="glyphicon glyphicon-star"></span></font>';
			}
	}else{
		echo('-');
		}
	?>
    </td>
</tr>
    
<?php }
	$totaltargetinpercent=($totaltargetdone/$totaltarget)*100;
	$inpercent=number_format((float)$totaltargetinpercent, 2, '.', '');

?>

<tr class="alert-info">
    <td></td>
    <td></td>
    <td></td>
    <td><?php echo $totaltarget;?></td>
    <td><?php echo $totaltargetdone;?></td>
    <td></td>
    <td><strong><?php echo $inpercent.'%';?></strong></td>
</tr>

    </table> 

<strong>Overall performance <?php echo $inpercent.' %';?></strong>
<div class="progress">
  
<?php
if($totaltargetinpercent<40){$bartype='danger';}
if($totaltargetinpercent>40 && $totaltargetinpercent<70){$bartype='warning';}
if($totaltargetinpercent>70){$bartype='success';}
?>  
  <div class="progress-bar progress-bar-<?php echo($bartype);?> progress-bar-striped active" style="width: <?php if($inpercent>100){$inpercent=100;} echo($inpercent);?>%">
    <span class="sr-only"><?php echo($inpercent.' %');?> Complete (warning)</span>
  </div>
</div>


</div> 


<?php }?>


<?php }else{
if($_GET['datenew']==''){
	$getdate = '01-'.$sysmonth.'-'.$sysyear;
}else{
	$getdate =$_GET['datenew'];
	}

$mysqlquery="Select * FROM employeedata WHERE immidiateid=$userid";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}
	mysql_free_result($rst);	


$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);


$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);
	
$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);

$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);

$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);

$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);

$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);
	
$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);

$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);

$mysqlquery="Select * FROM employeedata WHERE immidiateid IN('".implode("', '", $newid)."')";
$rst = mysql_query($mysqlquery, $con);
$num_rows = mysql_num_rows($rst);
if($num_rows>0){
	$arayloop=0;
		while($show = mysql_fetch_object($rst))
			{
			  $newid[$arayloop]= $show->id;
			  $allids[]= $show->id;
			  $arayloop=$arayloop+1;
			 	//echo $show->firstname.'<br>';
			}
	}else{ $newid[0]='-1';}
	mysql_free_result($rst);

$cdate='01-'.$sysmonth.'-'.$sysyear;

	$arrlength = count($allids);
	for($x = 0; $x < $arrlength; $x++) {
		//echo $allids[$x];
		//echo "<br/>";
		
	}?>
  <div class="form-group">
 <form action="my-stats.php" form name="datesubmit" id="datesubmit" method="GET">
    <label class="control-label">Show targets statistics for *</label>  
        <div class="row">
        <div class="col-sm-3">
        
            <div class="input-append date" id="datepicker" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy" data-date-minviewmode="months" data-date-viewmode="months">
            <span class="add-on">
            <input name="datenew" type="text" class="span2 form-control" id="datenew" size="10" readonly="" data-bind="value: Customer.DateOfBirth" />
            </span>  
            </div>        
          </div>
          <input type="submit" value="Get Targets" class="btn-primary btn btn-warning">  
        
        </div>        
</form>
</div> 

 
<h2>Month selected - <?php echo $getdate;?></h2> 
<div class="input-group">
<span class="input-group-addon">Filter</span>
<input id="filter" type="text" class="form-control col-sm-3" placeholder="Type here...">   
</div>

<div id="divTableDataHolder">
<div class="table-responsive">
  <table class="table table-hover table-striped searchable">
      <tr class="success">
        <td><strong>Sno</strong></td>
        <td><strong>Emp Code</strong></td>
        <td><strong>Name</strong></td>

        <td><strong>Division</strong></td>        
        <td><strong>Description</strong></td>
        <td><strong>Target</strong></td>
        <td><strong>Target Achieved</strong></td>
        <td><strong>Statistics</strong></td>
      </tr>
      
      <?php 
	  //$getdate='01-01-2015';
	  $targetdoneall=0;
	  $targetall=0;
	 $rst = mysql_query("SELECT *, DC.id as cityid, ET.id as dbid, ET.active as etactive FROM emptarget AS ET INNER JOIN employeedata AS E ON E.id=ET.empid INNER JOIN designation AS D ON D.id=ET.designationid INNER JOIN dbcity AS DC ON ET.cityid=DC.id WHERE ET.targetdate='$getdate' AND ET.empid IN('".implode("', '", $allids)."') ORDER BY ET.empid, ET.cityid DESC",$con);
				$loopi=1;
				while($show = mysql_fetch_object($rst))
				
					{
						?>
      <tr class="small">
        <td><?php echo($loopi);?>        
          </td>

          <td><?php echo($show->empcode);?>
          </td>
          
        <td><?php echo($show->firstname);?></td>


        
        <td>
        <?php echo($show->city);?>
        </td>
        
         <td>
         <?php echo($show->description);?>
         </td>
         
        <td>
		<?php 
		$targetall=$targetall+($show->target);
		echo($show->target);
		
		?><br/>
        </td>
        
        <td><?php 
		$targetdoneall=$targetdoneall+($show->targetdone);
		echo($show->targetdone);
		?></td>
        
        <td>
        <?php 
		$ta=$show->target;
		$td=$show->targetdone;
		if($ta>0){
			$targettotal=$targettotal+$ta;
			$targettotaldone=$targettotaldone+$td;
			
			$ts=$ta-$td;
			$tspercent=($td/$ta)*100;
			if($tspercent<40){
			echo '<font color=red>'.number_format((float)$tspercent, 2, '.', '').'%<br>'.$ts.'</font>';
			}else{
				echo '<font color=green>'.number_format((float)$tspercent, 2, '.', '').'%<br>'.$ts.'</font>';
				}
		}
		?>
        </td>
        
      </tr>      
       <?php $loopi++;  } ?>          

       <tr>
      <tr class="warning">
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>        
        <td></td>
        <td><strong><?php echo($targetall);?></strong></td>
        <td><strong><?php echo($targetdoneall);?></strong></td>
        <td></td>
      </tr>
       </tr>       
    </table>
</div>
</div>


<?php
 if($targetall>0){
	$tspercent=($targetdoneall/$targetall)*100;
 }
 
if($targetdoneall>0){
$totaltargetinpercent=($targetdoneall/$targetall)*100;
}

$inpercent=number_format((float)$totaltargetinpercent, 2, '.', '');
	
if($totaltargetinpercent<40){$bartype='danger';}
if($totaltargetinpercent>40 && $totaltargetinpercent<70){$bartype='warning';}
if($totaltargetinpercent>70){$bartype='success';}
?> 
 
<strong>Overall performance <?php echo $inpercent.' %';?></strong>
<div class="progress">
  
  <div class="progress-bar progress-bar-<?php echo($bartype);?> progress-bar-striped active" style="width: <?php if($inpercent>100){$inpercent=100;} echo($inpercent);?>%">
    <span class="sr-only"><?php echo($inpercent.' %');?> Complete (warning)</span>
  </div>
</div>
<button id="myButtonControlID" class="btn btn-success pull-right"><i class="fa fa-2x fa-file-excel-o"></i> Export to Excel</button>

<?php }?>

</Div>
<?php include_once('essential.php');?>  
<?php include_once("../includes/footerlinks.php");?>
<?php include_once("../includes/footer.php");?>
<script language="javascript">
$(window).load(function(){
var d = new Date();
var currentMonth=('0'+(d.getMonth()+1)).slice(-2);
//$('#datenew').val('01'+'-'+ currentMonth +'-'+d.getFullYear());
$('#datepicker').datepicker('show');
});//]]>

(function ($) {
        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })
	$('[data-toggle="tooltip"]').tooltip();
    }(jQuery));

$('#datesubmit').submit(function() {
	
	$errmsg='';	
	
	if($('#datenew').val().length<=0){
		$('#datenew').focus();
		$errmsg='Please select month';
		}
		
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{
	
	var selObj = document.getElementById('attachedfiles');
	  for (var i=0; i<selObj.options.length; i++) {
		selObj.options[i].selected = true;
	  }
		return true;	
		}
		
});

(function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));

$("[id$=myButtonControlID]").click(function(e) {
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=divTableDataHolder]').html()));
    e.preventDefault();
});
</script>
</body>
</html>