<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='USERDASHHOME';
include_once('../includes/connection.php');
if($_SESSION['ISLOGIN']<>'TRUE'){
	header("Location: ../index.php?info=Please login first");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("../includes/meta.php");?>
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link href="../css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../css/bootstrap.css">
<link rel="stylesheet" href="../css/datepicker.css">
<?php include_once("../includes/title.php");?>

</head>

<body>
<?php include_once("../includes/header.php");?>
<?php include_once("../includes/toplinks.php");?> 
<?php include('../includes/admin-alerts.php');?>

<Div id="midsection" class="clearall">
<h2><span class="label label-primary">Customer testimonials</span></h2>   
<?php
$rst = mysql_query("SELECT * FROM customertestimonials WHERE active='Y' order by id DESC",$con);
$num_rows = mysql_num_rows($rst);
	$modelid=1;
	if($num_rows>0){
		?>
<table class="table table-striped">
<?php while($show = mysql_fetch_object($rst))
	{
	?>
<tr>
<td scope="row"><strong><?php echo($show->title);?></strong></td>
<td scope="row"><?php echo($show->testimonial);?></td>
<td scope="row" class="small"><?php echo($show->sysdate);?></td>
</tr>


 <?php }?>
</table>  
<?php }?>
</Div>
 
<?php include_once("../includes/footerlinks.php");?>
<?php include_once("../includes/footer.php");?>

</body>
</html>