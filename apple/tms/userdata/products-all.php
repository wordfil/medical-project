<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='USERDASHHOME';
include_once('../includes/connection.php');
if($_SESSION['ISLOGIN']<>'TRUE'){
	header("Location: ../index.php?info=Please login first");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("../includes/meta.php");?>
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link href="../css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../css/bootstrap.css">
<link rel="stylesheet" href="../css/datepicker.css">
<?php include_once("../includes/title.php");?>
<style type='text/css'>

.flip {
  -webkit-perspective: 800;
  -ms-perspective: 800;
  -moz-perspective: 800;
  -o-perspective: 800;
   width: 360px;
   height: 250px;
   position: relative;
   margin: 50px auto;

}
.flip .card.flipped {
  transform:rotatey(-180deg);
  -ms-transform:rotatey(-180deg); /* IE 9 */
  -moz-transform:rotatey(-180deg); /* Firefox */
  -webkit-transform:rotatey(-180deg); /* Safari and Chrome */
  -o-transform:rotatey(-180deg); /* Opera */

}
.flip .card {
  width: 360px;
  height: 250px;

  -webkit-transform-style: preserve-3d;
  -webkit-transition: 0.5s;
  -moz-transform-style: preserve-3d;
  -moz-transition: 0.5s;
  -ms-transform-style: preserve-3d;
  -ms-transition: 0.5s;
  -o-transform-style: preserve-3d;
  -o-transition: 0.5s;
  transform-style: preserve-3d;
  transition: 0.5s;
}
.flip .card .face {
  width: 360px;
  height: 252px;
  position: absolute;
  z-index: 2;
  font-family: Georgia;
  font-size: 1em;
  text-align: left;


  backface-visibility: hidden;  /* W3C */
  -webkit-backface-visibility: hidden; /* Safari & Chrome */
  -moz-backface-visibility: hidden; /* Firefox */
  -ms-backface-visibility: hidden; /* Internet Explorer */
  -o-backface-visibility: hidden; /* Opera */

}
.flip .card .front {
	font-family:"Lato",Arial,sans-serif !important;
	font-size:32px;
  position: absolute;
  z-index: 1;
  color:#069;
  width:360px;
  height:249px;
  cursor: pointer;
}
.flip .card .back {
    color: black;
    cursor: pointer;
	font-family:"Lato",Arial,sans-serif !important;
	padding-left:10px;
	padding-right:10px;
	background-color:#F7F7F7;
	border:1px solid #CCC;
  transform:rotatey(-180deg);
  -ms-transform:rotatey(-180deg); /* IE 9 */
  -moz-transform:rotatey(-180deg); /* Firefox */
  -webkit-transform:rotatey(-180deg); /* Safari and Chrome */
  -o-transform:rotatey(-180deg); /* Opera */ 

}
.nopictext{
	z-index:100;
    position:relative;
    font-size:32px;
    left:20px;
    top:80px;
	}
	
</style>
</head>

<body>
<?php include_once("../includes/header.php");?>
<?php include_once("../includes/toplinks.php");?> 
<?php include('../includes/admin-alerts.php');?>

<Div id="midsection" class="clearall">
<h2><span class="label label-primary">Products</span></h2>   
<?php
$rst = mysql_query("SELECT * FROM productinfo order by id DESC",$con);
$num_rows = mysql_num_rows($rst);
	$modelid=1;
	if($num_rows>0){
		while($show = mysql_fetch_object($rst))
			{
			?> 
<div class="col-sm-4">            
    <div class="flip"> 
      <div class="card"> 
        <div class="face front"><?php if(($show->picname)=='NA'){?><img style="position:absolute; left:0; top:0;"  src="../productpics/blankprotemp.jpg" ><p class="nopictext"><?php echo($show->mediname);?></p><?php }else{?><img src="../productpics/<?php echo($show->picname);?>" width="360" height="250"><?php }?></div> 
        <div class="face back"><h2><?php echo($show->mediname);?></h2><br><?php echo($show->backinfo);?><br><a role="button" class="btn btn-info btn-small pull-right" data-toggle="modal" data-target="#myModal<?php echo($modelid);?>">more</a></div> 
      </div> 
    </div> 
</div> 



<!-- Modal -->
<div class="modal fade" id="myModal<?php echo($modelid);?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel<?php echo($modelid);?>" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel<?php echo($modelid);?>"><?php if(($show->mediname)!='NA') echo($show->mediname);?></h4>
      </div>
      <div class="modal-body">
      <?php if(($show->picname)!='NA'){?><img src="../productpics/<?php echo($show->picname);?>" alt="Your product picture" name="blah" width="360" height="250" class="img-thumbnail"><?php }?><br>
       <?php if(($show->description)!='NA') echo($show->description);?>
      </div>
    </div>
  </div>
</div>
 <?php $modelid=$modelid+1; }}?>
 
</Div>
 
<?php include_once("../includes/footerlinks.php");?>
<?php include_once("../includes/footer.php");?>
<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
	/* card flip */
	$(".flip").hover(function(){
	  $(this).find(".card").toggleClass("flipped");
	  return false;
	});
});//]]>  

</script>
</body>
</html>