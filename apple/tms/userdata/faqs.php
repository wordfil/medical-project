<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='UFAQS';
include_once('../includes/connection.php');
if($_SESSION['ISLOGIN']<>'TRUE'){
	header("Location: ../index.php?info=Please login first");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("../includes/meta.php");?>
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link href="../css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../css/bootstrap.css">
<link rel="stylesheet" href="../css/datepicker.css">
<?php include_once("../includes/title.php");?>

</head>

<body>
<?php include_once("../includes/header.php");?>
<?php include_once("../includes/toplinks.php");?> 
<?php include('../includes/admin-alerts.php');?>

<Div id="midsection" class="clearall">
<h2><span class="label label-primary">FAQs</span></h2>   
<?php
$rst = mysql_query("SELECT * FROM faqs WHERE active='Y' order by id DESC",$con);
$num_rows = mysql_num_rows($rst);
	$modelid=1;
	if($num_rows>0){
		?>
<div class="table-responsive">   
    <div class="input-group"> <span class="input-group-addon">Filter</span>
    <input id="filter" type="text" class="form-control col-sm-3" placeholder="Type here...">   
	</div>
    <table class="table table-striped searchable">
    <?php while($show = mysql_fetch_object($rst))
        {
			$isanyattachment=$show->isanyattachment;
			$attachmentids=$show->attachmentids;
        ?>
    <tr>
    <td scope="row"><strong><?php echo($show->policytitle);?></strong> <span class="pull-right"><small><?php echo($show->sysdate);?></small></span></td>
    </tr>
    
<tr>
    <td scope="row">
		<?php echo($show->policies);?>
        
		<?php    
   		if(strlen($attachmentids)>0){   		
			$array =explode(',',$attachmentids);			
			$rstA = mysql_query("SELECT * FROM uploaddetails WHERE id IN('".implode("','",$array)."')",$con);
			$num_rowsA = mysql_num_rows($rstA);
			

	if($num_rowsA>0){
		while($showA = mysql_fetch_object($rstA))
			{
				include('file-preview-code.php');           
			}
		}
	}
	 ?>
    </td>
 </tr>
<?php }?>
    </table> 

<form id="form1" name="form1" method="post" action="../process/insertdb.php">   
<Div id="loginform1" class="col-sm-6">
<div class="panel panel-default">
    <div class="panel-heading"><h3>Contact Admin</h3></div>       
	<div class="panel-body">
      
        <div class="form-group">
          <label for="msgsubject">Subject:</label>
          <input type="msgsubject" class="form-control" id="msgsubject" placeholder="Subject" name="msgsubject">
        </div>
        <div class="form-group">
          <label for="msgbody">Password:</label>
          <textarea name="msgbody" class="form-control" id="msgbody" maxlength="499" placeholder="Type your query/suggestion/feedback here"></textarea>
          <i class="small">Min 20 chracter and max 500 chracters</i>
        </div>
          <button type="submit" class="btn btn-primary">Submit</button>
</div>
</div>
</Div>
</form>
</div> 
<?php }?>
</Div>
<?php include_once('essential.php');?>  
<?php include_once("../includes/footerlinks.php");?>
<?php include_once("../includes/footer.php");?>
<script language="javascript">
(function ($) {
        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));

$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#msgsubject').val().length<=0){
		$('#msgsubject').focus();
		$errmsg='Message subject can not be blank\n';
		}
		
	if($('#msgbody').val().length<=20){
		$('#msgbody').focus();
		$errmsg=$errmsg+'Message body is either short or blank, please check\n';
		}	
	
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});
</script>
</body>
</html>