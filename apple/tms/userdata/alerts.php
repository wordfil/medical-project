<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='UALERTS';
include_once('../includes/connection.php');
if($_SESSION['ISLOGIN']<>'TRUE'){
	header("Location: ../index.php?info=Please login first");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("../includes/meta.php");?>
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link href="../css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../css/bootstrap.css">
<link rel="stylesheet" href="../css/datepicker.css">
<?php include_once("../includes/title.php");?>
</head>

<body>
<?php include_once("../includes/header.php");?>
<?php include_once("../includes/toplinks.php");?> 
<?php include('../includes/admin-alerts.php');?>

<Div id="midsection" class="clearall">
<h2><span class="label label-danger">Alerts <span class="badge alert-danger"><?php echo $alerttotal;?> </span> </span> </h2>   
<table class="table table-striped">
<tr class="info">
    <td><strong>Date</strong></td>
    <td><strong>Alert Description</strong></td> 
    <td><strong>Mark</strong></td> 
</tr>

<?php
$rst = mysql_query("Select * from alertofdeadlines WHERE active='Y' AND FIND_IN_SET($userid, empids)",$con);
$num_rows = mysql_num_rows($rst);

	if($num_rows>0){
		while($show = mysql_fetch_object($rst))
			{
			?>
            <tr>
             <td><?php echo($show->sysdate);?></td> 
             <td>
			  <strong><?php echo($show->policytitle);?></strong><br/>
              <?php echo($show->policies);?>
              </td>
              <td>
              <?php 
			 	 $nosempids =explode(',',$show->empids);
				 $arrlength = count($nosempids);
				if($arrlength>1){
			  ?>
              <a type="button" class="btn btn-primary btn-xs" href="../process/user-alert-set-active.php?id=<?php echo($show->id);?>&status=<?php echo($show->markasread);?>"><?php if($show->markasread=='N'){?>Read<?php }else{?>Unread<?php }?></a>
              <?php }else{?>
              <a type="button" class="btn btn-primary btn-xs disabled" href="#"><?php if($show->markasread=='N'){?>Read<?php }else{?>Unread<?php }?></a>
              <?php } ?>
              </td>
            </tr>
			<?php } } ?>
</table>  
</Div>
<?php include_once('essential.php');?>  
<?php include_once("../includes/footerlinks.php");?>
<?php include_once("../includes/footer.php");?>
<script language="javascript">
(function ($) {
        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
</script>
</body>
</html>