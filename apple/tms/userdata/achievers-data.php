<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='USERTA';
include_once('../includes/connection.php');
if($_SESSION['ISLOGIN']<>'TRUE'){
	header("Location: ../index.php?info=Please login first");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("../includes/meta.php");?>
<link rel="icon" href="../favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../favicon.ico" type="image/x-icon" />
<link href="../css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="../font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="../css/bootstrap.css">
<link rel="stylesheet" href="../css/datepicker.css">
<?php include_once("../includes/title.php");?>
</head>

<body>
<?php include_once("../includes/header.php");?>
<?php include_once("../includes/toplinks.php");?> 
<?php include('../includes/admin-alerts.php');?>

<?php
$cdate='01-'.$sysmonth.'-'.$sysyear;
$rst = mysql_query("Select * from toprank AS TR INNER JOIN employeedata AS E ON TR.empid=E.id INNER JOIN dbstate AS DS ON TR.stateid=DS.id WHERE TR.active='Y' AND TR.targetdate='$cdate' ORDER BY TR.widetype, TR.rank",$con);
	$num_rows = mysql_num_rows($rst);

if($num_rows>0){
	while($show = mysql_fetch_object($rst))
	{		
	$state=$show->statename;
	$empname=$show->firstname.' '.$show->lastname;
	$tardate=$show->targetdate;
		if($show->widetype==1){
			$both[]= $show->rank.'- '.$empname.' from '.$state;
		}
		if($show->widetype==2){
			$nwide[]= $show->rank.'- '.$empname.' from '.$state;
		}
		if($show->widetype==3){
			$swise[]= $show->rank.'- '.$empname.' from '.$state;
		}
	}
}
?>

<?php
function getdata($erg)
{
	$arrlength = count($erg);
	for($x = 0; $x < $arrlength; $x++) {
		echo $erg[$x];
		echo "<br/>";
	}
}
?>

<Div id="midsection" class="clearall">
<h2><span class="label label-success">Top Achievers <span class="badge alert-success"><?php echo $alerttotal;?> </span> </span> </h2>   


<div class="row container-fluid">
<?php if(count($both)!=0){?><div class="alert alert-info col-md-6 col-xs-offset-3" role="alert"><strong><i class="fa fa-trophy fa-5x"></i><h3>Nationwide and State wise winner!</h3><br/></strong> <?php getdata($both);?></div><?php }?>
<?php if(count($nwide)!=0){?><div class="alert alert-success col-md-6 col-xs-offset-3" role="alert"><strong><i class="fa fa-trophy fa-5x"></i><h3>Nationwide winner!</h3><br/></strong> <?php getdata($nwide);?></div><?php }?>
<?php if(count($swise)!=0){?><div class="alert alert-warning col-md-6 col-xs-offset-3" role="alert"><strong><i class="fa fa-trophy fa-5x"></i><h3>State wise winner!</h3><br/></strong> <?php getdata($swise);?></div><?php }?>
</div>
</Div>
<?php include_once('essential.php');?>  
<?php include_once("../includes/footerlinks.php");?>
<?php include_once("../includes/footer.php");?>
<script language="javascript">
(function ($) {
        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
</script>
</body>
</html>