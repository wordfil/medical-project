<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='INBOX';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">


<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
<h2>Admin mailbox</h2>
<?php
$rst = mysql_query("SELECT *, admin_inbox.id AS mid FROM admin_inbox INNER JOIN employeedata AS E ON uid=E.id order by admin_inbox.id DESC",$con);
$num_rows = mysql_num_rows($rst);
	if($num_rows>0){
?>

<h3 class="btn btn-primary">All messages</h3>

<div class="input-group"> <span class="input-group-addon">Filter</span>
    <input id="filter" type="text" class="form-control" placeholder="Type here...">
</div>

<div id="divTableDataHolder">
    <table class="table table-striped table-condensed searchable">
    <tr class="info">
    <td><strong>From</strong></td>
      <td><strong>Subject</strong></td>
      <td><strong>Message</strong></td>
      <td><strong>System Date and Time</strong></td>  
      <td><strong>Action</strong></td>
    </tr>
    <?php
    while($show = mysql_fetch_object($rst))
        {
        ?>
        <tr class="small">
            <td><?php echo($show->firstname.' '.$show->lastname);?></td>
            <td><?php echo($show->msgsubject);?></td>              
            <td><?php echo($show->msgbody);?></td>
            <td><?php echo($show->systime);?>, <?php echo($show->sysdate);?>, IP <?php echo($show->sysip);?></td>
            <td><a href="process/actions.php?mid=<?php echo($show->mid);?>">Delete</a></td>
        </tr>
        <?php } ?>
    </table>
</div>

<button id="myButtonControlID" class="btn btn-success pull-right"><i class="fa fa-2x fa-file-excel-o"></i> Export to Excel</button>              
<?php } ?>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[   

(function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
 
 $("[id$=myButtonControlID]").click(function(e) {
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=divTableDataHolder]').html()));
    e.preventDefault();
});
</script>

</body>
</html>