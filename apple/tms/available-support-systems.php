<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='ASSYS';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>

<script type="text/javascript">
tinymce.init({
    selector: "textarea",
	object_resizing : "img",

 
	plugins : 'media colorpicker insertdatetime advlist autolink link image lists charmap preview table emoticons textcolor paste jbimages code hr',
		toolbar: [
        "undo redo | styleselect | bold italic | link image alignleft aligncenter alignright insertdatetime emoticons forecolor backcolor jbimages"
    ],
	insertdatetime_formats: ["%d-%m-%Y", "%H:%M"],
	relative_urls: false,
	remove_script_host: false
	
 });

</script>

<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>Available support systems</h2>
 
<?php include('includes/admin-alerts.php');?>
   
 <form id="policyform" name="policyform" method="post" action="process/insertdb.php">
 <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <label class="control-label">Caption *</label>
            <input name="policytitle" type="text" class="form-control" id="policytitle" value="<?php echo $policytitle;?>" />
            </div>
            
        </div>
    </div>
 <div class="form-group">
        <div class="row">
              <div class="col-sm-12">
                <label class="control-label">Details *</label>
                <textarea name="editor" rows="15" id="editor" class="form-control" ><?php echo $rawrtfdata;?></textarea>
                <input type="hidden" id="editor_html" name="editor_html" />
            </div>
            
        </div>
    </div>
   <P>
   <select name="attachedfiles[]" id="attachedfiles" multiple class="form-control" style="visibility:hidden; height:0px;"></select>
</P>
 
     <div class="form-group">
        <div class="row">
              <div class="col-sm-12">

<div role="tabpanel">

  <!-- Nav tabs -->
  <ul class="nav nav-tabs" role="tablist">
    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Settings</a></li>
    <li role="presentation" style="visibility:hidden" id="attachment001"><a class="danger" href="#attach" aria-controls="attach" role="tab" data-toggle="tab">Attachment</a></li>
  </ul>

  <!-- Tab panes -->
  <div class="tab-content">
    	<div role="tabpanel" class="tab-pane active" id="home">

                <label class="btn btn-primary active"><input type="checkbox" id="addattachment" name="addattachment" onClick="openmanager();" autocomplete="off">&nbsp; Add attachment &nbsp;</label>
                <label class="btn btn-primary warning"><input type="checkbox" checked  autocomplete="off" id="isactive" name="isactive">&nbsp; Active &nbsp;</label>
                <label class="btn btn-primary active"><input type="checkbox" autocomplete="off" id="forceopen" name="forceopen">&nbsp; Force Open &nbsp;</label>
                
    	</div>
    	<div role="tabpanel" class="tab-pane" id="attach"><?php include_once("uploaded-files.php");?></div>
  </div>

</div>


            </div>

            
        </div>
    </div>
    

 	
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="reset" class="form-control btn-info">Clear</button>              
        	</div>
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary">Submit</button>              
        	</div>            
	  </div>     
   </div>
 </form>
 
 <?php //echo($rawrtfdata);?> 
  
<table class="table table-striped">
<tr class="info">
  <td><strong>Heading</strong></td>
  <td><strong>Log</strong></td>
  <td><strong>Active</strong></td>
  <td><strong>Action</strong></td>
</tr>

<?php
$rst = mysql_query("SELECT * FROM supportsystem order by id",$con);
$num_rows = mysql_num_rows($rst);

	if($num_rows>0){
		while($show = mysql_fetch_object($rst))
			{
			?>
            <tr>
              <td><a href="edit-available-support-systems.php?id=<?php echo($show->id);?>"><?php echo($show->policytitle);?></a></td>
              <td><?php echo($show->sysdate);?> - <?php echo($show->sysip);?></td>
              <td><?php echo($show->active);?></td>
              <td><a href="process/actions.php?id=<?php echo($show->id);?>">Delete</a></td>
            </tr>
			<?php } } ?>
</table>  
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[   									  
$(window).load(function(){
$('#datepicker').datepicker();
});//]]> 

$('#policyform').submit(function() {
	
	$errmsg='';	
	$('#editor_html').val(tinyMCE.get('editor').getContent());
	
	if($('#policytitle').val().length<=0){
		$('#policytitle').focus();
		$errmsg='Training caption can not be blank';
		}
	if($('#editor_html').val().length<=0){
		$('#editor').focus();
		$errmsg='Training information can not be blank';
		}
		
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{
	
	var selObj = document.getElementById('attachedfiles');
	  for (var i=0; i<selObj.options.length; i++) {
		selObj.options[i].selected = true;
	  }
		return true;	
		}
		
});
</script>

</body>
</html>