<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='PRODUCTUP';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<!-- Place inside the <head> of your HTML -->
<script type="text/javascript" src="tinymce/tinymce.min.js"></script>

<script type="text/javascript">
tinymce.init({
    selector: "textarea",
	object_resizing : "img",

 
	plugins : 'media colorpicker insertdatetime advlist autolink link image lists charmap preview table emoticons textcolor paste code hr',
		toolbar: [
        "undo redo | styleselect | bold italic | link image alignleft aligncenter alignright insertdatetime emoticons forecolor backcolor"
    ],
	insertdatetime_formats: ["%d-%m-%Y", "%H:%M"],
	relative_urls: false,
	remove_script_host: false
	
 });

</script>
 <?php
 	$id=$_GET['id'];
	  $rst = mysql_query("Select * from productinfo WHERE id=$id",$con);
	  while($show = mysql_fetch_object($rst))
	  {
		  $active=$show->active;
		  $forceopen=$show->forceopen;
		  $picname=$show->picname;
		  
		  $frontinfo =$show->frontinfo;
		  $backinfo=$show->backinfo;	
		  $description = $show->description;
		  $mediname=$show->mediname;			    
		  
	  }
?>
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>Product Master -Edit</h2>
 
<?php include('includes/admin-alerts.php');?>
   
<form id="form1" name="form1" method="post" action="process/update-product.php?id=<?php echo $id;?>" enctype="multipart/form-data">
  <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
          <?php 
		  if($picname=='NA')
		  {
		  ?>
            <img src="productpics/no_product.gif" alt="Your product picture" name="blah" width="300" height="200" class="img-thumbnail" id="blah" onload="if (this.width > 100) this.width = 250;"> 
           <?php
           }else{
			?>
            <img src="productpics/<?php echo($picname);?>" alt="Your product picture" name="blah" width="300" height="200" class="img-thumbnail" id="blah" onload="if (this.width > 100) this.width = 250;"> 
            <?php }?>               
            </div>
        </div>
    </div>
    
<div class="form-group">
        <div class="row">
              <div class="col-sm-12">
                <label class="control-label">Attach file here</label>
                <div style="position:relative;">
		<a class='btn btn-primary' href='javascript:;'>
			Browse picture...
			<input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="fileToUpload" id="fileToUpload" size="40"  onchange="readURL(this);">
		</a>
                <label class="control-label"></label>
          		   
                <p class="alert-warning">Size 350X250px, 350px width and 250px height Only jpg, jpeg, png, gif allowed</p>
            </div>
           
        </div>
    </div>

 <div class="form-group">
        <div class="row">
              <div class="col-sm-6">
                <label class="control-label">If product picture is not available then show this information *</label>
                <input maxlength="180" name="frontinfo" type="text" class="form-control" id="frontinfo" value="<?php echo $frontinfo;?>" />
                
                <label class="control-label">Product name *</label>
                <input maxlength="90" name="mediname" type="text" class="form-control" id="mediname" value="<?php echo $mediname;?>" />
                
                <label class="control-label">Fliped product information *</label>
                <input maxlength="280" name="backinfo" type="text" class="form-control" id="backinfo" value="<?php echo $backinfo;?>" />
                
                <label class="control-label">Product description</label>
                <textarea name="editor" rows="15" id="editor" class="form-control" ><?php echo $description;?></textarea>
                <input type="hidden" id="editor_html" name="editor_html" />
                <input type="hidden" id="oldpic" name="oldpic" value="<?php echo $picname;?>"/>
            </div>                      
        </div>
    </div>
    
  <div class="form-group">
        <div class="row">
              <div class="col-sm-6">
                <label class="btn btn-primary warning"><input name="isactive" type="checkbox" id="isactive" <?php if($active=='Y'){echo('checked');}?>  autocomplete="off">&nbsp; Active &nbsp;</label>
                <label class="btn btn-primary active"><input type="checkbox" autocomplete="off" id="forceopen" name="forceopen" <?php if($forceopen=='Y'){echo('checked');}?>>&nbsp; Force Open &nbsp;</label>
             </div>                      
        </div>
    </div>           
 	
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-3">
            <label class="control-label"></label>
           <button type="reset" class="form-control btn-info">Clear</button>              
        	</div>
            <div class="col-sm-3">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary">Submit</button>              
        	</div>            
	  </div>     
   </div>
 </form>

    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 
	$('#editor_html').val(tinyMCE.get('editor').getContent());
	
	if($('#frontinfo').val().length<=0){
		$('#frontinfo').focus();
		$errmsg='Please provide information\n';
		}
		
		if($('#mediname').val().length<=0){
		$('#mediname').focus();
		$errmsg =$errmsg+ 'Please provide product name\n';
		}
		
		if($('#backinfo').val().length<=0){
		$('#backinfo').focus();
		$errmsg =$errmsg+ 'Please provide flipped information';
		}
	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});

function readURL(input) {
			
			$("#upload-file-info").html($(input).val());
			
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
		
$("input#frontinfo").keyup(function(e){
  var val = $(this).val();
  $("input#mediname").val(val);
});
</script>

</body>
</html>