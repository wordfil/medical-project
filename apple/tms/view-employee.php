<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='VIEWEMP';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>View all employees</h2>
 
<?php include('includes/admin-alerts.php');?>   
 <div class="input-group"> <span class="input-group-addon">Filter</span>
    <input id="filter" type="text" class="form-control" placeholder="Type here...">
</div>
<table class="table table-striped searchable small">
<tr class="info">
  <td><strong>Emp Code</strong></td>
  <td><strong>Emp Photo</strong></td>  
  <td><strong>Name</strong></td>
  <td><strong>Adddress</strong></td>
  </td><td><strong>Role</strong></td>
  </td><td><strong>Immediate Manager</strong></td>
  <td><strong>Target Based Job</strong>
  <td><strong>Active</strong>
</tr>

<?php
$rst = mysql_query("SELECT *, employeedata.id as eid, employeedata.active as active FROM employeedata INNER JOIN designation ON designation.id=employeedata.designationid AND employeedata.isadmin='N' ORDER BY employeedata.ID",$con);
$num_rows = mysql_num_rows($rst);

	if($num_rows>0){
		while($show = mysql_fetch_object($rst))
			{
			?>
            <tr>
              <td><a href="edit-emp-register.php?id=<?php echo($show->eid);?>"><?php echo($show->empcode);?></a></td>
              <td>
              <?php if(($show->empphoto)=='NA')
				 {?>
				 <img src='emppics/nopic_2014.png' width="50" height="50"/>
				 <?php }else{?>
				 <img src='emppics/<?php echo($show->empphoto);?>'  width="50" height="50"/>
			<?php }?>
              </td>
              <td><?php echo($show->firstname.' '.$show->lastname);?><br>
              <span class="fa fa-calendar"></span> <?php echo($show->dateofbirth);?>
              </td>
              <td><?php echo($show->address);?><br>
              <span class="fa fa-phone-square"></span> <?php echo($show->landline);?> <span class="fa fa-mobile-phone"></span> <?php echo($show->mobile);?><br>
              <span class="fa fa-envelope"></span> <?php echo($show->emailid);?></td>
              <td><?php echo($show->designation);?></td>
              <td>
			  
			  	<?php
				$mgrid=$show->immidiateid;
				$rst2 = mysql_query("SELECT * FROM employeedata INNER JOIN designation ON employeedata.designationid=designation.id WHERE employeedata.id=$mgrid",$con);
				$num_rows2 = mysql_num_rows($rst2);
				if($num_rows2>0){
					
				$show2 = mysql_fetch_assoc($rst2);
					echo($show2['designation'].'<br>');
					echo($show2['firstname']);
				mysql_free_result($rst2);
				
				}else{
					echo('Self');
					}
				?>
              <?php echo($show->designation2);?>
              </td>
              <td><a href="process/change-target.php?id=<?php echo($show->eid);?>&act=<?php echo($show->istarget);?>"><?php echo($show->istarget);?></a></td>
              <td><a href="process/actions.php?id=<?php echo($show->eid);?>&act=<?php echo($show->active);?>"><?php echo($show->active);?></a></td>
            </tr>
			<?php } } ?>
</table>  
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
$('#datepicker').datepicker();
});//]]>  

 (function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
</script>

</body>
</html>