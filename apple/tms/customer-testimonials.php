<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='CT';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>Customer Testimonials</h2>
 <?php
	 	$rst = mysql_query("Select * from customertestimonials",$con);
	 	$show = mysql_fetch_object($rst);
		$testimonial = $show->testimonial;
		$title  = $show->title ;
?>
<?php include('includes/admin-alerts.php');?>
   
 <form id="form1" name="form1" method="post" action="process/insertdb.php">
  <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <label class="control-label">Title *</label>
            <input name="title" type="text" class="form-control" id="title" value="<?php echo $title;?>" />
            </div>
            
        </div>
    </div>
 <div class="form-group">
        <div class="row">
              <div class="col-sm-12">
                <label class="control-label">Customer Testimonials *</label>
                <textarea name="testimonial" class="form-control" id="testimonial"><?php echo $testimonial;?></textarea>
                
            </div>
            
        </div>
    </div>
    
 	<label class="btn btn-primary warning"><input type="checkbox" checked  autocomplete="off" id="isactive" name="isactive">&nbsp; Active &nbsp;</label>
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="reset"  class="form-control btn-info">Clear</button>              
        	</div>
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary">Submit</button>              
        	</div>            
	  </div>     
   </div>
 </form> 
 
 <table class="table table-striped">
<tr class="info">
  <td><strong>Title</strong></td>
  <td><strong>Testimonial</strong></td>
  <td><strong>Active</strong></td>
  <td><strong>System Date and Time</strong></td>  
  <td><strong>Action</strong></td>
</tr>

<?php
$rst = mysql_query("SELECT * FROM customertestimonials order by id",$con);
$num_rows = mysql_num_rows($rst);

	if($num_rows>0){
		while($show = mysql_fetch_object($rst))
			{
			?>
            <tr>
              <td><a href="edit-customer-testimonials.php?ctid=<?php echo($show->id);?>"><?php echo($show->title);?></a></td>              
              <td><?php echo(substr(($show->testimonial),0,20));?></td>
              <td><?php echo($show->active);?></td>
              <td><?php echo($show->sysdate);?><br><?php echo($show->sysip);?></td>
              <td><a href="process/actions.php?pid=<?php echo($show->id);?>">Delete</a></td>
            </tr>
			<?php } } ?>
</table>  
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#title').val().length<=0){
		$('#title').focus();
		$errmsg='Title thought can not be blank\n';
		}
	
	
	if($('#testimonial').val().length<=0){
		$('#testimonial').focus();
		$errmsg=$errmsg+'Testimonial thought can not be blank';
		}
	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});
</script>

</body>
</html>