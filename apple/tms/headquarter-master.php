<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='MRAREA';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>Assign Headquarters</h2>
 <?php include('includes/admin-alerts.php');?>
     
 <form id="form1" name="form1" method="post" action="process/insertdb.php">
 
    <div class="form-group">
        <div class="row">
            <div class="col-sm-6">
              <label class="control-label">Employee *</label>
                <select name="empname" id="empname" class="form-control">
                <option value=0>Select name</option>
				  <?php $rst = mysql_query("SELECT *,emp.id as empid FROM employeedata as emp INNER JOIN designation ON emp.designationid=designation.id WHERE emp.active='Y' AND istarget='Y' order by emp.firstname",$con);
                            while($show = mysql_fetch_object($rst))
                                {
                        ?>
                  <option value="<?php echo($show->empid); ?>"><?php echo($show->firstname); ?> <?php echo($show->lastname); ?> [<?php echo($show->empcode); ?>-<?php echo($show->designation); ?>]</option>
                  <?php }
				  mysql_free_result($rst);?>
                </select>
</div>

            <div class="col-sm-3">
              <label class="control-label">State *</label>
                <select name="state" id="state" class="form-control">
                <option value=0>Select state</option>
				  <?php $rst = mysql_query("SELECT * FROM dbstate WHERE active='Y' order by statename",$con);
                            while($show = mysql_fetch_object($rst))
                                {
                        ?>
                  <option value="<?php echo($show->id); ?>"><?php echo($show->statename); ?></option>
                  <?php }
				  mysql_free_result($rst);?>
                </select>

            </div>
            
              <div class="col-sm-3">
                <label class="control-label">City *</label>
                <select name="city" id="city" class="form-control">
                  <option value="0">none</option>
                </select>
            </div>
            
        </div>
    </div>
 	
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-3">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary">Assign headquarter</button>              
        	</div>            
	  </div>
      
   </div>
 </form>
 
 <div class="input-group"> <span class="input-group-addon">Filter</span>
    <input id="filter" type="text" class="form-control" placeholder="Type here...">
</div>
<table class="table table-striped searchable">
<tr class="info">
  <td><strong>Emp Code</strong></td>
  <td><strong>Name</strong></td>
  <td><strong>Designation</strong></td>
  <td><strong>State</strong></td>
  <td><strong>City</strong></td>
  <td><strong>Active</strong></td>
  <td><strong>Action</strong></td>
</tr>

<?php
$rst = mysql_query("SELECT emp.firstname AS empfname,emp.empcode AS empcode, emp.lastname AS emplname, city.city AS city, mra.active AS activestatus, mra.id AS mraid, desi.designation AS designation, DS.statename AS statename FROM mrarea as mra INNER JOIN dbcity as city ON mra.cityid=city.id INNER JOIN employeedata as emp ON mra.empid=emp.id INNER JOIN designation AS desi ON emp.designationid=desi.id INNER JOIN dbstate as DS ON city.stateid=DS.id",$con);

		while($show = mysql_fetch_object($rst))
			{
			?>
            <tr>
            <td><?php echo($show->empcode);?></td>
              <td><?php echo($show->empfname);?> <?php echo($show->emplname);?></td>
               <td><?php echo($show->designation);?></td>
              <td><?php echo($show->statename);?></td>
              <td><?php echo($show->city);?></td>
              <td><?php echo($show->activestatus);?></td>
              <td><a href="process/actions.php?id=<?php echo($show->mraid);?>"><span class="glyphicon glyphicon-floppy-remove"></span></a></td>
            </tr>
			<?php }  ?>
</table>  
 
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(document).ready(function() {
        $('#state').change(function(){
            $.ajax({
                type: "GET",
                url: "process/getcity.php",
                data: 'stateid=' + $('#state').val(),
                success: function(msg){
                    $('#city').html(msg);
                }

            }); // Ajax Call
        }); //event handler
    }); //document.ready

$('#form1').submit(function() {
	
	$errmsg='';					 
		if($('#empname').val()=='0'){
		$('#empname').focus();
		$errmsg='Employee not selected\n';
		}
		
	
	if($('#city').val()=='0'){
		$('#city').focus();
		$errmsg=$errmsg+'City not selected';
		}
	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});

(function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
</script>


</body>
</html>