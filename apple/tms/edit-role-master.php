<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='DBROLE';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>Designations/Roles -Edit</h2>
      
<?php include('includes/admin-alerts.php');?>
   <?php
	 	$id=$_GET['id'];
		$rst = mysql_query("Select * from designation WHERE id=$id",$con);
	 	$show = mysql_fetch_object($rst);
		$designation=$show->designation;
		$active=$show->active;
?>      
 <form id="form1" name="form1" method="post" action="process/updatedb.php?id=<?php echo $id;?>">
 <div class="form-group">
        <div class="row">
              <div class="col-sm-12">
                <label class="control-label">Role</label>
                <input name="designation" type="text" class="form-control" id="designation" value="<?php echo($designation); ?>" />
                <label class="btn btn-primary warning"><input name="isactive" type="checkbox" id="isactive" <?php if($active=='Y'){echo('checked');}?>  autocomplete="off">&nbsp; Active &nbsp;</label>
            </div>
            
        </div>
    </div>
    
 	
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="reset" class="form-control btn-info">Clear</button>              
        	</div>
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary">Submit</button>              
        	</div>            
	  </div>
      
   </div>
 </form>

    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#designation').val().length<=0){
		$('#designation').focus();
		$errmsg='Designation can not be blank';
		}
	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});  
</script>

</body>
</html>