<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='MRFORM';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>M R Reporting Form URL</h2>
 
<?php include('includes/admin-alerts.php');?>
   
 <form id="form1" name="form1" method="post" action="process/insertdb.php">
 <div class="form-group">
        <div class="row">
              <div class="col-sm-12">
                <label class="control-label">Form URL<small> - Type complete url: http://some domain or ip</small></label>
                <input name="formurl" type="text" class="form-control" id="formurl" />
            </div>
            
        </div>
    </div>
    
 	
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="reset" class="form-control btn-info">Clear</button>              
        	</div>
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary">Submit</button>              
        	</div>            
	  </div>     
   </div>
 </form>
  
  
<table class="table table-striped">
<tr class="info">
  <td><strong>FormURL</strong></td>
  <td><strong>System Date and Time</strong></td>
  <td><strong>System IP</strong></td>
  <td><strong>Action</strong></td>
</tr>

<?php
$rst = mysql_query("SELECT * FROM mrform order by id",$con);
$num_rows = mysql_num_rows($rst);

	if($num_rows>0){
		while($show = mysql_fetch_object($rst))
			{
			?>
            <tr>
              <td><?php echo($show->formurl);?></td>
              <td><?php echo($show->sysdate);?></td>
              <td><?php echo($show->ip);?></td>
              <td><a href="process/actions.php?mfid=<?php echo($show->id);?>">Delete</a></td>
            </tr>
			<?php } } ?>
</table>  
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
$('#datepicker').datepicker();
});//]]>  
</script>

</body>
</html>