<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='DBEMP';
include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");
include_once("includes/toplinks.php");

?>
<Div id="midsection" class="clearall">
<?php include('includes/admin-alerts.php');?>

    <Div id="loginform">
      <h2>New Registration</h2>
      <h6>* fields required</h6>
     
<form name="empform" method="post" action="process/insert-employee.php" enctype="multipart/form-data" id="empform">
  <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <img src="emppics/nopic_2014.png" alt="Your profile picture" name="blah" width="100" height="100" class="img-thumbnail" id="blah" onload="if (this.width > 100) this.width = 150;">               
            </div>
        </div>
    </div>

    
 	<div class="form-group">
        <div class="row">
          <div class="col-sm-3">
            <label class="control-label">First name *</label>
              <input name="name" type="text" class="form-control" id="name" value="<?php echo $_SESSION['name'];?>" />
            </div>

            <div class="col-sm-3">
                <label class="control-label">Last name *</label>
                <input name="lastname" type="text" class="form-control" id="lastname"  value="<?php echo $_SESSION['lastname'];?>"/>
            </div>
            
            <div class="col-sm-3">
                <label class="control-label">Employee code *</label>
                <input name="empcode" type="text" class="form-control" id="empcode"  value="<?php echo $_SESSION['empcode'];?>"/>
            </div>
            
            <div class="col-sm-3">
                <label class="control-label">Email *</label>
                <input name="email" type="email" class="form-control" id="email" value="<?php echo $_SESSION['email'];?>" />
            </div>

        </div>
    </div>
 
 
  <div class="form-group">
        <div class="row">
          <div class="col-sm-12">
            <label class="control-label">Address *</label>
              <input name="address" type="text" class="form-control" id="address"  value="<?php echo $_SESSION['address'];?>"/>
            </div>
        </div>
    </div>
    
    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
              <label class="control-label">State *</label>
                <select name="state" id="state" class="form-control">
                <option value=0>Select state</option>
				  <?php $rst = mysql_query("SELECT * FROM dbstate WHERE active='Y' order by statename",$con);
                            while($show = mysql_fetch_object($rst))
                                {
                        ?>
                  <option value="<?php echo($show->id); ?>"><?php echo($show->statename); ?></option>
                  <?php }
				  mysql_free_result($rst);?>
                </select>

            </div>
            
                         <div class="col-sm-3">
                <label class="control-label">Headquarter *</label>
                <select name="city" id="city" class="form-control">
                  <option value="0">none</option>
                </select>
            </div>
            

            <div class="col-sm-3">
                 <div class="input-append date" id="datepicker" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy">
                 <label class="control-label">Date of Birth</label>
                    <span class="add-on"><input name="dob" type="text" class="span2 form-control" id="dob" size="10" data-bind="value: Customer.DateOfBirth" value="<?php echo $_SESSION['dob'];?>"/></span>
                </div>
            </div>

          <div class="col-sm-3">
            <label class="control-label">Pincode</label>
              <input name="pincode" type="text" class="form-control" id="pincode"value="<?php echo $_SESSION['pincode'];?>" />
            </div>
        </div>
    </div>
    
	<div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <label class="control-label">Landline</label>
                <input name="landline" type="text" class="form-control" id="landline" value="<?php echo $_SESSION['landline'];?>"/>
            </div>

            <div class="col-sm-3">
                <label class="control-label">Mobile *</label>
                <input name="mobile" type="text" class="form-control" id="mobile" value="<?php echo $_SESSION['mobile'];?>"/>
            </div>

            <div class="col-sm-3">
              <label class="control-label">Current role *</label>
               <select name="designation" id="designation" class="form-control">
				  <?php $rst = mysql_query("SELECT * FROM designation WHERE active='Y' order by id desc",$con);
                            while($show = mysql_fetch_object($rst))
                                {								
		
                        ?>
                  <option value="<?php echo($show->id); ?>"><?php echo($show->designation); ?></option>
                  <?php 				  
				  }
				  mysql_free_result($rst);?>
                </select>

            </div>
            
            <div class="col-sm-3">
                <label class="control-label">Reporting to [Immediate Manager] *</label>
                
                <select name="mgrid" id="mgrid" class="form-control">      
     			<option value="0">self</option>
					<?php $rst = mysql_query("SELECT emp.id as eid,desi.designation as des, emp.firstname as empname FROM employeedata as emp,designation as desi where emp.designationid=desi.id order by designationid",$con);
                        while($show = mysql_fetch_object($rst))
                            {
								if($optpval!=($show->des)){
									$optpval=$show->des;
									echo('<optgroup label='.($show->des).'>');
									}
                    ?>
            	<option value="<?php echo($show->eid); ?>"><?php echo(($show->des)."-".($show->empname)); ?></option>
      				<?php 
					if($optpval!=($show->des)){
		  					$optpval=$show->des;
							echo('</optgroup>');
			  
						}
					}
					mysql_free_result($rst);?>
     			 </select>
            </div>
            
        </div>
    </div>
    
    
    <div class="form-group">
        <div class="row">
            <div class="col-sm-3">
                <label class="control-label">Password *</label>
                <input name="password" type="text" maxlength="20" class="form-control" id="password" value="<?php echo $_SESSION['pwd'];?>"/>
            </div>

            <div class="col-sm-3">
                <label class="control-label">Confirm password *</label>
                <input name="confirmpassword" type="text" maxlength="20" class="form-control" id="confirmpassword" value="<?php echo $_SESSION['pwd'];?>"/>
            </div>
        </div>
    </div>
    
    <div class="form-group alert-danger">
    <div class="row"> 
   <div class="radio">
   <label class="text-danger"><strong>Is this job is target based </strong></label>
     <label class="text-danger"><input class="radio-inline" type="radio" name="istarget" id="istarget" value="Y" checked>Yes</label>
     <label class="text-danger"><input class="radio-inline" type="radio" name="istarget" id="istarget1" value="N" checked>No</label>
   </div>
   </div>
   </div> 
    
	<div class="form-group">
        <div class="row">
            <div class="col-sm-12">
            <label class="control-label">Upload employee picture</label>
                  <div style="position:relative;">
		<a class='btn btn-primary' href='javascript:;'>
			Browse picture...
			<input type="file" style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;' name="fileToUpload" id="fileToUpload" size="40"  onchange="readURL(this);">
		</a>
        <br/>
		<small>Picture must be in jpg or jpeg format and less than 1MB</small>
		<span class='label label-info' id="upload-file-info"></span>
	</div>
      </div>              
	  </div>
      </div>      
      
      <div class="form-group">
        <div class="row">     
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="reset" class="form-control btn-info">Clear</button>              
        	</div>
            <div class="col-sm-6">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-primary" id="submitform" name="submitform">Submit</button>              
        	</div>            
	  </div>
      
   </div>
 </form>
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
$('#datepicker').datepicker();
});//]]>  


        function readURL(input) {
			
			$("#upload-file-info").html($(input).val());
			
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#blah').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

$(document).ready(function() {
        $('#state').change(function(){
            $.ajax({
                type: "GET",
                url: "process/getcity.php",
                data: 'stateid=' + $('#state').val(),
                success: function(msg){
                    $('#city').html(msg);
                }

            }); // Ajax Call
        }); //event handler
    }); //document.ready


$('#empform').submit(function() {
		 
	 $errmsg='';					 
	
	if($('#password').val().length<=0){	
		$('#password').val(($('#name').val().slice(0, 4)) + ((($('#mobile').val()).split("").reverse().join("")).slice(0, 4)));
		$('#confirmpassword').val($('#password').val())
		}
	if($('#name').val().length<=0){
		$('#name').focus();
		$errmsg='Name can not be blank';
	}
	if($('#lastname').val().length<=0){
		$('#lastname').focus();
		$errmsg=$errmsg+'\n Last name can not be blank';
	}
	if($('#empcode').val().length<=0){
		$('#empcode').focus();
		$errmsg=$errmsg+'\n Employee code can not be blank';
	}
	
	if($('#email').val().length<=0){
		$('#email').focus();
		$errmsg=$errmsg+'\n Email can not be blank';
	}
	
	var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	var $emailresult=regex.test($('#email').val());
												
  	if($emailresult==false){
		$errmsg=$errmsg+'\n Please enter valid email account';
	}

	
	if($('#address').val().length<=0){
		$('#address').focus();
		$errmsg=$errmsg+'\n Address can not be blank';
	}
	if($('#state').val()==0){
		
		$errmsg=$errmsg+'\n Please select state';
	}
	if($('#dob').val().length<=0){
		$('#dob').val('01-01-1900');
		//$errmsg=$errmsg+'\n Date of birth can not be blank';
	}
	if($('#pincode').val().length<=0){
		$('#pincode').focus();
		$('#pincode').val('000000');
		//$errmsg=$errmsg+'\n Pincode can not be blank';
	}
	if($('#landline').val().length<=0){
		$('#landline').val('NA');
	}
	if($('#mobile').val().length<=0){
		$('#mobile').focus();
		$errmsg=$errmsg+'\n Mobile can not be blank';
	}
	if($('#password').val().length<=0){
		$('#password').focus();
		$errmsg=$errmsg+'\n Password can not be blank';
	}
	if($('#confirmpassword').val().length<=0){
		$errmsg=$errmsg+'\n Confirm password can not be blank';
	}
	if($('#password').val() != $('#confirmpassword').val()){
		$errmsg=$errmsg+'\n Password and confirm password mismatch';
	}
	
	
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{
		return true;	
		}
		
});


</script>

</body>
</html>