<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='UPLOADMGR';

include_once('includes/admin-permissions.php');
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
    <Div id="loginform">
      <h2>Upload Manager</h2>
 
<?php include('includes/admin-alerts.php');?>

  
  <form id="form1" name="form1" method="post" action="process/upload-file.php" enctype="multipart/form-data">
 <div class="form-group">
        <div class="row">
              <div class="col-sm-12">
                <label class="control-label">Attach file here</label>
                <input type="file" name="fileToUpload" id="fileToUpload" size="40">
                <label class="control-label"></label>
          		   
                <p class="alert-warning">Only doc, docx, pdf, xls, xlsx, jpg, jpeg, png, gif allowed</p>
                <button type="submit" class="btn btn-primary btn-lg">Upload</button>  
            </div>
           
        </div>
    </div>
 </form>
  
  
<table class="table table-striped">
<tr class="info">
  <td ><strong>File</strong></td>
  <td ><strong>Type</strong></td>
  <td ><strong>Details</strong></td>
  <td ><strong>Upload by</strong></td>
  <td><strong>Action</strong></td>
</tr>

<?php
$rst = mysql_query("SELECT sysip,sysdate,lastname,firstname,filename,sysfiletype, ud.id as udid FROM uploaddetails as ud INNER JOIN employeedata as ed ON ed.id=ud.userid WHERE ud.active='Y' order by ud.id DESC",$con);
$num_rows = mysql_num_rows($rst);

	if($num_rows>0){
		while($show = mysql_fetch_object($rst))
			{
				$getfiletype=$show->sysfiletype;
			?>
            <tr>
              
              <?php
			  if($getfiletype=='jpg'||$getfiletype=='jepg'||$getfiletype=='png'||$getfiletype=='gif'){
              ?>
			  <td><a rel="popover" data-img="<?php echo($show->filename);?>"><?php echo($show->filename);?></a></td>
              <?php }else{?>
               <td><a target="_blank" href="empfiles/<?php echo($show->filename);?>"><?php echo($show->filename);?></a></td>
              <?php }?>
              <td>
			  <?php 
			  	
				switch ($getfiletype) {
					case "pdf":
						echo "<i class='fa fa-file-pdf-o fa-2x'></i>";
						break;
					case "doc":
						echo "<i class='fa fa-file-word-o fa-2x'></i>";
						break;
					case "docx":
						echo "<i class='fa fa-file-word-o fa-2x'></i>";
						break;
					case "xls":
						echo "<i class='fa fa-file-excel-o fa-2x'></i>";
						break;
					case "xlsx":
						echo "<i class='fa fa-file-excel-o fa-2x'></i>";
						break;
					case "ppt":
						echo "<i class='fa fa-file-powerpoint-o fa-2x'></i>";
						break;
					case "pptx":
						echo "<i class='fa fa-file-powerpoint-o fa-2x'></i>";
						break;
					case "jpg":
						echo "<i class='fa fa-file-image-o fa-2x'></i>";
						break;
					case "jpeg":
						echo "<i class='fa fa-file-image-o fa-2x'></i>";
						break;
					case "png":
						echo "<i class='fa fa-file-image-o fa-2x'></i>";
						break;
					case "gif":
						echo "<i class='fa fa-file-image-o fa-2x'></i>";
						break;
					default:
						echo "<span class='fa fa-file fa-2x'></span>";
				}
			  ?>              
              </td>
              <td><?php echo($show->sysdate);?><br>IP-<?php echo($show->sysip);?></td>
              <td><?php echo($show->firstname);?> <?php echo($show->lastname);?></td>
              <td><a href="process/actions.php?id=<?php echo($show->udid) ?>&sysfile=<?php echo($show->filename);?>">Delete</a></td>
            </tr>
			<?php } } ?>
</table>  
  
    </Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
$('#datepicker').datepicker();
});//]]> 

$('a[rel=popover]').popover({
  html: true,
  trigger: 'hover',
  placement: 'bottom',
  content: function(){return '<img src="empfiles/'+$(this).data('img') + '" />';}
}); 
</script>

</body>
</html>