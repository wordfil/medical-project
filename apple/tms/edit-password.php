<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='EDITPASSWORD';
include_once('includes/connection.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">
      <h2>Change password</h2>

<?php include('includes/admin-alerts.php');?>
   
 <form id="form1" name="form1" method="post" action="process/updatedb.php">
<Div id="loginform1" class="col-sm-4 col-md-offset-4">
<div class="panel panel-primary">
    <div class="panel-heading"><h3>Change password</h3></div>       
	<div class="panel-body">
    
  <div class="form-group">
            <label class="control-label">New Password *</label>
            <input name="password" type="password" class="form-control" maxlength="20" id="password"/>
    </div>
    
  <div class="form-group">
            <label class="control-label">Confirm Password *</label>
            <input name="password2" type="password" class="form-control" maxlength="20" id="password2"/>
    </div>
    

      <div class="form-group">
        <div class="row">     
            <div class="col-sm-3">
            <label class="control-label"></label>
           <button type="submit" class="form-control btn-warning">Submit</button>              
        	</div>            
	  </div>     
   </div>
</div>
</div>
</div>
 </form> 
 

</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#password').val().length<=0){
		$('#password').focus();
		$errmsg='Password can not be blank\n';
		}
		
	if($('#password2').val().length<=5){
		$('#password2').focus();
		$errmsg=$errmsg+'Password is too short \n';
		}	
	
	if($('#password2').val().length<=0){
		$('#password2').focus();
		$errmsg=$errmsg+'Confirm password can not be blank \n';
		}
	
	if($('#password2').val()!=$('#password').val()){
		$('#password2').focus();
		$errmsg=$errmsg+'Password mismatch please check \n';
		} 
 
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});
</script>

</body>
</html>