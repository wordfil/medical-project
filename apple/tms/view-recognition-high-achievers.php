<?php
ob_start();
session_start();
include_once('includes/connection.php');
$_SESSION['FORMPOS']='VDBTOPA';
include_once('includes/admin-permissions.php');
$getdate=$_SESSION['getdate'];
?>

<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/datepicker.css">
<?php include_once("includes/title.php");?>

</head>

<body>
<?php include_once("includes/header.php");?>
<?php include_once("includes/toplinks.php");?>
<Div id="midsection" class="clearall">

<Div id="loginform">
      <h3>View Highest Achievers Data</h3>
<?php include('includes/admin-alerts.php');?>
     
<form action="process/get-achievers-sessions.php" form name="datesubmit" id="datesubmit" method="POST">
  <label class="control-label">Show high achievers for *</label>  
  <div class="row">
  <div class="col-sm-3">
          
    <div class="input-append date" id="datepicker" data-date="dateValue: Customer.DateOfBirth" data-date-format="dd-mm-yyyy" data-date-minviewmode="months" data-date-viewmode="months">
      <span class="add-on">
      <input name="datenew" type="text" class="span2 form-control" id="datenew" size="10" readonly="" data-bind="value: Customer.DateOfBirth" value="<?php echo($getdate);?>"/>
      </span>      
    </div>
      
  </div>            
  </div>

  <label class="control-label"></label>         
  <div class="row">
  <div class="col-sm-3">        
      <input type="submit" value="Show Targets" class="btn-primary btn btn-success"> 	
  </div>            
  </div> 

</form>
</div> 
 
    <div class="input-group"><span class="input-group-addon">Filter</span>
        <input id="filter" type="text" class="form-control col-sm-3" placeholder="Type here...">   
    </div>
    
<div id="divTableDataHolder">
<table class="table table-hover table-striped searchable">
      <tr class="info">
        <td><strong>Sno</strong></td>
        <td><strong>Emp Code</strong></td>
        <td><strong></strong></td>
        <td><strong>Name</strong></td>
        <td><strong>Month</strong></td>
        <td><strong>Zone</strong></td>
        <td><strong>Division</strong></td>        
        <td><strong>Rank</strong></td>
        <td></td>
        <td><strong>Note</strong></td>
         <td><strong>Actions</strong></td>
      </tr>
      
<?php

$sqlquery="SELECT *,TR.id AS topid, TR.active AS tactive FROM toprank AS TR INNER JOIN employeedata AS E ON TR.empid=E.id INNER JOIN designation AS D ON E.designationid=D.id INNER JOIN dbcity AS DC ON TR.cityid=DC.id INNER JOIN dbstate AS DS ON TR.stateid=DS.id WHERE TR.targetdate='$getdate' ORDER BY TR.id DESC";		


		$rst = mysql_query($sqlquery,$con);
		$loopi=1;
		$targettotal=0;
		$targettotaldone=0;
				while($show = mysql_fetch_object($rst))
				
					{
						?>
      <tr class="small">
        <td><?php echo($loopi);?>     
          
          </td>

          <td><?php echo($show->empcode);?>
          </td>
          
        <td>
        <?php if(($show->empphoto)=='NA')
				 {?>
				 <img src='emppics/nopic_2014.png' width="50" height="50" class="img-circle"/>
				 <?php }else{?>
				 <img src='emppics/<?php echo($show->empphoto);?>'  width="50" height="50" class="img-circle"/>
			<?php }?>
        </td>
          
        <td>
		<?php echo($show->firstname .' '.$show->lastname);?><br>
        <?php echo($show->designation);?></td>
        <td>
		<?php 
		$mydate =$show->targetdate;
		$month = date("M",strtotime($mydate));
		$year = date("Y",strtotime($mydate));
		?>
        <?php echo($month.' '.$year);?>
          </td>
          
         <td>
        <?php echo($show->statename);?>
        </td>
                         
        <td>
        <?php echo($show->city);?>
        </td>         
       
        <td>
<div class="form-group">
         <label class="col-sm-1 fa fa-4x fa-trophy" ></label>
         
         <div class="col-sm-7" style="color:#fff; padding-top:2px; padding-left:3px; font-size:20px; font-weight:bold;">
         <?php echo($show->rank); ?>
         </div>
</div>
        </td>
        
        <td>
         <?php 
		 if($show->widetype==2){ 
			 echo('National'); 
		 }
		 if($show->widetype==3){ 
			 echo('Zonal'); 
		 }
		 if($show->widetype==1){ 
			 echo('National and Zonal'); 
		 }
		 ?>
        </td>
        
         <td class="small">
        	<?php echo($show->description); ?>
        </td>
        
        <td>
         <a href="#" widetype="<?php echo($show->widetype);?>" rank="<?php echo($show->rank); ?>" id="btnform<?php echo($loopi);?>" des="<?php echo($show->description); ?>" rel="<?php echo $show->topid;?>" class="btnform" data-toggle="modal" data-target=".bs-example-modal-sm"> Edit </a> | <a href="process/achievers-set-active.php?topid=<?php echo $show->topid;?>&status=<?php if(($show->tactive)=='Y'){ echo('N');}else{ echo('Y');}?>"><?php if(($show->tactive)=='Y'){ echo('Active');}else{ echo('Inactive');}?></a> |  <a href="process/actions.php?topid=<?php echo $show->topid;?>"> Delete</a>
        </td>
      </tr>
      
              <?php 
			  $loopi=$loopi+1;
			  $getdate= $show->targetdate;
			  }
			  $_SESSION['loopi']='';
			  $_SESSION['loopi']=$loopi-1;
			  ?>
              
</table>

</div>
<button id="myButtonControlID" class="btn btn-success pull-right"><i class="fa fa-2x fa-file-excel-o"></i> Export to Excel </button>


<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  
  
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h2 class="modal-title">Edit Achievers Data</h2>
          </div>
      
    
       <form action="process/updatedb.php" form name="hadata" id="hadata" method="POST" class="form-horizontal">
           
       <div class="form-group" style="padding:50px;">
       <input type="hidden" class="form-control" id="dbid" name="dbid">         
       <label for="rank">Edit rank:</label>
       <Select size="1" name="rank" id="rank" class="form-control">
       <?php
       for($j=1; $j<=5;$j++)
	   {
	   ?>
        <Option value="<?php echo $j;?>" selected><?php echo $j;?></Option>
        <?php }?>
        </Select>
        <br/>
       
       <label for="rank">Winner for:</label>
       <Select size="1" name="widetype" id="widetype" class="form-control">
            <option value="2">National</option>
            <option value="3" selected>Zonal</option>
            <option value="1">Both</option>        
        </Select>
        <br/>
          <label for="description">Edit description:</label>
          <textarea name="description" class="form-control" id="description" rows="3"></textarea>        
        <br/>
        <button type="submit" class="btn btn-default">Submit</button>
                
        </div>
  		</form>
    
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
      
    </div>
  </div>
</div>            
  
  
  
  
</Div>
</Div>
 
<?php include_once("includes/footerlinks.php");?>
<?php include_once("includes/footer.php");?>


<script type='text/javascript'>//<![CDATA[ 
$(window).load(function(){
var d = new Date();
//$('#datenew').val('01'+'-'+(d.getMonth()+1+'-'+d.getFullYear()));
$('#datepicker').datepicker('hide');

$('#example').dataTable( {
        "sDom": "<'row'<'span6'l><'span6'f>r>t<'row'<'span6'i><'span6'p>>"
    } );

});//]]> 

$("[id$=myButtonControlID]").click(function(e) {
    window.open('data:application/vnd.ms-excel,' + encodeURIComponent( $('div[id$=divTableDataHolder]').html()));
    e.preventDefault();
});


(function ($) {

        $('#filter').keyup(function () {

            var rex = new RegExp($(this).val(), 'i');
            $('.searchable tr').hide();
            $('.searchable tr').filter(function () {
                return rex.test($(this).text());
            }).show();

        })

    }(jQuery));
 

$('.btnform').click(function(e) {
	var txt = $(e.target).attr('rel');
	var id = $(e.target).attr('id');
	var rank = $(e.target).attr('rank');
	var des = $(e.target).attr('des');
	var wt = $(e.target).attr('widetype');
	//alert(wt);

$('#description').val(des);	
$('#dbid').val(txt);	

var element = document.getElementById('rank');
    element.value = rank;

var element2 = document.getElementById('widetype');
    element2.value = wt;
	
});
</script>

</body>
</html>