<?php
ob_start();
session_start();
$_SESSION['FORMPOS']='CLOGIN';
include_once('includes/connection.php');
if($_SESSION['ISLOGIN']=='TRUE' && $_SESSION['ISADMIN']=='TRUE'){
	header("Location: dashboard.php?info=".$_SESSION['username']." you are already logged in. Please logout before new signin");
	}
	
if($_SESSION['ISLOGIN']=='TRUE' && $_SESSION['ISADMIN']=='FALSE'){
	header("Location: userdata/user-dashboard.php?info=".$_SESSION['username']." you are already logged in. Please logout before new signin");
	}
?>
<!DOCTYPE html>
<html lang="en">
<head>
<?php include_once("includes/meta.php");?>
<link rel="icon" href="favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<link href="css/main.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">

<?php include_once("includes/title.php");?>
</head>

<body>
<?php include_once("includes/header.php");?>

<Div id="midsection" class="clearall">
<?php include('includes/admin-alerts.php');?>
<form id="form1" name="form1" method="post" action="process/checklogin.php">   

<Div id="loginform1" class="col-sm-4 col-md-offset-4">
<div class="panel panel-warning">
    <div class="panel-heading"><h3>Login here</h3></div>       
	<div class="panel-body">
      
        <div class="form-group">
          <label for="email">Enter M R Reporting username:</label>
          <input type="text" class="form-control" id="email" placeholder="Please enter your M R Reporting username" name="email">
        </div>
        <div class="form-group">
          <label for="pwd">Password:</label>
          <input type="password" class="form-control" id="pwd" placeholder="Enter password" name="pwd">
        </div>
          <button type="submit" class="btn btn-default">Submit</button>
          
          <div class="form-group">
          <br/>
          <label><a href="forgot.php">Forget password</a></label>
          </div>
</div>
</div>
</Div>
</form>
</Div>

<?php include_once("includes/footer.php");?>
<script src="js/jquery.js"></script>
<script src="js/bootstrap.js"></script>
<script type='text/javascript'>//<![CDATA[ 
$('#form1').submit(function() {
	
	$errmsg='';					 

	if($('#email').val().length<=0){
		$('#email').focus();
		$errmsg='Please enter valid M R Reporting username\n';
		}
		if($('#pwd').val().length<=0){
		$('#pwd').focus();
		$errmsg=$errmsg+'Please enter valid M R Reporting password';
		}	
  
	if(($errmsg.length)>0)
	{
		alert($errmsg);
		return false;	
	}else{	
		return true;	
		}
		
});
</script>
</body>
</html>