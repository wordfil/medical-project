<style>
.wrapper{width: 500px;margin: 0 auto;font-family: Georgia, "Times New Roman", Times, serif;}
.wrapper > ul#results li{margin-bottom: 1px;background: #f9f9f9;padding: 20px;list-style: none;}
.loading-info{text-align:center;}
</style>


<link rel="stylesheet" type="text/css" href="css/jquery.ma.infinitypush.css" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<section style="margin-top:3px;">
		<style>
		@font-face { font-family: 'ravi'; src: url('font/ab.ttf'); }
		.text h1,h2,h3{ font-family:ravi,calibri; color:white; font-weight:bold;  width: 100%; }		
		</style>
		<div style="background:url(images/3.jpg); background-size:cover; padding:90px 30px;">
		  
		  <div style="text-align:right;" class="text"><h3>L'emollis range</h3></div>
		  
		</div>	
</section>
<section style="background:#D8D9D6;">
    <div class="container">
			
			
			<div class="col-md-12 col-xs-12 col-sm-12">
				    <div class="panel-heading"> <?php company(); ?> --> <b>Product's --> L'emollis range</b></div>
			</div>
          
    </div>            
</section>
<section style="padding:0px 0px; margin-top:0px; background:white;">
    <div class="container">
	<div class="col-md-3 col-sm-12 col-xs-12" style="padding:40px 10px;">
	  <?php include("include/product_load.php"); ?>

	</div>
	
	<div class="col-md-9 col-sm-12 col-xs-12" style="padding:30px 10px 10px 10px;">
	
	
	<div id="results"></div>
	</div>
    </div>            
</section>


  
   

		<script type="text/javascript">
		var track_page = 1; //track user scroll as page number, right now page number is 1
		var loading  = false; //prevents multiple loads

		load_contents(track_page); //initial content load

		$(window).scroll(function() { //detect page scroll
			if($(window).scrollTop() + $(window).height() >= $(document).height()) { //if user scrolled to bottom of the page
				track_page++; //page number increment
				load_contents(track_page); //load content	
			}
		});		
		//Ajax load function
		function load_contents(track_page){
			if(loading == false){
				loading = true;  //set loading flag on
				$('.loading-info').show(); //show loading animation 
				$.post( 'include/lemollis_page_load.php', {'page': track_page}, function(data){
					loading = false; //set loading flag off once the content is loaded
					if(data.trim().length == 0){
						//notify user if nothing to load
						$('.loading-info').html("No more records!");
						return;
					}
					$('.loading-info').hide(); //hide loading animation once data is received
					$("#results").append(data); //append data into #results element
				
				}).fail(function(xhr, ajaxOptions, thrownError) { //any errors?
					alert(thrownError); //alert with HTTP error
				})
			}
		}
		</script>


