<!DOCTYPE html>

<html lang="en">

<head>
<link rel="stylesheet" href="js/css/megamenu.css"><!-- Mega Menu Stylesheet -->

<script type="text/javascript" src="js/jquery.js"></script><!-- jQuery -->
<script type="text/javascript" src="js/megamenu_plugins.js"></script><!-- Mega Menu Plugins -->
<script type="text/javascript" src="js/megamenu.js"></script><!-- Mega Menu Script -->
<script>
$(document).ready(function($){
    $('.megamenu').megaMenuCompleteSet({
        menu_speed_show : 300, // Time (in milliseconds) to show a drop down
        menu_speed_hide : 0, // Time (in milliseconds) to hide a drop down
        menu_speed_delay : 0, // Time (in milliseconds) before showing a drop down
        menu_effect : 'hover_fade', // Drop down effect, choose between 'hover_fade', 'hover_slide', etc.
        menu_click_outside : 1, // Clicks outside the drop down close it (1 = true, 0 = false)
        menu_show_onload : 0, // Drop down to show on page load (type the number of the drop down, 0 for none)
        menu_responsive:1 // 1 = Responsive, 0 = Not responsive
    });
});
</script>

<style>
.city {display:none;}
.city h2 { color:gray; }
</style>

</head>
<body>

    <div class="megamenu_container megamenu_dark_bar megamenu_light" id="advanced"><!-- Begin Menu Container -->

        
        <ul class="megamenu"><!-- Begin Mega Menu -->
           
			<li class="megamenu_button"><a href="#_" style="background:url(images/dropdown.jpg) right no-repeat;"><center><img src="images/header.png" width="300px" style="padding:0px 0px;" class="swingimage"></center> </a></li>
			<li><a href="index.php">Home</a></li><!-- End Item -->
			<li><a href="index.php?about=About">About us</a>
				<div class="dropdown_2columns dropdown_container"><!-- Begin Item Container -->
					<ul class="dropdown_flyout">
						<li><a href="index.php?about=About">&nbsp;&nbsp;&nbsp; Company Profile</a></a></li>
						<li><a href="index.php?about=history">&nbsp;&nbsp;&nbsp; History</a></a></li>
						<li><a href="index.php?about=mission">&nbsp;&nbsp;&nbsp; Mission</a></li>
						<li><a href="index.php?about=business">&nbsp;&nbsp;&nbsp; Business Opportunities </a></li>
						<li><a href="index.php?about=network">&nbsp;&nbsp;&nbsp; Our Network</a>  </a></li>
                    </ul>                       
                
                </div><!-- End Item Container -->
            </li><!-- End Item -->
			
			<li><a href="index.php?about=Products" class="tablink" onmouseover="openCity(event, 'main')">Products</a>
				
				<div class="dropdown_fullwidth"><!-- Begin Item Container -->
					
					<div class="col-md-2" class="tablink" onload="openCity(event, 'load')" style="text-align:left;">
					<ul>
						
						<li style="padding:0px 0px;"><a href="" class="tablink">&nbsp;&nbsp;&nbsp; <h4 style="color:#A52A2A;">Therapeutic Areas </h4></a></li>
						<li style="padding:5px 0px;"><a href="index.php?about=Trichology" class="tablink" onmouseover="openCity(event, 'Trichology')">&nbsp;&nbsp;&nbsp; <strong>Trichology</strong> </a></li>
						<li style="padding:5px 0px;"><a href="index.php?about=Dermatology" class="tablink" onmouseover="openCity(event, 'Dermatology')">&nbsp;&nbsp;&nbsp; <strong>Dermatology</strong> </a></li>
						<li style="padding:5px 0px;"><a href="index.php?about=Gynecology" class="tablink" onmouseover="openCity(event, 'Gynecology')">&nbsp;&nbsp;&nbsp; <strong>Gynecology</strong> </a></li>
						<li style="padding:5px 0px;"><a href="index.php?about=Paediatrics" class="tablink" onmouseover="openCity(event, 'Pediatrics')">&nbsp;&nbsp;&nbsp; <strong>Pediatrics</strong> </a></li>
						<li style="padding:5px 0px;"><a href="index.php?about=Cosmetology" class="tablink" onmouseover="openCity(event, 'Cosmetology')">&nbsp;&nbsp;&nbsp; <strong>Cosmetology</strong> </a></li>
						<li style="padding:5px 0px;"><a href="index.php?about=Nutrition" class="tablink" onmouseover="openCity(event, 'Nutrition')">&nbsp;&nbsp;&nbsp; <strong>Nutrition</strong> </a></li>
						
					
					</ul>
					</div>
					<div class="col-md-10">
						<div id="main" class="w3-container city">
								<div class="col-md-12" style="line-height:30px; text-align:left;">
								<h4><br>Range of product category</h4><hr></hr>
								<?php
								include("include/database.php");
								$result0 = mysql_query("SELECT id,brandname,productname,head_name,pkg,nick_name,image_name,subcat FROM product where type='product'");
								$b=0;
								while($row  = mysql_fetch_array($result0))
								
								{
									$myarray0[$b]=$row[7];
									$b++;
								}

								$construct_array0=array_unique($myarray0);
								foreach($construct_array0 as $temp0)			
								{
										echo "<a href='index.php?about=subcat_product_list&subcat=$temp0&keyword=$temp0'>$temp0</a>"; 
										echo "&nbsp;&nbsp;  | &nbsp;&nbsp;";
										
								}
								?>
								</div>
								
						</div>	
						
						<div id="Trichology" class="w3-container city">
								<div class="col-md-8" style="line-height:30px; text-align:left;">
								<h4><br><strong>Trichology category</strong></h4><hr></hr>
								
								<?php
								include("include/database.php");
								$result = mysql_query("SELECT id,brandname,productname,head_name,pkg,nick_name,image_name,subcat FROM product where cat='Trichology' and type='product'");
								$i=0;
								while($row  = mysql_fetch_array($result))
								
								{
									$myarray[$i]=$row[7];
									$i++;
								}

								$construct_array=array_unique($myarray);
								foreach($construct_array as $temp)			
								{
										echo "<a href='index.php?about=subcat_product&subcat=$temp&cat=Trichology&keyword=$temp'>$temp </a>"; 
										echo "&nbsp;&nbsp;  | &nbsp;&nbsp;";
										
								}
								?>
								</div>
								<div class="col-md-4"><img src="images/tri.jpg"></div>
						</div>

						<div id="Dermatology" class="w3-container city">
								<div class="col-md-8" style="line-height:30px; text-align:left;">
								<h4><br><strong>Dermatology category</strong></h4><hr></hr>
								
								<?php
								include("include/database.php");
								$result1= mysql_query("SELECT id,brandname,productname,head_name,pkg,nick_name,image_name,subcat FROM product where cat='Dermatology' and type='product'");
								$j=0;
								while($row  = mysql_fetch_array($result1))
								
								{
									$myarray1[$j]=$row[7];
									$j++;
								}

								$construct_array1=array_unique($myarray1);
								foreach($construct_array1 as $temp1)			
								{
										echo "<a href='index.php?about=subcat_product&subcat=$temp1&cat=Dermatology&keyword=$temp1'>$temp1 </a>"; 
										echo "&nbsp;&nbsp;  | &nbsp;&nbsp;";
										
								}
								?>
								</div>
								<div class="col-md-4"><img src="images/der.jpg"></div>
						</div>

					  <div id="Gynecology" class="w3-container city">
								<div class="col-md-8" style="line-height:30px; text-align:left;">
								<h4><br><strong>Gynecology category</strong></h4><hr></hr>
								
								<?php
								include("include/database.php");
								$result2= mysql_query("SELECT id,brandname,productname,head_name,pkg,nick_name,image_name,subcat FROM product where cat='Gynecology' and type='product'");
								$k=0;
								while($row  = mysql_fetch_array($result2))
								
								{
									$myarray2[$k]=$row[7];
									$k++;
								}

								$construct_array2=array_unique($myarray2);
								foreach($construct_array2 as $temp2)			
								{
										echo "<a href='index.php?about=subcat_product&subcat=$temp2&cat=Gynecology&keyword=$temp2'>$temp2</a>"; 
										echo "&nbsp;&nbsp;  | &nbsp;&nbsp;";
										
								}
								?>
								</div>
								<div class="col-md-4"><img src="images/gyne.jpg"></div>
					  </div>
					  
					   <div id="Pediatrics" class="w3-container city">
								<div class="col-md-8" style="line-height:30px; text-align:left;">
								<h4><br><strong>Pediatrics category</strong></h4><hr></hr>
								<?php
								include("include/database.php");
								$result3= mysql_query("SELECT id,brandname,productname,head_name,pkg,nick_name,image_name,subcat FROM product where cat='Paediatrics' and type='product'");
								$l=0;
								while($row  = mysql_fetch_array($result3))
								
								{
									$myarray3[$l]=$row[7];
									$l++;
								}

								$construct_array3=array_unique($myarray3);
								foreach($construct_array3 as $temp3)			
								{
										echo "<a href='index.php?about=subcat_product&subcat=$temp3&cat=Paediatrics&keyword=$temp3'>$temp3 </a>"; 
										echo "&nbsp;&nbsp;  | &nbsp;&nbsp;";
										
								}
								?>
								</div>
								<div class="col-md-4"><img src="images/pedia.jpg"></div>
					  </div>
					  
					   <div id="Cosmetology" class="w3-container city">
								<div class="col-md-8" style="line-height:30px; text-align:left;">
								<h4><br><strong>Cosmetology category</strong></h4><hr></hr>
								<?php
								include("include/database.php");
								$result4= mysql_query("SELECT id,brandname,productname,head_name,pkg,nick_name,image_name,subcat FROM product where cat='Cosmetology' and type='product'");
								$m=0;
								while($row  = mysql_fetch_array($result4))
								
								{
									$myarray4[$m]=$row[7];
									$m++;
								}

								$construct_array4=array_unique($myarray4);
								foreach($construct_array4 as $temp4)			
								{
										echo "<a href='index.php?about=subcat_product&subcat=$temp4&cat=Cosmetology&keyword=$temp4'>$temp4 </a>"; 
										echo "&nbsp;&nbsp;  | &nbsp;&nbsp;";
										
								}
								?>
								</div>
								<div class="col-md-4"><img src="images/cos.jpg"></div>
					  </div>
					  
					  <div id="Nutrition" class="w3-container city">
								<div class="col-md-8" style="line-height:30px; text-align:left;">
								<h4><br><strong>Nutrition category</strong></h4><hr></hr>
								<?php
								include("include/database.php");
								$result5= mysql_query("SELECT id,brandname,productname,head_name,pkg,nick_name,image_name,subcat FROM product where cat='Nutrition' and type='product'");
								$n=0;
								while($row  = mysql_fetch_array($result5))
								
								{
									$myarray5[$n]=$row[7];
									$n++;
								}

								$construct_array5=array_unique($myarray5);
								foreach($construct_array5 as $temp5)			
								{
										echo "<a href='index.php?about=subcat_product&subcat=$temp5&cat=Nutrition&keyword=$temp5'>$temp5 </a>"; 
										echo "&nbsp;&nbsp;  | &nbsp;&nbsp;";
										
								}
								?>
								</div>
								<div class="col-md-4"><img src="images/nut.jpg"></div>
					  </div>
					 
					</div>

                </div>
            </li><!-- End Item -->
            
            
			
			
			
			
            
           
            
            <li><a href="index.php?about=doctor">Doctor info</a><!-- Begin Item -->
				<div class="dropdown_2columns dropdown_container"><!-- Begin Item Container -->
					<ul class="dropdown_flyout">
						 <li><a href="index.php?about=treading">&nbsp;&nbsp;&nbsp; Treading</a></li>
						<li><a href="index.php?about=update">&nbsp;&nbsp;&nbsp; Get Updates  </a></li>
						<li><a href="index.php?about=useful">&nbsp;&nbsp;&nbsp; Useful Links</a></li>
						<li><a href="index.php?about=feedback">&nbsp;&nbsp;&nbsp; Feedback  </a></li>
                    </ul>                       
                
                </div><!-- End Item Container -->
            </li><!-- End Item -->
            
            
             <li><a href="index.php?about=login">Login</a><!-- Begin Item -->
				<div class="dropdown_2columns dropdown_container"><!-- Begin Item Container -->
					<ul class="dropdown_flyout">
						<li><a href="login.php" target="blank">&nbsp;&nbsp;&nbsp;<strong> Admin</strong></a></li>
						<li><a href="http://www.appletherapeutics.in/tms/" target="blank">&nbsp;&nbsp;&nbsp; Sale report</a></li>
						<li><a href="index.php?about=submit_report">&nbsp;&nbsp;&nbsp; Submit report</a></li>
                    </ul>                       
                
                </div><!-- End Item Container -->
            </li><!-- End Item -->
            
            
            <li><a href="index.php?about=Career">Career</a><!-- Begin Item -->
				<div class="dropdown_2columns dropdown_container"><!-- Begin Item Container -->
					<ul class="dropdown_flyout">
						<li><a href="index.php?about=whyapple">&nbsp;&nbsp;&nbsp; Why Apple?</a></li>
						<li><a href="index.php?about=training">&nbsp;&nbsp;&nbsp; Training</a></li>
						<li><a href="index.php?about=Career">&nbsp;&nbsp;&nbsp; Current Opening</a></li>
                    </ul>                       
                
                </div><!-- End Item Container -->
            </li><!-- End Item -->
			
            <li><a href="index.php?about=Contact">Contact us</a><!-- Begin Item -->
				<div class="dropdown_2columns dropdown_container"><!-- Begin Item Container -->
					<ul class="dropdown_flyout">
						<li><a href="index.php?about=Contact">&nbsp;&nbsp;&nbsp; Contact</a></li>
						<li><a href="index.php?about=signup">&nbsp;&nbsp;&nbsp;Site map</a></li>
                    </ul>                       
                </div><!-- End Item Container -->
            </li><!-- End Item -->
			
			<li class="megamenu_right"><a href="index.php?about=greating">Greetings of the day</a></li>
			<li class="megamenu_right"><?php include("search/index.php"); ?></li>
        
        </ul><!-- End Mega Menu -->


    </div><!-- End Menu Container -->
	

	





</body>
</html>


<script>
function openCity(evt, cityName) {
  var i, x, tablinks;
  x = document.getElementsByClassName("city");
  for (i = 0; i < x.length; i++) {
     x[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablink");
  for (i = 0; i < x.length; i++) {
      tablinks[i].className = tablinks[i].className.replace(" w3-red", ""); 
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " w3-red";
}
</script>